import time
import random

for power in xrange(10, 21):
    a = list()
    iterations = 2**power
    print '%d\t' % iterations,
    
    sumTimes1 = 0
    for x in xrange(iterations):
        startTime = time.clock()
        a.append(x)
        sumTimes1 += time.clock() - startTime
    avgTime1 = sumTimes1/iterations
    print '%.15f\t' % avgTime1,

    sumTimes2 = 0
    for x in xrange(iterations):
        y = random.randint(iterations, iterations*2)
        startTime = time.clock()
        a[x] = y
        sumTimes2 += time.clock() - startTime
    avgTime2 = sumTimes2/iterations
    print '%.15f\t' % avgTime2,

    print '%.15f\t' % (avgTime2/avgTime1)
