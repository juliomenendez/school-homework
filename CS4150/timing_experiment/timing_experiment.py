#!/usr/bin/env python

import sys
import time
import random
from bst import BinarySearchTree


def generateRandomTree(size=8):
    tree = BinarySearchTree()
    items = range(size)
    for item in items:
        tree.insert(item)
    random.shuffle(items)
    return (items, tree)


def main(args):
    timesKeyExists = []
    timesKeyNotExists = []

#    powers = xrange(10, 21)
    powers = map(int, args)
    for power in powers:
        size = 2**power
        (items, tree) = generateRandomTree(size)

        timeSum = 0
        for item in items[:2]:
            timeStart = time.clock()
            tree.find(item)
            timeAvg = time.clock() - timeStart
            timeSum += timeAvg
        avgKeyExists = timeSum/float(2)

        timeSum = 0
        count = 0
        tries = 2
        while count < tries:
            count += 1
            item = size + count
            timeStart = time.clock()
            tree.find(item, True)
            timeAvg = time.clock() - timeStart
            timeSum += timeAvg
        avgKeyNotExists = timeSum/float(2)

        fout = open("power_%d.txt" % power, "w")
        print >> fout, '\t'.join(["2^%d" % power, str(avgKeyExists),
                                  str(avgKeyNotExists)])
        fout.close()

if __name__ == '__main__':
    main(sys.argv[1:])