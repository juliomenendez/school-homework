(TeX-add-style-hook "report"
 (lambda ()
    (TeX-run-style-hooks
     "float"
     "listings"
     "graphicx"
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

