#include <iostream>
#include "HashMap.h"

using namespace std;

int
main(int argc, char *argv[])
{
  if(argc == 1)
    {
      cout << "Usage: " << argv[0] << endl;
      return 1;
    }
  else
    {
      // TODO: Check arguments
      HashMap<string, int> hmStr = HashMap<string, int>(1, 0.75f);
      hmStr["key1"] = 1;
      hmStr["key2"] = 2;
      hmStr.add("key3", 3);
      hmStr.add("key4", 4);
      hmStr.add("key5", 53);
      cout << hmStr.size() << endl;
      cout << hmStr["key2"] << endl;
      cout << hmStr["key5"] << endl;
    }

  return 0;
}
