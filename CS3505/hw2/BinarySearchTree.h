#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H

#include <string>


//Node of generic type for use in the BST.
template <class T>
class BinarySearchTree
{
 public:
   
  // Defines a BST node.
  //Color returns a boolean where true = red and false = black.
  struct Node
  {
    T item;
    // V value;
    Node* parent;
    Node* left;
    Node* right;
    bool color;
    
    //**************************************************************************
    //Constructor to initialize pointers and colors
    //**************************************************************************
    Node()
    {
      parent = 0;
      left = 0;
      right = 0;
      color = true;

    }

    //**************************************************************************
    //Destructor when it gests call it should cause a cascading affect to
    // call the left right pointers node destructors and delete all nodes memory
    // memory allocation
    //**************************************************************************
    ~Node()
    {
      delete left; //should call the left childs destructor
      delete right; //should call the right childs destructor
      delete this; //both it's childrens memory should be de allocated so no de allocate this
    }
 };
  
  
  //Default Constructor
    BinarySearchTree();
   //Destructor
   ~BinarySearchTree();
    

  
  //method for finding a particular element.
    bool find(const T& item); 
    bool findRec(Node* n,const T& item);
    bool addNode(const T& item);
   

 private:
  
  Node* root;
  Node* nAt;
  int size;
  
 void rebalCase1(const Node *n);
 void rebalCase2(const Node *n);
 void rebalCase3(const Node *n);
 void rebalCase4(const Node *n);
 void rebalCase5(const Node *n);
 void rotateRight(const Node *n);
 void rotateLeft(const Node *n);   

};
  //*********************************************************************************
  //Default Constructor initialize all needed values
  //*********************************************************************************
  template <class T>
  BinarySearchTree<T>::BinarySearchTree()  
    {
      size = 0;
      //creates a root node
      root = new Node;
      
      //Need a default parent node to prevent segfault when checking it
      root->parent = new Node;
      root->parent = 0;
      //creates  the root nodes children
      root->left = new Node;
      root->right = new Node;
      //initialize item to null so we can tell if it is empty or not
      root->item = 0;
      root->left->item = 0;
      root->right->item = 0;
      //set childrens pareants to root node
      root->left->parent = root;
      root->right->parent = root;
      nAt = root;
    }

  //*********************************************************************************
  //Destructor deletes all alocated memory.
  //*********************************************************************************
  template <class T>
  BinarySearchTree<T>::~BinarySearchTree()
    {
      nAt = 0;
      delete root; //this should call the root nodes destructor and that should de allocate the tree
    }
  
//NOTE: MAYBE PASS ITEM AS A POINTER
//Method determining if the tree has the specified item. 
//Sets the the current Node to the leaf where the item should go
//if the item isn't present.
  template <class T>
  bool BinarySearchTree<T>::find(const T& item)
  { 
   return findRec(root, item);   
  }
  
//Recursive helper method for find using the node passed to traverse down the tree.
  template <class T>
  bool BinarySearchTree<T>::findRec(Node* n, const T& item)
  {

     if(n->item == 0)
       {
	 nAt = n;
	 return false;
       }
     else if(n->item < item)
       {
	 return findRec(n->left, item); 
       }
     else if (n->item > item)
       {
	 return findRec(n->right, item);
       }
     else 
       { 
	 nAt = n;
	 return true;
       }
  }

//Adds the specified item at the position specified by the current node pointer
//set by the find method.
  template <class T>
  bool BinarySearchTree<T>::addNode(const T& item)
  {
    if(find(item))
      {
	return false;
      }
    
    //if nAt's children are not pointing to null then it is the root node
    if(nAt->right!= 0 && nAt->left != 0){
      nAt->item = item;
      nAt->color = false;
      return true;
    }

    //if not root then set the nodes item equal to item
    nAt->item = item;
    //and create two new node children that items are null
    nAt->left = new Node;
    nAt->right = new Node;
    // set their parents equal to nat
    nAt->left->parent = nAt;
    nAt->right->parent = nAt;
    
    //paint  nAt red
    nAt->color = true;
    
    //Start with the top most case and iterate through from there till rebalanced.
    //assuming nAt is red.
    if(nAt->color == nAt->parent->color)
      { 
	//rebalCase1(nAt);
      }
    size++;
    return true;
  }

//Case 1 is for recoloring the root black in the event that during a rotation a red node was placed there
//This could lead to a black node with a black child but that is permissable so long as a red node doesnt
//have a red child.
  template <class T>
  void BinarySearchTree<T>:: rebalCase1(const Node *n) 
  {
    //if the node has no parent must be the root.
    if(n->parent->item == 0)
      {
        n->color = false;
      }
    else
      {
	rebalCase2(n);
      }
  }

//Case 2 says that if the parent color of the added node is black then the tree
//couldn't be imbalanced from the addition.
template <class T>
void BinarySearchTree<T>::rebalCase2(const Node *n)
{
  //if the parent color is black then adding a red node doesnt make the tree imbalanced
  if(!n->parent->color)
    {
      return;
    }
  else
    {
      rebalCase3(n);
    }
}

//Case 3 tests to see if the parent of the added node and the uncle share the same color.
//if that is the case then swapping their color with the color of gDad will fix the tree
//so long as we test gDad's recoloring for new imbalances.
template <class T>
void BinarySearchTree<T>::rebalCase3(const Node *n)
{
  Node *cParent = n->parent;
  Node *gDad = cParent->parent;
  Node *uncle = (gDad->item != 0 && gDad->left == cParent) ? gDad->right : gDad->left;
  
  //if the parent shares a color with the uncle then we can simply swap colors with gDad
  if(n->parent->color == uncle->color)
    {
      bool gDadColor = gDad->color;
      cParent->color = gDadColor;
      uncle->color = gDadColor;
      gDad->color = (gDad->color) ? false : true;
      //ensure gDad color switch imbalances are resolved.
      rebalCase1(gDad);
    }
  //otherwise we must continue with a rotation.
  else
    {
      rebalCase4(n);
    }
}

//Case 4 defines what types of rotation based on which color the parent vrs uncle situation is.
//they must be different to reach this case since case 3 tests sameness.
template <class T>
void BinarySearchTree<T>::rebalCase4(const Node *n)
{
  Node *nParent = n->parent;
  Node *gDad = nParent->parent;
  Node *uncle = (gDad->item != 0 && gDad->left == nParent) ? gDad->right : gDad->left;

  //determine how we should rotate
  if(n == nParent->right && nParent == gDad->left)
    {//was passing nparent
      rotateLeft(n);
    }
  else if(n == nParent->left && nParent == gDad->right)
    {//was passing nparent
      rotateRight(n);
    }
  //determine if the second kind of rotation is needed.
  rebalCase5(n);
}

//Case 5 defines a second type of rotation based on
//positions of parent uncle and n with respect to gDad.
template <class T>
void BinarySearchTree<T>::rebalCase5(const Node *n)
{
  Node *nParent = n->parent;
  Node *gDad = nParent->parent;
  Node *uncle = (gDad->item != 0 && gDad->left == nParent) ? gDad->right : gDad->left;

  if(n == nParent->left && nParent == gDad->left)
    {//was passing gd
      rotateRight(n);
    }
  else if(n == nParent->right && nParent == gDad->right)
    { //was passing gdad
      rotateLeft(n);
    }
}
 
//rotate the tree left
//***
template <class T>
void BinarySearchTree<T>::rotateLeft(const Node *n)
{ 
  Node *nParent = n->parent;
  Node *gDad = nParent->parent;
  Node *uncle = (gDad->item != 0 &&  gDad->left == nParent) ? gDad->right : gDad->left;
  
  //set grandpas right child to parents left child
  gDad->right = nParent->left;
  //set parents left childs parent to grandpa
  nParent->left->parent = gDad;
  //set parents left child to granpda 
  nParent->left = gDad;
  //set parents parent to grandpas parent
  nParent->parent = gDad->parent;
  //set grandpas parent to parent
  gDad->parent = nParent;   
}

//rotate the tree right
//***
template <class T>
void BinarySearchTree<T>::rotateRight(const Node *n)
{
  Node *nParent = n->parent;
  Node *gDad = nParent->parent;
  Node *uncle = (gDad->item !=0 && gDad->left == nParent) ? gDad->right : gDad->left;

  //set grandpas left child to parents left right child
  gDad->left = nParent->right;
  //set parents right childs parent to grandpa
  nParent->right->parent = gDad;
  //set parents right child to granpda
  nParent->right = gDad;
  //set parents parent to grandpas parent
  nParent->parent = gDad->parent;
  //set grandpas parent to parent
  gDad->parent = nParent;
}
#endif 
