#ifndef HASH_MAP_H
#define HASH_MAP_H

#include <string> // needed for specialized std::string Hasher
#include <cmath>

// General hash functor
template <typename T>
struct Hasher
{
  // Given a value val, returns the unsigned int hash value
  // of the given val
  unsigned int operator()(const T& val) const;
};

// Specialized std::string hasher
template <>
struct Hasher<std::string>
{
  // djb2 string hashing algorithm, adapted from http://www.cse.yorku.ca/~oz/hash.html
  unsigned int operator()(const std::string& val) const
  {
    unsigned int hash = 5381;
    unsigned int c;

    for (unsigned int i = 0; i < val.size(); i++)
      {
        c = (unsigned int)val[i];
        hash = ((hash << 5) + hash) ^ c;
      }

    return hash;
  }
};

// Stores (key, value) pairs of type (K, V), indexed according to the
// Hash functor
template <typename K, typename V, class Hash = Hasher<K> >
class HashMap
{
public:

  // Creates an empty HashMap
  HashMap(): bufferSize(1), data(new HeadNode[bufferSize]), elementCount(0), loadBalance(0.5f)
  {
  }
  
  // Creates a HashMap with enough memory allocated to hold size number
  // of elements with a load balance of loadBalanceRatio
  HashMap(unsigned int size, float loadBalanceRatio): bufferSize((int)(size / loadBalanceRatio)), data(new HeadNode[bufferSize]), elementCount(0), loadBalance(loadBalanceRatio)
  {
  }

  // Copy constructor
  HashMap(const HashMap<K, V, Hash>& map) : bufferSize(map.bufferSize), data(new HeadNode[bufferSize]), elementCount(map.elementCount), loadBalance(map.loadBalance)
  {
    for (unsigned int i = 0; i < bufferSize; i++)
      {
        Node* mapNode = map.data[i].next;

        if (mapNode != NULL)
          {
            data[i].next = new Node;
            data[i].next->key = mapNode->key;
            data[i].next->value = mapNode->value;
            
            mapNode = mapNode->next;
          }
        
        Node* thisNode = data[i].next;
        while (mapNode != NULL)
          {
            thisNode->next = new Node;
            thisNode->next->key = mapNode->key;
            thisNode->next->value = mapNode->value;
            
            thisNode = thisNode->next;
            mapNode = mapNode->next;
          }
      }
  }

  // Destructor
  ~HashMap()
  {
    for (unsigned int i = 0; i < bufferSize; i++)
      {
        Node* node = data[i].next;
        while (node != NULL)
          {
            Node* tmp = node->next;
            delete node;
            node = tmp;
          }
      }

    delete [] data;
  }
	
  // Returns the element at the particular key.  If the key does not exist
  // in the HashMap, it is inserted and it's value element is returned
  V& operator [] (const K& key)
    {
      reserve(elementCount + 1);
      unsigned int index = hasher(key) % bufferSize;
      Node* node = data[index].next;

      while (node != NULL)
        {
          if (node->key == key)
            {
              return node->value;
            }
          
          node = node->next;
        }
      
      node = new Node;
      node->key = key;
      node->value = V();
      node->next = data[index].next;
      data[index].next = node;
      elementCount++;
      return node->value;
    }
	
  // Allocates memory for the HashMap so that it can comfortably
  // fit size number of objects.  If the HashMap can already comfortably
  // fit size number of objects, no changes are made.
  void reserve(unsigned int size)
  {
    // If we can already comfortably fit size number of elements, return and do nothing
    if ((int)(size / loadBalance) < bufferSize)
      {
        return;
      }

    HeadNode* oldData = data;
    unsigned int oldBufferSize = bufferSize;

    bufferSize += (int) std::ceil(size / loadBalance);
    data = new HeadNode[bufferSize];
    
    for(unsigned int index=0; index<oldBufferSize; index++)
      {
        Node* node = oldData[index].next;
        while(node != NULL)
          {
            addNode(node->key, node->value);
            Node* tmp = node->next;
            delete node;
            node = tmp;
          }
      }

    delete [] oldData;
  }
	
  // Returns true if key is a valid key in the HashMap
  bool contains(const K& key) const
  {
    unsigned int index = hasher(key) % bufferSize;
    Node* node = data[index].next;

    while (node != NULL)
      {
        if (node->key == key)
          {
            return true;
          }
        
        node = node->next;
      }

    return false;
  }
	
  // Returns true if the HashMap contains no elements
  bool empty() const 
  {
    return elementCount == 0;
  }
	
  // Returns the number of elements stored in the HashMap (not
  // the size of the allocated memory buffer)
  unsigned int size() const
  {
    return elementCount;
  }

  // Adds a particular (key, value) pair to the HashMap.  This is
  // equivalent to hashMap[key] = value
  void add(const K& key, const V& value)
  {
    addNode(key, value);
    elementCount++;
  }
	
  // Removes a particular key from the HashMap.  If the key does not
  // exist in the HashMap, nothing is changed.
  void remove(const K& key)
  {
    unsigned int index = hasher(key) % bufferSize;
    Node* node = data[index].next;
    Node* previous = NULL;

    if (node != NULL && node->key == key)
      {
        Node* tmp = node->next;
        delete node;
        data[index].next = tmp;
        elementCount--;
        return;
      }
    else
      {
        previous = node;
        node = node->next;
      }

    while (node != NULL)
      {
        if (node->key == key)
          {
            Node* tmp = node->next;
            delete node;
            previous->next = tmp;
            elementCount--;
            return;
          }

        previous = node;
        node = node->next;
      }
  }

private:

  // Make this private so that others can't use it
  HashMap<K, V, Hash>& operator = (const HashMap<K, V, Hash>& h)
  {
    return *this;
  }

  // Because two keys may hash to the same index value, we need
  // a linked list at each index.
  struct Node
  {
    K key;
    V value;
    Node* next;
  };

  struct HeadNode
  {
    Node* next;
  };
	
  // The number of Nodes allocated for data
  unsigned int bufferSize;
  HeadNode* data; // place this after bufferSize so it can be initialized after it in the constructor
  // The number of valid/used Nodes in data
  unsigned int elementCount;
	
  // Represents the HashMap's load balance
  float loadBalance;

  Hash hasher;

  // Adds a particular (key, value) pair to the HashMap.  This is
  // equivalent to hashMap[key] = value. Doesn't increment the elementCount.
  void addNode(const K& key, const V& value)
  {
    reserve(elementCount + 1);
    unsigned int index = hasher(key) % bufferSize;
    Node* node = data[index].next;

    while (node != NULL)
      {
        if (node->key == key)
          {
            node->value = value;
	    return;
          }

        node = node->next;
      }

    node = new Node;
    node->key = key;
    node->value = value;
    node->next = data[index].next;
    data[index].next = node;
  }
	
};

#endif
