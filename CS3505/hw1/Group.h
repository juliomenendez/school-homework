#ifndef _GROUP_H_
#define _GROUP_H_

#include <string>
#include <vector>
#include <stdio.h>
#include <iostream>

#include "Student.h"

using namespace std;

class Group
{
 public:

  /**
   * Default constructor for a Group
   */
  Group();

  /**
   * Constructor which forms a group from the provided vector of students
   */
  Group(vector<Student> studentVector);

  /**
   * Method for adding a student to the Group.
   */
  void addStudent(Student student);

  /**
   * Method for returning the students in this group as a vector
   */
  vector<Student> getStudents();

  /**
   * Provides a string representation of the Group.
   */
  string toString();

  /**
   * Provides a HTML representation of the group.
   */
  string exportAsHtml();

 private:
  vector<Student> students;
};

#endif /* _GROUP_H_ */
