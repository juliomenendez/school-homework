#ifndef _STUDENT_H_
#define _STUDENT_H_

#include <string>
#include <vector>
#include <stdio.h>

using namespace std;

/**
 * class represents a student object.
 */
class Student
{
 private:
  string id;
  int goodnessFactor;
  vector<int> availableHours;

 public:
  bool assigned;

  /**
   * Default constructor for a Student
   */
  Student();

  /**
   * Copy constructor for a  Student
   */
  Student(const Student &student);

  /**
   * Constructor forming a Student object with the provided student id
   */
  Student(string id);

  /**
   * Constructor for forming a Student object with the provided student id
   * and goodness factor.
   */
  Student(string id, int goodnessFactor);

  /**
   * Method for adding available hours to the Student.
   */
  void addHour(int hour);

  /**
   * Provides a string representation of a student
   */
  string toString();

  /**
   * Returns the Student's id
   */
  string getId();

  /**
   * Returns an integer representing the Student's proficiency
   */
  int getGoodnessFactor() const;

  /**
   * Returns the Student's available hours.
   */
  vector<int> getAvailableHours();

  /**
   * Converts the string representation of proficiency into an int
   */
  static int GoodnessId(string goodness);

  /**
   * Array representing the possible values of proficiency
   */
  static string Goodness[];

  /**
   * Returns a HTML representation of the student
   */
  string exportAsHtml();
};

#endif /* _STUDENT_H_ */
