(TeX-add-style-hook "report_omega_alpha"
 (lambda ()
    (TeX-run-style-hooks
     "fullpage"
     "latex2e"
     "art11"
     "article"
     "11pt")))

