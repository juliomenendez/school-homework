#include "Group.h"

/**
 * Empty constructor.
 */
Group::Group()
{
}

/**
 * Group constructor taking a vector of students as a parameter.
 */
Group::Group(vector<Student> studentVector)
  :students(studentVector)
{

}

/**
 * Implementation for addStudent taking a student and adding it to the Group
 */
void Group::addStudent(Student student)
{
  students.push_back(student);
}

/**
 * A method for converting the group into a string
 */
string Group::toString()
{
  string theGroup;
  for(unsigned int i = 0; i<students.size(); i++)
    {
      theGroup += students[i].toString() + " ";
    }
  theGroup += "\n";
  return theGroup;
}

/**
 * Returns a vector representing the group
 */
vector<Student> Group::getStudents()
{
  return students;
}

/**
 * Returns a HTML representation of this group.
 */
string Group::exportAsHtml()
{
    string out = "<tr>";
    for(unsigned int i=0; i<students.size(); i++)
        out += "<td>" + students[i].exportAsHtml() + "</td>";
    out += "</tr>";
    return out;
}
