#ifndef _TEAMMATCHER_H_
#define _TEAMMATCHER_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <algorithm>

#include "Student.h"
#include "Group.h"

using namespace std;

/**
 * TeamMatcher application takes a file of students and places them into groups
 * based on their proficiency and ability to meet.
 */
class TeamMatcher
{
 public:

    /**
     * Constructor taking a filename and prepares it for parsing.
     */
    TeamMatcher(char *filename);

    /**
     * Constructor allowing the user to set student group sizes and allowed meeting times
     */
    TeamMatcher(char *filename, int groupSize, int meetingTimes);

    /**
     * Method containing the algorithims to place students into groups.
     */
    void createTeams();

    /**
     * Destructor for TeamMatcher application
     */
    ~TeamMatcher();

    /**
     * String representation
     */
    string toString();

    /**
     * HTML representation
     */
    string exportAsHtml();

 private:
    string filename;
    vector<Student> students;
    bool* studentHourMatrix[168];
    unsigned int requiredGroupSize;
    int requiredTimes;
    vector<Group> groups;
    Group outliers;

    /**
     * Method for parsing the file into student objects
     */
    void parseFile();

    /**
     * Helper method for grouping students by the hours they are able to meet.
     */
    void createStudentHourMatrix();

    /**
     * Helper method which determines the number of times students are able to
     * meet.
     */
    int countMeetingTimes(vector<int> &studentIndexes);

    /**
     * Method for placing students into groups
     */
    void makeGroups();

    /**
     * Helper method for marking students as assigned and placing them into a group.
     */
    Group assignToGroup(vector<int> &studentIndexes);

};

#endif /* _TEAMMATCHER_H_ */
