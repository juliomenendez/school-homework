#include "TeamMatcher.h"

/**
 * Implements TeamMatcher class
 */

/**
 * TeamMatcher constructor
 */
TeamMatcher::TeamMatcher(char *filename)
{
    this->filename = filename;
    requiredGroupSize = 4;
    requiredTimes = 5;
}

TeamMatcher::TeamMatcher(char *filename, int groupSize, int meetingTimes)
  :requiredGroupSize(groupSize), requiredTimes(meetingTimes)
{
  this->filename = filename;
}

TeamMatcher::~TeamMatcher()
{

}

/**
 * Main method for creating teams.
 */
void TeamMatcher::createTeams()
{
  parseFile();
  createStudentHourMatrix();
  makeGroups();
}

bool sortStudents(const Student& left, const Student& right)
{
    return (left.getGoodnessFactor() < right.getGoodnessFactor());
}

/**
 * Parses the information from the file in filename
 * and uses it to create a series of student objects.
 */
void TeamMatcher::parseFile()
{
    ifstream input (filename.c_str());
    if(input.is_open())
        {
            string line, word, studentId;
            Student student;
            int wordCount;
            int goodness;
            vector<Student>::iterator iter;
            while(input.good())
                {
                    getline(input, line);
                    istringstream linestream (line);
                    wordCount = 0;
                    if(line.size() == 0)
                      continue;
                    while(getline(linestream, word, ' '))
                        {
                            switch(wordCount)
                                {
                                case 0:
                                    studentId = word;
                                    break;
                                case 1:
                                    goodness = Student::GoodnessId(word);
                                    student = Student(studentId, goodness);
                                    break;
                                default:
                                    student.addHour(atoi(word.c_str()));
                                    break;
                                }
                            wordCount++;
                        }
                    students.push_back(student);
                }
            input.close();
            sort(students.begin(), students.end(), sortStudents);
        }
}

/**
 * Places students in groups based on the hours of the week they can meet.
 */
void TeamMatcher::createStudentHourMatrix()
{
  for(unsigned int i=0; i<168; i++)
    {
      studentHourMatrix[i] = new bool[students.size()];
      for(unsigned int j=0; j<students.size(); j++)
        studentHourMatrix[i][j] = 0;
    }
  for(unsigned int i=0; i<students.size(); i++)
    {
      vector<int> hours = students[i].getAvailableHours();
      for(unsigned int j=0; j<hours.size(); j++)
        studentHourMatrix[hours[j]][i] = 1;
    }
}

/**
 * Returns a string that represents the status of this instance.
 */
string TeamMatcher::toString()
{
    string out;

    //out += "Outliers: \n";
     for(unsigned int i = 0; i < groups.size(); i++)
        {
          out += groups[i].toString() + "\n\n";
        }
    return out;
}

/**
 * Helper method for checking the number of times students can meet.
 */
int TeamMatcher::countMeetingTimes(vector<int> &studentIndexes)
{
  int meetingTimes = 0;
  unsigned int sSize = studentIndexes.size();
  vector<int> indepentCounts;
  for(unsigned int i=0; i<sSize; i++)
    indepentCounts.push_back(0);

  unsigned int truthCounter = 0;
  for(int i = 0; i < 168; i++)
    {
      unsigned int x = 0;
      for(unsigned int j = 0; j < sSize; j++)
        {
          x += studentHourMatrix[i][studentIndexes[j]];
          if(studentHourMatrix[i][studentIndexes[j]])
            {
              truthCounter = 0;
              for(unsigned int k = 0; k < sSize; k++)
                {
                  truthCounter += studentHourMatrix[i][studentIndexes[k]];
                }
              indepentCounts[j] += (truthCounter == sSize);
            }
        }
      meetingTimes += (x == sSize); 
    }
  // if(meetingTimes < requiredTimes)
  //  {
  //   int tmpCount = 0;
  //  for(unsigned int i=0; i<indepentCounts.size(); i++)
  //       {
	    //      if(indepentCounts[i] < requiredTimes)
  //        {
  //          studentIndexes.erase(studentIndexes.begin()+(i - tmpCount));
  //          tmpCount++;
  //        }
  //      }

  //  return -1;
  // }

  return meetingTimes;
}

/**
 * Places the students into groups of 4 that can meet 5 times.
 * Students placed into a group have their assigned variable set to true.
 * returns the groups formed as a vector of groups.
 */
void TeamMatcher::makeGroups()
{

  //Begin with one student.
  //Add students until we have a group of 4 that can meet 5 times.

  vector<int> trialStudents;
  unsigned int groupedStudents = 0;
  unsigned int currentIndex = 0;
  unsigned int studentsCount = students.size();
  int tmpCount;
  //Iterate through the hours of the studentHourMatrix
  while(true)
      {
      if(groupedStudents == studentsCount || currentIndex == studentsCount)
        break;
      if(requiredGroupSize > trialStudents.size())
        {
          if(!students[currentIndex].assigned)
            {
              trialStudents.push_back(currentIndex);
            }
          currentIndex++;
        }
      else
        {
          tmpCount = countMeetingTimes(trialStudents);

          if(tmpCount == -1)
            continue;
          if(tmpCount >= requiredTimes)
            {
              groups.push_back(assignToGroup(trialStudents));
              currentIndex = 0;
              groupedStudents += requiredGroupSize;
            }
	  else
	    {
	      trialStudents.erase(trialStudents.begin());
	    }
        }
    }

  for(unsigned int i = 0; i < studentsCount; i++)
    {
        if(!students[i].assigned)
          outliers.addStudent(students[i]);
    }
}

/**
 * Makes a group from the provided studentIndexes and marks them as assigned.
 */
Group TeamMatcher::assignToGroup(vector<int> &studentIndexes)
{
  Group theGroup;

  for(unsigned int j = 0; j < studentIndexes.size(); j++)
    {
      students[studentIndexes[j]].assigned = true;
      theGroup.addStudent(students[studentIndexes[j]]);
    }
  studentIndexes.clear();
  return theGroup;
}


/**
 * Returns a HTML representation of the groups created.
 */
string TeamMatcher::exportAsHtml()
{
    string output = "<!DOCTYPE HTML>\n<html lang=\"en-US\">\n";
    output += "<head><meta charset=\"UTF-8\"><title></title></head>\n<body>";
    output += "<h3>Groups</h3><table cellspacing=\"4\">";
    for(unsigned int i=0; i<groups.size(); i++)
        output += groups[i].exportAsHtml();
    output += "</table>\n<h3>Outliers</h3>";
    vector<Student> outStudents = outliers.getStudents();
    for(unsigned int i=0; i<outStudents.size(); i++)
        output += outStudents[i].exportAsHtml() + "<br />";
    output += "</body>\n</html>";
    return output;
}
