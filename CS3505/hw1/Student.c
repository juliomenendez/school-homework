#include "Student.h"

/**
 * Empty constructor required by vector<Student>
 */
Student::Student()
{

}

/**
 * Copy constructor required by vector<Student>
 */
Student::Student(const Student &student)
{
    id = student.id;
    goodnessFactor = student.goodnessFactor;
    assigned = student.assigned;
    vector<int> avHours = student.availableHours;
    for(unsigned int i=0; i<(unsigned int)avHours.size(); i++)
        addHour(avHours.at(i));
}

/**
 * Student constructor naming the id
 */
Student::Student(string id)
  :id(id)
{
  assigned = false;
}

/**
 * Student constructor setting id and goodnessFactor
 */
Student::Student(string id, int goodnessFactor)
  :id(id),
   goodnessFactor(goodnessFactor)
{
  assigned = false;
}

/**
 * A method adding hours to the Student's availableHours
 */
void Student::addHour(int hour)
{
  availableHours.push_back(hour);
}

/**
 * Returns the student id attribute
 */
string Student::getId()
{
  return id;
}

/**
 * Returns the student goodnessFactor attribute
 */
int Student::getGoodnessFactor() const
{
  return goodnessFactor;
}

/**
 * Returns the student avaialableHours attribute
 */
vector<int> Student::getAvailableHours()
{
  return availableHours;
}

/**
 * Returns a string representing a student object
 */
string Student::toString()
{
  char *studentString = new char[1024];
  sprintf(studentString, "Student ID: %s, Goodness: %s", id.c_str(),
          Goodness[goodnessFactor].c_str());
  return string(studentString);
}

int Student::GoodnessId(string goodness)
{
    for(int i=0; i<5; i++)
        if(strcasecmp(goodness.c_str(), Goodness[i].c_str()) == 0)
            return i;
    return -1;
}

string Student::Goodness[5] = { "Excellent", "Good", "OK", "Poor", "No_Say" };

string Student::exportAsHtml()
{
    string out = "<span class=\"student-id\">" + id;
    out += "</span> <span class=\"student-goodness\">";
    out += Goodness[goodnessFactor] + "</span>";
    return out;
}
