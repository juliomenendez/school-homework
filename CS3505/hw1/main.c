#include <stdio.h>
#include <iostream>
#include <fstream>

#include "TeamMatcher.h"

using namespace std;

void usage(char *program)
{
    cout << "Usage: " << program << "  <input filename> [output filename]" << endl;
}

int main(int argc, char *argv[])
{
    char *outputFilename = NULL;
    char *inputFilename = NULL;
    switch(argc)
        {
        case 2:
            inputFilename = argv[1];
            break;
        case 3:
            inputFilename = argv[1];
            outputFilename = argv[2];
        }
    if(inputFilename == NULL)
        {
            cout << "ERROR: Input file is required." << endl;
            usage(argv[0]);
            return 1;
        }

    TeamMatcher *teamMatcher = new TeamMatcher(argv[1]);
    teamMatcher->createTeams();

    if(outputFilename != NULL)
        {
            ofstream output (outputFilename);
            output << teamMatcher->exportAsHtml() << endl;
            output.close();
        }
    else
        {
            cout << teamMatcher->exportAsHtml() << endl;
        }
    return 0;
}
