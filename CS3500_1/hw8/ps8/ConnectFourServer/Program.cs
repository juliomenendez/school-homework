﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectFourServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var timeout = (args.Length <= 1) ? 240 : Convert.ToInt16(args[1]);
            var port = (args.Length == 0) ? 4000 : Convert.ToInt16(args[0]);
            Server server = new Server(port, timeout);
            Console.ReadLine();
            server.Shutdown();
        }
    }
}
