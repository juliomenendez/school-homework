﻿using ConnectFourClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.IO;


namespace ConnectFourClientTests
{
    
    
    /// <summary>
    ///This is a test class for ClientTest and is intended
    ///to contain all ClientTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClientTest
    {

        private Random randomizer = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        private string randomString(int size)
        {
            char[] buffer = new char[size];
            for (int i = 0; i < size; i++)
            {
                buffer[i] = chars[randomizer.Next(chars.Length)];
            }
            return new string(buffer);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            Process.Start("ConnectFourServer.exe");
        }
        //
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            Process[] processes = Process.GetProcessesByName("ConnectFourServer");
            if (processes.Length > 0)
                processes[0].Kill();
        }

        #endregion

        public Client_Accessor GetClientInstance()
        {
            string username = randomString(8);
            string address = "localhost";
            int port = 4000;
            Client_Accessor target = new Client_Accessor(username, address, port);
            return target;
        }

        /// <summary>
        ///A test for Client Constructor
        ///</summary>
        [TestMethod()]
        public void ClientConstructorTest()
        {
            Client_Accessor client1 = GetClientInstance();
            AutoResetEvent client1Connected = new AutoResetEvent(false);
            string otherName = null;
            client1.add_GameStarted(new GameStartedHandler((string opponent, int timeout, string color) =>
                {
                    otherName = opponent;
                    client1Connected.Set();
                }));
            
            Client_Accessor client2 = GetClientInstance();
            client1Connected.WaitOne();
            Assert.AreEqual(client2.Name, otherName);
        }

        /// <summary>
        ///A test for ProcessCommand
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ConnectFourClient.exe")]
        public void ProcessCommandTest()
        {
            Client_Accessor client1 = GetClientInstance();
            AutoResetEvent resetEvent = new AutoResetEvent(false);
            bool called = false;
            client1.add_GameDraw(new GameDrawHandler(() =>
                {
                    called = true;
                    resetEvent.Set();
                }));
            client1.ProcessCommand("@draw");
            resetEvent.WaitOne();
            Assert.IsTrue(called);

            resetEvent = new AutoResetEvent(false);
            called = false;
            GameLegalHandler gLegal = new GameLegalHandler(() =>
            {
                called = true;
                resetEvent.Set();
            });
            client1.add_GameLegal(gLegal);
            client1.ProcessCommand("@legal");
            resetEvent.WaitOne();
            Assert.IsTrue(called);
            client1.remove_GameLegal(gLegal);

            Client_Accessor client2 = GetClientInstance();

            AutoResetEvent resetEventMovement1 = new AutoResetEvent(false);
            int column = -1;
            GameMovementHandler gMove = new GameMovementHandler((int col) =>
                {
                    column = col;
                    resetEventMovement1.Set();
                });
            client2.add_GameMovement(gMove);

            AutoResetEvent resetEventMovement2 = new AutoResetEvent(false);
            GameMovementHandler gMove2 = new GameMovementHandler((int col) =>
            {
                resetEventMovement2.Set();
            });
            client1.add_GameMovement(gMove2);

            AutoResetEvent resetEventClientOnline = new AutoResetEvent(false);
            client2.add_GameStarted(new GameStartedHandler((string opponent, int timeout, string color) =>
            {
                resetEventClientOnline.Set();
            }));

            resetEventClientOnline.WaitOne();
            client1.sendMove(1);
            resetEventMovement1.WaitOne();
            Assert.AreEqual(1, column);

            string playerColor = null;
            AutoResetEvent resetEventTicking = new AutoResetEvent(false);
            client1.add_GameTicking(new GameTickingHandler((string color, int time) =>
                {
                    playerColor = color;
                    resetEventTicking.Set();
                }));

            resetEventTicking.WaitOne();
            Assert.AreEqual("white", playerColor);

            AutoResetEvent resetEventWin = new AutoResetEvent(false);
            playerColor = null;
            client1.add_GameWinner(new GameWinnerHandler((string color) =>
                {
                    playerColor = color;
                    resetEventWin.Set();
                }));

            bool offline = false;
            AutoResetEvent resetEventDisconnected = new AutoResetEvent(false);
            client2.add_Disconnected(new DisconnectedHandler((string reason) =>
                {
                    offline = true;
                    resetEventDisconnected.Set();
                }));

            for (int i = 0; i < 3; i++)
            {
                resetEventMovement2.Reset();
                client2.sendMove(2);
                resetEventMovement2.WaitOne();
                Thread.Sleep(500);
                resetEventMovement1.Reset();
                client1.sendMove(1);
                resetEventMovement1.WaitOne();
                Thread.Sleep(500);
            }

            resetEventWin.WaitOne();
            Assert.AreEqual("black", playerColor);
            resetEventDisconnected.WaitOne();
            Assert.IsTrue(offline);
        }

        /// <summary>
        ///A test for Resign
        ///</summary>
        [TestMethod()]
        public void ResignTest()
        {
            Client_Accessor client1 = GetClientInstance();
            AutoResetEvent client1Connected = new AutoResetEvent(false);
            string otherName = null;
            client1.add_GameStarted(new GameStartedHandler((string opponent, int timeout, string color) =>
            {
                otherName = opponent;
                client1Connected.Set();
            }));

            Client_Accessor client2 = GetClientInstance();
            string playerColor = null;
            AutoResetEvent client1ResignedEventReset = new AutoResetEvent(false);
            client2.add_GamePlayerResigned(new GamePlayerResignedHandler((string color) =>
                {
                    playerColor = color;
                    client1ResignedEventReset.Set();
                }));

            client1Connected.WaitOne();
            client1.Resign();
            client1ResignedEventReset.WaitOne();
            Assert.AreEqual("black", playerColor);
        }

    }
}
