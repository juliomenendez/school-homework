﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConnectFourClient
{
    public partial class GameBoard : UserControl
    {
        GameBoardElement[,] board = new GameBoardElement[7, 6];
        public Brush CurrentBrush { get; set; }

        public event ColumnClickedHandler ColumnClicked;

        /// <summary>
        /// Initializes the control.
        /// </summary>
        public GameBoard()
        {
            InitializeComponent();
            Reset();
        }

        /// <summary>
        /// Resets the control to the initial state where no pieces have been placed.
        /// </summary>
        public void Reset()
        {
            int margin = 4;
            int boxWidth = (Width - margin * 7) / 7;
            int boxHeight = (Height - margin * 6) / 6;
            for (int i = 0; i < 7; i++)
                for (int j = 0; j < 6; j++)
                    board[i, j] = new GameBoardElement(i * (margin + boxWidth), j * (margin + boxHeight), boxWidth, boxHeight, Brushes.Transparent);
            Invalidate();
        }

        /// <summary>
        /// Handler for the paint event of the control.
        /// </summary>
        /// <param name="sender">control sending the event</param>
        /// <param name="e">arguments of the event</param>
        private void GameBoard_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    GameBoardElement temp = board[i, j];
                    graphics.DrawEllipse(Pens.Black, temp.Rectangle);
                    graphics.FillEllipse(board[i, j].Brush, temp.Rectangle);
                }
            }
        }

        /// <summary>
        /// Handler for the click event of the control.
        /// </summary>
        /// <param name="sender">control sending the event</param>
        /// <param name="e">arguments of the event</param>
        private void GameBoard_Click(object sender, EventArgs e)
        {
            MouseEventArgs mouseEventArgs = (MouseEventArgs)e;
            GameBoardElement element = null;
            GameBoardElement temp;
            int column = 0;
            for (int i = 0; i < 7; i++)
            {
                temp = board[i, 0];
                int y = temp.Rectangle.Y / 2;
                if (temp.Rectangle.Contains(mouseEventArgs.X, y))
                {
                    element = temp;
                    column = i;
                    break;
                }
            }
            if (element != null)
            {
                if (ColumnClicked != null)
                    ColumnClicked(column + 1);
            }
        }

        /// <summary>
        /// Given a column number puts a piece in the first empty cell of that column.
        /// </summary>
        /// <param name="column">number of the column to drop the piece</param>
        /// <param name="brush">brush with the color to fill the droppped piece</param>
        public void DropInColumn(int column, Brush brush)
        {
            GameBoardElement element = null, temp;
            for (int i = 5; i >= 0; i--)
            {
                temp = board[column, i];
                if (temp.Brush == Brushes.Transparent)
                {
                    element = temp;
                    break;
                }
            }
            element.Brush = brush;
            Invalidate();
        }
    }

    public class GameBoardElement
    {
        public Rectangle Rectangle { get; set; }
        public Brush Brush { get; set; }

        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="x">X coordinate of this piece</param>
        /// <param name="y">Y coordinate of this piece</param>
        /// <param name="width">width of this piece</param>
        /// <param name="height">height of this piece</param>
        /// <param name="brush">brush instance with the color of this piece</param>
        public GameBoardElement(int x, int y, int width, int height, Brush brush)
        {
            Rectangle = new Rectangle(x, y, width, height);
            this.Brush = brush;
        }
    }

    /// <summary>
    /// Delegate for the GameBoardColumnClicked event.
    /// </summary>
    /// <param name="column">the column that was clicked</param>
    public delegate void ColumnClickedHandler(int column);
}
