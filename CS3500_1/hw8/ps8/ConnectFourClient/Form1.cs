﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConnectFourClient
{
    public partial class MainForm : Form
    {
        string lastNameUsed = "";
        string lastIpUsed = "";
        string lastPortUsed = "";
        string youColor = "";
        Client gameClient;
        int lastColumnClicked;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            gameBoard.ColumnClicked += new ColumnClickedHandler(gameBoard_ColumnClicked);
        }

        /// <summary>
        /// Handler for the Form shown event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            showConnectionForm();
        }

        /// <summary>
        /// Shows the connect form. If the user click Connect stablishes the connection. Quits the application otherwise.
        /// </summary>
        private void showConnectionForm()
        {
            lock (this)
            {
                youNameRadio.Text = "";
                opponentNameRadio.Text = "";
                clockLabel.Text = "";
                yourColorLabel.BackColor = Color.Transparent;
                opponentColorLabel.BackColor = Color.Transparent;
                gameBoard.Reset();

                ConnectForm connectForm = new ConnectForm();
                connectForm.Username = lastNameUsed;
                connectForm.ServerPort = lastPortUsed;
                connectForm.ServerIP = lastIpUsed;
                if (connectForm.ShowDialog() == DialogResult.OK)
                {
                    lastNameUsed = connectForm.Username;
                    lastPortUsed = connectForm.ServerPort;
                    lastIpUsed = connectForm.ServerIP;
                    gameClient = new Client(lastNameUsed, lastIpUsed, Convert.ToInt16(lastPortUsed));
                    gameClient.Disconnected += new DisconnectedHandler(gameClient_Disconnected);
                    gameClient.GameStarted += new GameStartedHandler(gameClient_GameStarted);
                    gameClient.GameTicking += new GameTickingHandler(gameClient_GameTicking);
                    gameClient.GameMovement += new GameMovementHandler(gameClient_GameMovement);
                    gameClient.GameLegal += new GameLegalHandler(gameClient_GameLegal);
                    gameClient.GamePlayerDisconnected += new GamePlayerDisconnectedHandler(gameClient_GamePlayerDisconnected);
                    gameClient.GamePlayerResigned += new GamePlayerResignedHandler(gameClient_GamePlayerResigned);
                    gameClient.ExceptionRaised += new ExceptionRaisedHandler(gameClient_ExceptionRaised);
                    gameClient.GameWinner += new GameWinnerHandler(gameClient_GameWinner);
                    gameClient.GameDraw += new GameDrawHandler(gameClient_GameDraw);
                    gameClient.GameTimeout += new GameTimeoutHandler(gameClient_GameTimeout);
                }
                else
                {
                    Close();
                }
            }
        }

        /// <summary>
        /// Handler for the timeout event of the game.
        /// </summary>
        /// <param name="color">color of the player that ran out of time</param>
        void gameClient_GameTimeout(string color)
        {
            MessageBox.Show(color == youColor ? "You ran out of time. YOU LOST!" : "Your opponent ran out of time. YOU WIN!", "Game ended");
        }

        /// <summary>
        /// Handler for the draw event of the game.
        /// </summary>
        void gameClient_GameDraw()
        {
            MessageBox.Show("Nobody wins. Its a draw.", "Game ended");
        }

        /// <summary>
        /// Handler for the winner event of the game.
        /// </summary>
        /// <param name="color">color of the player that won</param>
        void gameClient_GameWinner(string color)
        {
            MessageBox.Show(color == youColor ? "YOU WON! :D" : "YOU LOST! :(", "Game ended");
        }

        /// <summary>
        /// Handler for the exceptionraised event of the game.
        /// </summary>
        /// <param name="message">message of the exception</param>
        void gameClient_ExceptionRaised(string message)
        {
            MessageBox.Show(message);
        }

        /// <summary>
        /// Handler for the GamePlayerResigned event of the game.
        /// </summary>
        /// <param name="color">color of the player who resigned</param>
        void gameClient_GamePlayerResigned(string color)
        {
            MessageBox.Show("Your opponent resigned. YOU WON!", "Game ended");
            showConnectionForm();
        }

        /// <summary>
        /// Handler for the GamePlayerDisconnected event of the game.
        /// </summary>
        /// <param name="color">color of the player that disconnected</param>
        void gameClient_GamePlayerDisconnected(string color)
        {
            MessageBox.Show("Your opponent disconnected.", "Game ended");
            showConnectionForm();
        }

        /// <summary>
        /// Handler for the GameLegal event of the game.
        /// </summary>
        void gameClient_GameLegal()
        {
            Action op = () =>
            {
                youNameRadio.Checked = false;
                opponentNameRadio.Checked = true;
                gameBoard.DropInColumn(lastColumnClicked - 1, (youColor == "black" ? Brushes.Black : Brushes.White));
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the ColumnClicked event of the game board.
        /// </summary>
        /// <param name="column">number of the column clicked</param>
        void gameBoard_ColumnClicked(int column)
        {
            if (youNameRadio.Checked)
            {
                lastColumnClicked = column;
                gameClient.sendMove(column);
            }
        }

        /// <summary>
        /// Handler for the GameMovement event of the game.
        /// </summary>
        /// <param name="column">number of the column where the move was done</param>
        void gameClient_GameMovement(int column)
        {
            Action op = () =>
            {
                youNameRadio.Checked = true;
                opponentNameRadio.Checked = false;
                gameBoard.DropInColumn(column - 1, (youColor != "black" ? Brushes.Black : Brushes.White));
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the GameTicking event of the game.
        /// </summary>
        /// <param name="color">color of the player whose clock is ticking</param>
        /// <param name="seconds">seconds remaining for that player</param>
        void gameClient_GameTicking(string color, int seconds)
        {
            Action op = () =>
            {
                if (color == youColor)
                    clockLabel.Text = seconds.ToString();
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the game started event. Sets the names and colors of the players
        /// </summary>
        /// <param name="opponentName">name of the opponent</param>
        /// <param name="timeout">time the player have in the entire game</param>
        /// <param name="youColor">"white" or "black". the color of this player</param>
        void gameClient_GameStarted(string opponentName, int timeout, string youColor)
        {
            Action op = () =>
            {
                this.youColor = youColor;
                youNameRadio.Text = lastNameUsed;
                opponentNameRadio.Text = opponentName;
                clockLabel.Text = timeout.ToString();
                yourColorLabel.BackColor = youColor == "black" ? Color.Black : Color.White;
                opponentColorLabel.BackColor = youColor == "black" ? Color.White : Color.Black;
                youNameRadio.Checked = youColor == "black";
                opponentNameRadio.Checked = !youNameRadio.Checked;
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the disconnected event of the game instance.
        /// </summary>
        /// <param name="reason">Reason the game disconnected</param>
        void gameClient_Disconnected(string reason)
        {
            Action op = () =>
            {
                gameClient.Disconnected -= gameClient_Disconnected;
                gameClient.GameStarted -= gameClient_GameStarted;
                gameClient.GameTicking -= gameClient_GameTicking;
                gameClient.GameMovement -= gameClient_GameMovement;
                gameClient.GameLegal -= gameClient_GameLegal;
                gameClient.GamePlayerDisconnected -= gameClient_GamePlayerDisconnected;
                gameClient.GamePlayerResigned -= gameClient_GamePlayerResigned;
                gameClient.ExceptionRaised -= gameClient_ExceptionRaised;
                gameClient.GameWinner -= gameClient_GameWinner;
                gameClient.GameDraw -= gameClient_GameDraw;
                gameClient.GameTimeout -= gameClient_GameTimeout;

                showConnectionForm();
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the Resign button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resignButton_Click(object sender, EventArgs e)
        {
            gameClient.Resign();
        }
    }
}
