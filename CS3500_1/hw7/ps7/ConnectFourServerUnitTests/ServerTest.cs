﻿using ConnectFourServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConnectFourServerUnitTests
{
    
    
    /// <summary>
    ///This is a test class for ServerTest and is intended
    ///to contain all ServerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ServerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Server Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ConnectFourServer.exe")]
        public void ServerConstructorTest()
        {
            int port = 0; // TODO: Initialize to an appropriate value
            int timeout = 0; // TODO: Initialize to an appropriate value
            Server_Accessor target = new Server_Accessor(port, timeout);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Shutdown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ConnectFourServer.exe")]
        public void ShutdownTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Server_Accessor target = new Server_Accessor(param0); // TODO: Initialize to an appropriate value
            target.Shutdown();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
