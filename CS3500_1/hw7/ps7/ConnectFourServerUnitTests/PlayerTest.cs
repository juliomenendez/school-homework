﻿using ConnectFourServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Sockets;

namespace ConnectFourServerUnitTests
{
    
    
    /// <summary>
    ///This is a test class for PlayerTest and is intended
    ///to contain all PlayerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PlayerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.Name;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Color
        ///</summary>
        [TestMethod()]
        public void ColorTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.Color = expected;
            actual = target.Color;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for setupGame
        ///</summary>
        [TestMethod()]
        public void setupGameTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket); // TODO: Initialize to an appropriate value
            string color = string.Empty; // TODO: Initialize to an appropriate value
            int timeout = 0; // TODO: Initialize to an appropriate value
            string opponent = string.Empty; // TODO: Initialize to an appropriate value
            target.setupGame(color, timeout, opponent);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for sendCommand
        ///</summary>
        [TestMethod()]
        public void sendCommandTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket); // TODO: Initialize to an appropriate value
            string command = string.Empty; // TODO: Initialize to an appropriate value
            target.sendCommand(command);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for disconnectGracefully
        ///</summary>
        [TestMethod()]
        public void disconnectGracefullyTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket); // TODO: Initialize to an appropriate value
            target.disconnectGracefully();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Player Constructor
        ///</summary>
        [TestMethod()]
        public void PlayerConstructorTest()
        {
            Socket socket = null; // TODO: Initialize to an appropriate value
            Player target = new Player(socket);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
