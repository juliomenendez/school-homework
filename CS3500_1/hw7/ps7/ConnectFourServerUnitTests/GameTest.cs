﻿using ConnectFourServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConnectFourServerUnitTests
{

    /// <summary>
    ///This is a test class for GameTest and is intended
    ///to contain all GameTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GameTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for translateMoveCoords
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ConnectFourServer.exe")]
        public void translateMoveCoordsTest()
        {
            int rowExp, colExp;
            int row, col;
            Game_Accessor target = new Game_Accessor(null);
            
            for (int i = 1; i < 43; i++)
            {
                rowExp = i / 7 - (i % 7 == 0 ? 1 : 0);
                colExp = (i % 7 == 0) ? 6 : i % 7 - 1;
                target.translateMoveCoords(i, out row, out col);
                Assert.AreEqual(rowExp, row);
                Assert.AreEqual(colExp, col);
            }
        }

        /// <summary>
        ///A test for Game Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ConnectFourServer.exe")]
        public void GameConstructorTest()
        {
            Player player1 = null; // TODO: Initialize to an appropriate value
            Player player2 = null; // TODO: Initialize to an appropriate value
            int timeout = 0; // TODO: Initialize to an appropriate value
            Game_Accessor target = new Game_Accessor(player1, player2, timeout);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }
    }
}
