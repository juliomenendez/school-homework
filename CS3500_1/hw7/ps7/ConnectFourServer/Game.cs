﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConnectFourServer
{
    /// <summary>
    /// Determines the next adjacent position.
    /// </summary>
    /// <param name="row">current row</param>
    /// <param name="col">current column</param>
    /// <param name="newRow">adjacent row</param>
    /// <param name="newCol">adjacent column</param>
    delegate void NextAdjacent(int row, int col, out int newRow, out int newCol);

    class Game
    {
        private Player player1, player2, currentPlayer = null;
        int timeout;
        int[,] board = new int[6, 7];
        int currentClock;
        CancellationTokenSource tickingCancellation;
        Task tickingTask;

        /// <summary>
        /// Creates a new game and starts waiting for the first player to move.
        /// </summary>
        /// <param name="player1">represents the first player that would become the black one</param>
        /// <param name="player2">represents the second player that would become the white one</param>
        /// <param name="timeout">>maximum time a player has to make a move</param>
        public Game(Player player1, Player player2, int timeout)
        {
            this.player1 = player1;
            this.player2 = player2;
            this.player1.Disconnected += new Disconnected(player_Disconnected);
            this.player2.Disconnected += new Disconnected(player_Disconnected);
            this.player1.Moved += new Moved(player_Moved);
            this.player2.Moved += new Moved(player_Moved);
            this.player1.Resign += new Resign(player_Resign);
            this.player2.Resign += new Resign(player_Resign);
            this.timeout = timeout;
            
            for (int i = 0; i < 6; i++)
                for (int j = 0; j < 7; j++)
                    board[i, j] = 0;

            this.player1.setupGame("black", timeout, this.player2.Name);
            this.player2.setupGame("white", timeout, this.player1.Name);

            changeTurn();
        }

        /// <summary>
        /// Handles a player resigning.
        /// </summary>
        /// <param name="player"></param>
        void player_Resign(Player player)
        {
            opponent(player).sendCommand(string.Format("resigned\r\n{0}", player.Color));
            forceEnd();
        }

        /// <summary>
        /// Handles a player moving.
        /// </summary>
        /// <param name="player">Player making the move.</param>
        /// <param name="pos">Position they moved to.</param>
        void player_Moved(Player player, int pos)
        {
            if (player != currentPlayer)
            {
                player.sendCommand("illegal");
                return;
            }
            int row, col;
            if (pos < 1 || pos > 42)
            {
                currentPlayer.sendCommand("illegal");
                return;
            }
            translateMoveCoords(pos, out row, out col);
            if ((row != 0 && board[row - 1, col] == 0) || board[row, col] != 0)
            {
                currentPlayer.sendCommand("illegal");
                return;
            }
            tickingCancellation.Cancel();

            board[row, col] = (currentPlayer == player1) ? 1 : 2;

            bool won = false;
            bool draw = false;
            // Check if the player has 4 adjancent horizontally
            int total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r; newCol = c + 1; });
            if (total >= 3)
                won = true;

            if (!won)
            {
                // Check if the player has 4 adjancent vertically
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the player has 4 adjancent diagonally raising
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c + 1; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the player has 4 adjancent diagonally falling
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c + 1; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the board is filled making the game a draw
                draw = true;
                for (int i = 0; i < 6; i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        draw = board[i, j] != 0;
                        if (!draw)
                            break;
                    }
                    if (!draw)
                        break;
                }
            }

            string opponentCommand = string.Format("move\r\n{0}", pos);
            string secondaryCommand = "";
            if (won)
                secondaryCommand = string.Format("win\r\n{0}", currentPlayer.Color);
            else if (draw)
                secondaryCommand = "draw";
            if (won || draw)
            {
                opponent(currentPlayer).sendCommand(string.Format("{0}\r\n{1}", opponentCommand, secondaryCommand));
                currentPlayer.sendCommand(string.Format("legal\r\n{0}", secondaryCommand));
                forceEnd();
            }
            else
            {
                opponent(currentPlayer).sendCommand(opponentCommand);
                currentPlayer.sendCommand("legal");
                changeTurn();
            }
        }

        /// <summary>
        /// Counts the number of adjacent positions with the same value
        /// </summary>
        /// <param name="row">row of the starting position</param>
        /// <param name="col">column of the starting position</param>
        /// <param name="nextAdjacent">method that calculates the next adjacent position</param>
        /// <returns></returns>
        int countAdjacent(int row, int col, NextAdjacent nextAdjacent)
        {
            int count = 0;
            int playerValue = board[row, col];
            int newRow, newCol;
            while (true)
            {
                nextAdjacent(row, col, out newRow, out newCol);
                if (newRow < 0 || newRow >= 6 || newCol < 0 || newCol >= 7 || board[newRow, newCol] != playerValue)
                {
                    break;
                }
                else
                {
                    count++;
                    row = newRow;
                    col = newCol;
                }
            }
            return count;
        }
       
        /// <summary>
        /// Changes the current player and restarts the timer.
        /// </summary>
        void changeTurn()
        {
            currentClock = timeout;
            currentPlayer = (currentPlayer == null) ? player1 : opponent(currentPlayer);
            tickingCancellation = new CancellationTokenSource();
            tickingTask = new Task(tickingHandler, tickingCancellation.Token);
            tickingTask.Start();
            
        }

        /// <summary>
        /// Decrements the move clock and waits for cancel if player moves before clock = 0.
        /// </summary>
        void tickingHandler()
        {
            lock (this)
            {
                currentClock--;
                string commandName = (currentClock == 0) ? "time" : "tick";
                String commandArgument = (currentClock == 0) ? currentPlayer.Color : currentClock.ToString();
                String command = String.Format("{0}\r\n{1}", commandName, commandArgument);

                player1.sendCommand(command);
                player2.sendCommand(command);

                if (currentClock == 0)
                {
                    forceEnd();
                    return;
                }

                if (!tickingCancellation.IsCancellationRequested)
                {
                    Thread.Sleep(1000);
                    tickingHandler();
                }
            }
        }

        /// <summary>
        /// Handles a player disconnecting.
        /// </summary>
        /// <param name="player">The player that disconnected</param>
        void player_Disconnected(Player player)
        {
            string commandFormat = "disconnect\r\n{0}";
            opponent(player).sendCommand(string.Format(commandFormat, player.Color));
        }

        /// <summary>
        /// Returns the player's opponent.
        /// </summary>
        /// <param name="player">a player instance in this game</param>
        /// <returns>the opponent to <paramref name="player"/> in this game</returns>
        Player opponent(Player player)
        {
            return (player == this.player1) ? this.player2 : this.player1;
        }

        /// <summary>
        /// Translates a one integer move position to a x,y coordinate compatible
        /// with the game board.
        /// </summary>
        /// <param name="movePos">position sent by the client</param>
        /// <param name="row">variable where to store the calculated row</param>
        /// <param name="col">variable where to store the calculated column</param>
        public void translateMoveCoords(int movePos, out int row, out int col)
        {
            row = movePos / 7 - (movePos % 7 == 0 ? 1 : 0);
            col = (movePos % 7 == 0) ? 6 : movePos % 7 - 1;
        }

        /// <summary>
        /// Method called when the server shutsdown. Disconnects both players.
        /// </summary>
        public void forceEnd()
        {
            player1.disconnectGracefully();
            player2.disconnectGracefully();
        }
    }
}
