﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Net.Sockets;
using System.Net;

namespace ConnectFourServer
{
    /// <summary>
    /// Handles the first line of interaction with the clients. Creates the players and the games.
    /// </summary>
    class Server
    {
        private int port, timeout;
        private ArrayList waitingPlayers, games;
        private TcpListener server;

        /// <summary>
        /// Creates a new instance of the server. Starts listening on the specified port waiting for clients. 
        /// </summary>
        /// <param name="port">port to listen to</param>
        /// <param name="timeout">maximum time the players has to make a move</param>
        public Server(int port, int timeout)
        {
            this.port = port;
            this.timeout = timeout;
            waitingPlayers = new ArrayList();
            games = new ArrayList();

            server = new TcpListener(IPAddress.Any, port);
            server.Start();
            server.BeginAcceptSocket(AcceptSocketCallback, null);
        }

        /// <summary>
        /// Callback for the Socket accept
        /// </summary>
        /// <param name="result">Result from the async accept socket</param>
        void AcceptSocketCallback(IAsyncResult result)
        {
            try
            {
                Socket socket = server.EndAcceptSocket(result);
                server.BeginAcceptSocket(AcceptSocketCallback, null);
                Player player = new Player(socket);
                player.Connected += new Connected(player_Connected);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Handler for the Player.Connected event.
        /// </summary>
        /// <param name="player">player that sent the event</param>
        /// <param name="name">name of the player</param>
        void player_Connected(Player player, string name)
        {

            waitingPlayers.Add(player);

            if (waitingPlayers.Count > 1)
            {
                Player player1 = (Player)waitingPlayers[0];
                Player player2 = (Player)waitingPlayers[1];

                waitingPlayers.RemoveAt(0);
                waitingPlayers.RemoveAt(0);
                games.Add(new Game(player1, player2, timeout));
            }

        }

        /// <summary>
        /// Closes the server and disconnects gracefully all the games and its players.
        /// </summary>
        public void Shutdown()
        {
            server.Stop();
            lock (this)
            {
                foreach (Game game in games)
                {
                    game.forceEnd();
                }
            }
        }
    }
}
