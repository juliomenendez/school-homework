﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace ConnectFourServer
{
    /// <summary>
    /// Handler for the connected event.
    /// </summary>
    /// <param name="player">player instance that connected</param>
    /// <param name="name">name of the player</param>
    public delegate void Connected(Player player, string name);

    /// <summary>
    /// Handler for the disconnected event.
    /// </summary>
    /// <param name="player">player instance that disconnected</param>
    public delegate void Disconnected(Player player);

    /// <summary>
    /// Handler for the moved event.
    /// </summary>
    /// <param name="player">player instance that made the move</param>
    /// <param name="pos">position the player moved to</param>
    public delegate void Moved(Player player, int pos);

    /// <summary>
    /// Handler for the resign event.
    /// </summary>
    /// <param name="player">player instance that resigned</param>
    public delegate void Resign(Player player);


    /// <summary>
    /// Represents the player on the server. Handles all the communication with the client.
    /// </summary>
    public class Player
    {
        private Socket socket;
        private string name;
        private byte[] buffer = new byte[1024];
        private string receivedData;

        /// <summary>
        /// Creates a player instance. Start listening on the socket.
        /// </summary>
        /// <param name="socket">Socket connected to the client.</param>
        public Player(Socket socket)
        {
            this.Connected += new ConnectFourServer.Connected(Player_Connected);
            this.socket = socket;
            try
            {
                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Handles when the user sends the name to use in the game.
        /// </summary>
        /// <param name="player">player instance triggering the event</param>
        /// <param name="name">name of the player</param>
        void Player_Connected(Player player, string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Callback for Socket receiving.
        /// </summary>
        /// <param name="result">Represents the results from the receive</param>
        void ReceiveCallback(IAsyncResult result)
        {
            int receivedByteCount = 0;
            try
            {
                receivedByteCount = socket.EndReceive(result);
            }
            catch (Exception)
            {
            }

            if (receivedByteCount == 0)
            {
                socket.Close();
                Disconnected(this);
            }
            else
            {
                receivedData += Encoding.Default.GetString(buffer, 0, receivedByteCount);
                parseReceivedData();

                try
                {
                    socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
                }
                catch (Exception)
                {
                    socket.Close();
                    Disconnected(this);
                }
            }
        }

        /// <summary>
        /// Triggers when the user logs in.
        /// </summary>
        public event Connected Connected;

        /// <summary>
        /// Triggers whenever the user connection is lost.
        /// </summary>
        public event Disconnected Disconnected;

        /// <summary>
        /// Triggers when the user makes a move.
        /// </summary>
        public event Moved Moved;

        /// <summary>
        /// Triggers when the user resigns.
        /// </summary>
        public event Resign Resign;

        public string Color { get; set; }
        public string Name { get { return name; } }

        /// <summary>
        /// Sends a command to the client.
        /// </summary>
        /// <param name="command">command to send.</param>
        public void sendCommand(string command)
        {
            lock (this)
            {
                byte[] buffer = Encoding.Default.GetBytes(command + "\r\n");
                try
                {
                    socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, SendCallback, null);
                }
                catch (Exception)
                {
                    Disconnected(this);
                }
            }
        }

        /// <summary>
        /// Callback for Socket sending
        /// </summary>
        /// <param name="result">Represents the results from the async send.</param>
        void SendCallback(IAsyncResult result)
        {
            try
            {
                socket.EndSend(result);
            }
            catch (Exception)
            {
                Disconnected(this);
            }
        }

        /// <summary>
        /// Setups the needed information to play on a game.
        /// </summary>
        /// <param name="color">color assigned to this player</param>
        /// <param name="timeout">maximum time the player has to make a move</param>
        /// <param name="opponent">name of the opponent</param>
        public void setupGame(string color, int timeout, string opponent)
        {
            Color = color;
            string command = string.Format("play\r\n{0}\r\n{1}\r\n{2}", opponent, timeout, color);
            sendCommand(command);
        }

        /// <summary>
        /// Parses the data received from the socket.
        /// </summary>
        public void parseReceivedData()
        {
            int index;
            string command, argument;
            command = argument = "";
            bool waitForArgument = false;
            string originalData = receivedData;

            while ((index = receivedData.IndexOf('\n')) > 0)
            {
                string line = receivedData.Substring(0, index);
                if(line.EndsWith("\r")) 
                {
                    line = line.Substring(0, index - 1);
                }
                if (waitForArgument)
                {
                    argument = line;
                }
                else
                {
                    waitForArgument = (line == "name" || line == "move");
                    command = line;
                }
                receivedData = receivedData.Substring(index + 1);
            }

            bool commandHandled = false;
            switch (command)
            {
                case "name":
                    if (argument != "")
                    {
                        commandHandled = true;
                        Connected(this, argument);
                    }
                    break;
                case "move":
                    if (argument != "")
                    {
                        commandHandled = true;
                        Moved(this, Convert.ToInt16(argument));
                    }
                    break;
                case "resign":
                    commandHandled = true;
                    Resign(this);
                    break;
            }

            // If the command or argument hasn't been received entirely restore the received data so far
            // and hope the next receive callback will process it.
            if (!commandHandled)
                receivedData = originalData;
        }

        /// <summary>
        /// Closes the socket gracefully.
        /// </summary>
        public void disconnectGracefully()
        {
            socket.Close();
        }
    }
       
}
