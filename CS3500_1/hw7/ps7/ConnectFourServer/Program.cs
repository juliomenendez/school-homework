﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectFourServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server(Convert.ToInt16(args[0]), Convert.ToInt16(args[1]));
            Console.ReadLine();
            server.Shutdown();
        }
    }
}
