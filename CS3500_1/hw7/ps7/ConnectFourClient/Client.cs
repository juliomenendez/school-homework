﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace ConnectFourClient
{
    /// <summary>
    /// Delegate for the Disconnected event of the Client class.
    /// Triggered whenever the server disconnects.
    /// </summary>
    /// <param name="reason">the reason for the disconnection.</param>
    public delegate void DisconnectedHandler(string reason);

    /// <summary>
    /// Delegate for the ExceptionRaised event of the Client class.
    /// Triggered when there is an exception.
    /// </summary>
    /// <param name="message">message describing the exception</param>
    public delegate void ExceptionRaisedHandler(string message);

    /// <summary>
    /// Delegate for the GameStarted event of the Client class.
    /// Triggered when the client receives the "play" command.
    /// </summary>
    /// <param name="opponentName">the name of the opponent</param>
    /// <param name="timeout">the number of seconds the player has to make a move</param>
    /// <param name="youColor">the color of this client</param>
    public delegate void GameStartedHandler(string opponentName, int timeout, string youColor);

    /// <summary>
    /// Delegate for the GameTicking event of the Client class.
    /// Triggered when the client receives the "tick" command.
    /// </summary>
    /// <param name="seconds">number of seconds remaining</param>
    public delegate void GameTickingHandler(int seconds);

    class Client
    {
        private IPAddress address;
        private int port;
        private string username;
        private TcpClient tcpClient;
        private string readData;
        private byte[] readBuffer;

        /// <summary>
        /// Event definitions. For documentation about each event refer to the corresponding
        /// delegate.
        /// </summary>
        public event DisconnectedHandler Disconnected;
        public event ExceptionRaisedHandler ExceptionRaised;
        public event GameStartedHandler GameStarted;
        public event GameTickingHandler GameTicking;

        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="username">Username to log in to the server</param>
        /// <param name="address">IP address of the server to connect. Defaults to 'localhost'</param>
        /// <param name="port">Port where the server is listening to. Defaults to 4000</param>
        public Client(string username, string address = "localhost", int port = 4000)
        {
            try
            {
                this.address = IPAddress.Parse(address == "localhost" ? "127.0.0.1" : address);
            }
            catch
            {
                TriggerDisconnected("Invalid IP address");
                return;
            }
            this.username = username;
            this.port = port;


            tcpClient.BeginConnect(address, port, tcpClientConnectCallback, null);
        }

        /// <summary>
        /// Method called from the TcpClient.BeginConnect call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void tcpClientConnectCallback(IAsyncResult result)
        {
            try
            {
                tcpClient.EndConnect(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(ex.ToString());
                return;
            }

            // Client connected.
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);

            // Login to the server.
            sendCommand(string.Format("name\n{0}", username));
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginRead call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void streamReadCallback(IAsyncResult result)
        {
            int readCount = 0;
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                readCount = stream.EndRead(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(string.Format("Error reading stream. {0}", ex.Message));
            }

            if (readCount == 0)
            {
                tcpClient.Close();
            }

            readData += Encoding.UTF8.GetString(readBuffer, 0, readCount);

            int index;
            while ((index = readData.IndexOf("\r\n")) > 0)
            {
                string command = readData.Substring(0, index + 1);
                ProcessCommand(command);
                readData = readData.Substring(index + 1);
            }

            if (tcpClient.Connected)
                // Back to reading from the connection.
                stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);
        }

        /// <summary>
        /// Takes the string comming from the server, parses it and triggers the corresponding events.
        /// </summary>
        /// <param name="command">string comming from the server with a valid command</param>
        private void ProcessCommand(string command)
        {
 
        }

        /// <summary>
        /// Starts an async send to the server.
        /// </summary>
        /// <param name="command">command to send</param>
        private void sendCommand(string command)
        {
            byte[] outgoingBytes = Encoding.UTF8.GetBytes(command + "\n");
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginWrite(outgoingBytes, 0, outgoingBytes.Length, streamWriteCallback, null);
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginWrite call
        /// </summary>
        /// <param name="result">result of the async call</param>
        private void streamWriteCallback(IAsyncResult result)
        {
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                stream.EndWrite(result);
            }
            catch (Exception ex)
            {
                if (ExceptionRaised != null)
                    ExceptionRaised(string.Format("Error sending command to the server. {0}", ex.Message));
                else
                    throw new Exception("Error sending command to the server.", ex);
            }
        }

        /// <summary>
        /// Helper method that triggers the Disconnected event.
        /// </summary>
        /// <param name="reason">reason of the disconnection</param>
        private void TriggerDisconnected(string reason)
        {
            if (Disconnected != null)
                Disconnected(reason);
        }
    }
}
