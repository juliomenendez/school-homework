﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConnectFourClient
{
    public partial class MainForm : Form
    {
        string lastNameUsed = "";
        string lastIpUsed = "";
        string lastPortUsed = "";
        Client gameClient;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handler for the Form shown event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            showConnectionForm();
        }

        /// <summary>
        /// Shows the connect form. If the user click Connect stablishes the connection. Quits the application otherwise.
        /// </summary>
        private void showConnectionForm()
        {
            youNameRadio.Text = "";
            opponentNameRadio.Text = "";
            clockLabel.Text = "";
            yourColorLabel.BackColor = Color.Transparent;
            opponentColorLabel.BackColor = Color.Transparent;

            ConnectForm connectForm = new ConnectForm();
            connectForm.Username = lastNameUsed;
            connectForm.ServerPort = lastPortUsed;
            connectForm.ServerIP = lastIpUsed;
            if (connectForm.ShowDialog() == DialogResult.OK)
            {
                lastNameUsed = connectForm.Username;
                lastPortUsed = connectForm.ServerPort;
                lastIpUsed = connectForm.ServerIP;
                gameClient = new Client(lastNameUsed, lastIpUsed, Convert.ToInt16(lastPortUsed));
                gameClient.Disconnected += new DisconnectedHandler(gameClient_Disconnected);
            }
            else
            {
                Close();
            }
        }

        void gameClient_Disconnected(string reason)
        {
            showConnectionForm();
        }

        /// <summary>
        /// Handler for the Resign button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resignButton_Click(object sender, EventArgs e)
        {
            // TODO: Send the resign signal to the server.
        }
    }
}
