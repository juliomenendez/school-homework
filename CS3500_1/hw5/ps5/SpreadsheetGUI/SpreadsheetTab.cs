﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SS;
using System.Windows.Forms;


namespace SpreadsheetGUI
{
    class SpreadsheetTab : System.Windows.Forms.TabPage
    {
        // Controls
        private SS.SpreadsheetPanel spreadsheetPanel;
        private System.Windows.Forms.TextBox currentName;
        private System.Windows.Forms.TextBox currentValue;
        private System.Windows.Forms.TextBox currentContent;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;

        private SS.Spreadsheet spreadsheet;
        private string filename;
        private string selectedCellName;

        /// <summary>
        /// Creates a new instance. Lays out the controls.
        /// </summary>
        /// <param name="name">Name of the spreadsheet. Usually its file name.</param>
        public SpreadsheetTab(string name) : base(name)
        {
            filename = name;
            spreadsheet = new Spreadsheet();
            CreateInnerControls();
            if (System.IO.File.Exists(filename))
            {
                spreadsheet = (Spreadsheet)spreadsheet.Read(filename);
                foreach (string cellName in spreadsheet.GetAllCellNames())
                {
                    int[] coords = TranslateCellNameToCoords(cellName);
                    string cellValue = System.Convert.ToString(spreadsheet.GetCellValue(cellName));
                    spreadsheetPanel.SetValue(coords[0], coords[1], cellValue);
                }
            }

            // Call the SelectionChanged delegate to update the text boxes with the cell A1
            spreadsheetPanel_SelectionChanged(spreadsheetPanel);
        }

        private void CreateInnerControls()
        {
            this.SuspendLayout();

            // Setup the tab
            Text = filename;
            Location = new System.Drawing.Point(4, 22);
            Padding = new System.Windows.Forms.Padding(3);
            Size = new System.Drawing.Size(907, 450);

            // Create all the inner controls and add them to this tab
            spreadsheetPanel = new SpreadsheetPanel();
            Controls.Add(spreadsheetPanel);
            currentName = new System.Windows.Forms.TextBox();
            Controls.Add(currentName);
            currentValue = new System.Windows.Forms.TextBox();
            Controls.Add(currentValue);
            currentContent = new System.Windows.Forms.TextBox();
            Controls.Add(currentContent);
            okButton = new System.Windows.Forms.Button();
            Controls.Add(okButton);
            cancelButton = new System.Windows.Forms.Button();
            Controls.Add(cancelButton);
            System.Windows.Forms.Label nameLabel = new System.Windows.Forms.Label();
            Controls.Add(nameLabel);
            System.Windows.Forms.Label valueLabel = new System.Windows.Forms.Label();
            Controls.Add(valueLabel);
            System.Windows.Forms.Label contentLabel = new System.Windows.Forms.Label();
            Controls.Add(contentLabel);

            // Setup labels
            nameLabel.Text = "Cell name:";
            nameLabel.Location = new System.Drawing.Point(8, 6);
            nameLabel.Size = new System.Drawing.Size(68, 13);

            valueLabel.Text = "Cell value:";
            valueLabel.Location = new System.Drawing.Point(138, 6);
            valueLabel.Size = new System.Drawing.Size(68, 13);

            contentLabel.Text = "Cell content:";
            contentLabel.Location = new System.Drawing.Point(384, 6);
            contentLabel.Size = new System.Drawing.Size(80, 13);

            // Setup text boxes
            currentName.Location = new System.Drawing.Point(80, 3);
            currentName.Size = new System.Drawing.Size(52, 20);
            currentName.Enabled = false;

            currentValue.Location = new System.Drawing.Point(212, 3);
            currentValue.Size = new System.Drawing.Size(166, 20);
            currentValue.Enabled = false;

            currentContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            currentContent.Location = new System.Drawing.Point(470, 3);
            currentContent.Size = new System.Drawing.Size(331, 20);
            currentContent.Multiline = false;

            // Setup the buttons
            okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            okButton.Location = new System.Drawing.Point(807, 1);
            okButton.Size = new System.Drawing.Size(38, 23);
            okButton.Text = "OK";
            okButton.Click += new EventHandler(okButton_Click);

            cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            cancelButton.Location = new System.Drawing.Point(851, 1);
            cancelButton.Size = new System.Drawing.Size(50, 23);
            cancelButton.Text = "Cancel";
            cancelButton.Click += new EventHandler(cancelButton_Click);

            // Setup the spreadsheet panel
            spreadsheetPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            spreadsheetPanel.Location = new System.Drawing.Point(6, 29);
            spreadsheetPanel.Size = new System.Drawing.Size(893, 413);
            spreadsheetPanel.SelectionChanged += new SelectionChangedHandler(spreadsheetPanel_SelectionChanged);

            ResumeLayout(false);
            PerformLayout();
        }

        void cancelButton_Click(object sender, EventArgs e)
        {
            currentContent.Text = spreadsheet.GetCellContents(selectedCellName);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string cellCurrentContent = spreadsheet.GetCellContents(selectedCellName);
            if (currentContent.Text != cellCurrentContent)
            {
                try
                {
                    IEnumerable<string> cellNamesToUpdate = spreadsheet.SetCellContents(selectedCellName, currentContent.Text);
                    foreach (string cellName in cellNamesToUpdate)
                    {
                        int[] coords = TranslateCellNameToCoords(cellName);
                        string cellValue = System.Convert.ToString(spreadsheet.GetCellValue(cellName));
                        if (cellName == selectedCellName)
                            currentValue.Text = cellValue;
                        spreadsheetPanel.SetValue(coords[0], coords[1], cellValue);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format("Error ocurred changing the content of cell {0}.\n{1}", selectedCellName, ex.Message), "Cell content error");
                }
            }
        }

        private int[] TranslateCellNameToCoords(string cellName)
        {
            int col = ((byte)cellName[0]) - 65;
            int row = System.Convert.ToInt16(cellName.Substring(1)) - 1;
            return new int[2] {col, row};
        }

        private void spreadsheetPanel_SelectionChanged(SpreadsheetPanel sender)
        {
            int row, col;
            sender.GetSelection(out col, out row);
            selectedCellName = String.Format("{0}{1}", (char) (col + 65), (row + 1));
            currentName.Text = selectedCellName;
            currentValue.Text = System.Convert.ToString(spreadsheet.GetCellValue(selectedCellName));
            currentContent.Text = System.Convert.ToString(spreadsheet.GetCellContents(selectedCellName));
        }

        /// <summary>
        /// Closes this tab. Checks if the spreadsheet needs to be saved and shows a dialog to do so if needed.
        /// </summary>
        /// <returns>If the close was cancelled.</returns>
        public bool Close()
        {
            if (!spreadsheet.Changed)
                return false;

            if (System.IO.File.Exists(filename))
            {
                DialogResult dr = MessageBox.Show(String.Format("The spreadsheet of file {0} has changed.\nDo you want to save thos changes?", filename),
                                "Spreadsheet changed", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Cancel)
                    return true;
                if (dr == DialogResult.Yes)
                    Save();
            }
            return false;
        }

        /// <summary>
        /// Saves the spreadsheet to a file. If forceNewFile is true or the filename property doesn't exists
        /// this method behaves as "Save As..." showing a dialog to select a file name.
        /// </summary>
        /// <param name="forceNewFile">If true the method behaves as "Save As..."</param>
        /// <returns>True if the users clicks "Cancel" on any of the dialogs shown by this method</returns>
        public bool Save(bool forceNewFile = false)
        {
            if (!System.IO.File.Exists(filename) || forceNewFile)
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Text file|*.txt";
                saveDialog.Title = "Save the spreadsheet";
                if (saveDialog.ShowDialog() == DialogResult.Cancel)
                    return true;
                filename = saveDialog.FileName;
                Text = filename;
            }
            spreadsheet.Save(filename);
            return false;
        }
    }
}
