﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpreadsheetGUI
{
    public partial class MainForm : Form
    {
        private int noNameCounter = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CreateNewTab();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (SpreadsheetTab tab in tabControl.Controls)
            {
                // Close each tab. If any returns true then cancel the form close.
                if (tab.Close())
                {
                    e.Cancel = true;
                    break;
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNewTab();
        }

        private void CreateNewTab(string name=null)
        {
            name = (name == null) ? String.Format("NoName{0}", ++noNameCounter) : name;

            SpreadsheetTab spreadsheetTab = new SpreadsheetTab(name);
            tabControl.Controls.Add(spreadsheetTab);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((SpreadsheetTab)tabControl.SelectedTab).Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((SpreadsheetTab)tabControl.SelectedTab).Save(true);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Text file|*.txt";
            openDialog.Title = "Save the spreadsheet";
            if (openDialog.ShowDialog() == DialogResult.Cancel)
                return;
            try
            {
                CreateNewTab(openDialog.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Error ocurred reading the content of file {0}.\n{1}", openDialog.FileName, ex.Message), "Spreadsheet read error");
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl.TabCount == 0)
                return;

            if (!((SpreadsheetTab)tabControl.SelectedTab).Close())
                tabControl.Controls.Remove(tabControl.SelectedTab);
        }
    }
}
