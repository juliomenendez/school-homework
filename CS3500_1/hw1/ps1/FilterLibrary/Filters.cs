﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilterLibrary
{
    /// <summary>
    /// Library with solutions for homework 1 of class CS3810
    /// </summary>
    public class Filters
    {
        /// <summary>
        /// Writes <paramref name="lines"/> to the console.
        /// </summary>
        /// <param name="lines">Lines to write to the console</param>
        public static void WriteToConsole(IEnumerable<String> lines)
        {
            foreach (String s in lines)
            {
                Console.WriteLine(s);
            }
        }

        /// <summary>
        /// Reads lines from the console.
        /// </summary>
        /// <returns>Lines read from the console</returns>
        public static IEnumerable<String> ReadFromConsole()
        {
            String line;
            while ((line = Console.ReadLine()) != null)
            {
                yield return line;
            }
        }

        /// <summary>
        /// Converts <paramref name="strings"/> to upper or lower case.
        /// </summary>
        /// <param name="strings">Strings to convert</param>
        /// <param name="toUpper">If true converts <paramref name="strings"/> to uppercase</param>
        /// <returns>Converted strings</returns>
        public static IEnumerable<String> ConvertCase(IEnumerable<String> strings, bool toUpper)
        {
            foreach (String current in strings)
	        {   
                if (toUpper)
		            yield return current.ToUpper();
                else
                    yield return current.ToLower();
 	        }
        }

        /// <summary>
        /// Returns those lines that are palindrome.
        /// </summary>
        /// <param name="strings">Strings to check</param>
        /// <returns>Strings that are palindrome</returns>
        public static IEnumerable<String> CheckPalindrome(IEnumerable<String> strings)
        {
            foreach (String current in strings)
            {
                String reversed = new String(current.Reverse().ToArray());
                if (current == reversed)
                    yield return current;
            }
        }

        /// <summary>
        /// Returns the permutations of each element of <paramref name="strings"/>
        /// </summary>
        /// <param name="strings">Strings to calculate permutations</param>
        /// <returns>Permutations of the characters in each element of <paramref name="strings"/></returns>
        public static IEnumerable<String> Permutations(IEnumerable<String> strings)
        {
            foreach (String str in strings)
                foreach (String permutation in GetPermutations(str))
                    yield return permutation;
	    }
    
        /// <summary>
        /// Calculates the permutations of <paramref name="source"/>
        /// </summary>
        /// <param name="source">String to calculate the permutations</param>
        /// <returns>Permutations of the characters in <paramref name="source"/></returns>
        private static IEnumerable<String> GetPermutations(String source)
        {
            if (source.Length <= 1)
                yield return source;
            else
            {
                for (int i = 0; i < source.Length; i++)
                {
                    String part1 = source.Substring(0, i);                 
                    String part2 = source.Substring(i + 1, source.Length - i - 1);
                    foreach (String permutation in GetPermutations(part2 + part1))
                        yield return source[i] + permutation;
                }
            }
        }
    }
}
