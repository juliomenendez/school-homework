﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FilterLibrary;

namespace CheckPalindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            Filters.WriteToConsole(Filters.CheckPalindrome(Filters.ReadFromConsole()));
        }
    }
}
