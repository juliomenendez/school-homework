﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FilterLibrary;

namespace ConvertCase
{
    class Program
    {
        /// <summary>
        /// Converts to upper or lower case the lines read from the console
        /// </summary>
        static void Main(string[] args)
        {
            Filters.WriteToConsole(Filters.ConvertCase(Filters.ReadFromConsole(), args.Length == 1 && args[0] == "upper"));
        }
    }
}
