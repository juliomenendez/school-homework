﻿// Written by Joe Zachary for CS 3500, August 2011

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FilterLibrary;

namespace FilterTester
{
    /// <summary>
    /// Interactive test driver for the FilterLibrary
    /// </summary>
    public class TestCode
    {
        /// <summary>
        /// Only one of these four test cases should be used per run.  The rest should stay
        /// commented out.
        /// </summary>
        public static void Main(string[] args)
        {
            // (1) This should read lines from the console and write them back with all letters
            // converted to upper case.
            //Filters.WriteToConsole(Filters.ConvertCase(Filters.ReadFromConsole(), true));

            // (2) This should read lines from the console and write them back with all letters
            // converted to lower case.
            //Filters.WriteToConsole(Filters.ConvertCase(Filters.ReadFromConsole(), false));

            // (3) This should read lines from the console but write back only those lines that
            // are palindromes (are exactly the same, including case, forwards and backwards).
            //Filters.WriteToConsole(Filters.CheckPalindrome(Filters.ReadFromConsole()));

            // (4) This should read lines from the console and, for every line read, write back
            // all of the permutations of that line.
            Filters.WriteToConsole(Filters.Permutations(Filters.ReadFromConsole()));

            Console.WriteLine("Press Enter to close");
            Console.ReadLine();
        }

    }

}
