<?php
require_once('Database.php');

/**
* Represents a user in the database.
*/
class User
{
	public $id;
	public $name;
	
	/**
	 * Creates the User instance. If the name is not in the table
	 * it creates a new row for it.
	 */
	public function User($id=null, $name=null)
	{
		$db = Database::getInstance();
		if($id == null) {
			$name = htmlspecialchars($name);
			$this->name = $name;
			$row = $db->getRow('ps10_User', "Name = ?", array($name));
			if($row == false) {
				$db->execute("INSERT INTO ps10_User(Name) VALUES(?)", array($name));
				$row = $db->getRow('ps10_User', "Name = ?", array($name));
			}
			$this->id = $row['id'];
		} else {
			$row = $db->getRow('ps10_User', "id = ?", array($id));
			if($row == false)
				throw Exception("Invalid User ID");
		
			$this->id = $row['id'];
			$this->name = $row['Name'];
		}
	}
	
	/**
	 * Redirects to the proper page depending on the type.
	 * @param string	$type	Either 'student' or 'teacher'.
	 */
	public function showInterfaceFor($type)
	{
		header("Location: $type.php");
		exit;
	}
	
	/**
	 * Returns the stored User instance in the session variables or redirects to the login
	 * page if it doesn't exists.
	 */
	public static function fromSession()
	{
		if(!isset($_SESSION['ps10_user']) || empty($_SESSION['ps10_user'])) {
			header('Location: index.php');
			exit;
		}
		return new User($_SESSION['ps10_user']);
	}
}

?>