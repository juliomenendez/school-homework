<?php
/**
* Handles all the database functionality. Uses the singleton methodology. 
*/
class Database
{
	private static $instance = null;
	private $connection;
	
	/**
	 * Creates a new instance. Initializes the database connection.
	 */
	private function Database()
	{
		$this->connection = new PDO('mysql:host=atr.eng.utah.edu;dbname=juliog', 'juliog', '00665929');
		$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	
	/**
	 * Returns the instance of this Database.
			 */
	public static function getInstance()
	{
		if(self::$instance == null)
			self::$instance = new Database();
		return self::$instance;
	}
	
	/**
	 * Returns the first row of $table. Can filter the result with a valid SQL
	 * condition in $where.
	 * 
	 * @param string	$table 	Name of the table.
	 * @param string	$where	Optional SQL conditions.
	 * @param array 	$params	Array of parameters to bind in the query.
	 */
	public function getRow($table, $where='', $params=array())
	{
		$sql = "SELECT * FROM $table";
		if($where != '')
			$sql .= " WHERE $where";
		$stmt = $this->connection->prepare($sql);
		foreach($params as $i => $value)
			$stmt->bindValue($i+1, $value);
		$stmt->execute();
		$result = $stmt->fetch();
		$stmt->closeCursor();
		return $result;
	}
	
	/**
	 * Returns an array with the ids from $table. Can filter the result 
	 * with valid SQL conditions in $where.
	 * 
	 * @param string	$table 	Name of the table.
	 * @param string	$where	Optional SQL conditions.
	 * @param array 	$params	Array of parameters to bind in the query.
	 */
 	public function getIds($table, $where='', $params=array())
	{
		$sql = "SELECT id FROM $table";
		if($where != '')
			$sql .= " WHERE $where";
		$rows = $this->getRows($sql, $params);
		$ids = array();
		foreach($rows as $row)
			$ids[] = $row['id'];
		return $ids;
	}
	
	/**
	 * Executes a SQL query and returns all the resulting rows.
	 * 
	 * @param string	$query	SQL query to execute.
	 * @param array	 	$params	Array of parameters to bind in the query.	
	 */
	public function getRows($query, $params=array())
	{
		$stmt = $this->connection->prepare($query);
		foreach($params as $i => $value)
			$stmt->bindValue($i+1, $value);
		$stmt->execute();
		$result = $stmt->fetchAll();
		$stmt->closeCursor();
		return $result;
	}
	
	public function execute($query, $params=array())
	{
		$stmt = $this->connection->prepare($query);
		foreach($params as $i => $value)
			$stmt->bindValue($i+1, $value);
		$stmt->execute();
		$stmt->closeCursor();
	}
	
	/**
	 * Inserts a new row in $table and returns the id of the new row.
	 *
	 * @param string	$table 	Table name.
	 * @param array 	$fields Fields whose values are specified in $values.
	 * @param array 	$values	Values for each field in $fields.
	 * @param array 	$params	Array of parameters to bind in the query.
	 */
	public function insert($table, $fields, $values, $params=array())
	{
		$fields_str = implode(', ', $fields);
		$values_str = implode(', ', $values);
		$sql = "INSERT INTO $table($fields_str)	VALUES($values_str)";
		$stmt = $this->connection->prepare($sql);
		foreach($params as $i => $value)
			$stmt->bindValue($i+1, $value);
		$stmt->execute();
		$inserted_id = $this->connection->lastInsertId();
		$stmt->closeCursor();
		return $inserted_id;
	}
	
	/**
	 * Updates a row in $table.
	 *
	 * @param string	$table 	Table name.
	 * @param array 	$fields Fields whose values are specified in $values.
	 * @param array 	$values	Values for each field in $fields.
	 * @param string	$where	Optional SQL conditions.
	 * @param array 	$params	Array of parameters to bind in the query.
	 */
	public function update($table, $fields, $values, $where='', $params=array())
	{
		$set_parts = array();
		$params_values = array();
		for($i=0; $i<count($fields); $i++) {
			$set_parts[] = "{$fields[$i]}=?";
			if($values[$i] != '?')
				$params_values[] = $values[$i];
		}
		$params = array_merge($params, $params_values);
		$set_str = implode(',', $set_parts);
		$sql = "UPDATE $table SET $set_str WHERE $where";
		$stmt = $this->connection->prepare($sql);
		foreach($params as $i => $value)
			$stmt->bindValue($i+1, $value);
		$stmt->execute();
		$stmt->closeCursor();
	}
}

?>