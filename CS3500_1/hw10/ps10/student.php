<?php
session_start();
require_once('User.php');
require_once('Quiz.php');

$user = User::fromSession();
if(isset($_GET['quiz']) && !empty($_GET['quiz']))
	$quiz = new Quiz($_GET['quiz']);

if(isset($_POST['submit'])) {
	if(isset($_POST['user_answer']) && !empty($_POST['user_answer'])) {
		$quiz->setUserAnswer($user->id, $_POST['user_answer']);
		header("Location: student.php");
	} else 
		$error = "Please select an answer";
}	
?>
<html>
	<head>
		<title>Student section - <?php echo $user->name; ?></title>
	</head>
	<body>
	<?php 
		if(isset($quiz)) {
	?>
	<?php if(isset($error)) { ?>
		<p><?php echo $error; ?></p>
	<?php } ?>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?quiz=<?php echo $quiz->id; ?>">
			<h4>Question:</h4><?php echo $quiz->question; ?>
			<fieldset>
				<legend>Answers:</legend>
				<dl>
				<?php foreach($quiz->answers as $answer) { ?>
					<dt><input type="radio" name="user_answer" value="<?php echo $answer->id; ?>" /></dt>
					<dd><?php echo $answer->text; ?></dd>
				<?php } ?>
				</dl>
			</fieldset>
			<input type="submit" name="submit" value="Submit Answer" id="submit">
		</form>
			
	<?php } else { ?>
		<h1>List of quizzes:</h1>
		<table cellpadding="2" cellspacing="2">
			<tr>
				<th>Title</th>
				<th>Teacher</th>
				<th>Score</th>
			</tr>
			
		<?php $total=0; $count=0; foreach(Quiz::all() as $quiz) { ?>
			<tr>
				<td><?php echo $quiz->title; ?></td>
				<td><?php echo $quiz->getTeacherName(); ?></td>
				<td>
				<?php 
				$score = $quiz->getUserScore($user);
				
				if($score!=null)
				{
					$count++;
					$total += $score;	
				}				 
				if($score == null) {
				?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?quiz=<?php echo $quiz->id; ?>">Answer</a>
				<?php } else { ?>
				<?php echo $score; ?>
				<?php } ?>
			    </td>
			</tr>
		<?php } ?>
			<tr>
				<th>Average</th>
				<th><?php echo ($count > 0) ? $total/$count : "Take A quiz!"; ?></th>
			</tr>
		</table>
	<?php } ?>
	</body>
</html>