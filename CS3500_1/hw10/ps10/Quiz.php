<?php
require_once('Database.php');
require_once('Answer.php');

/**
* Represents a quiz on the database.
*/
class Quiz
{
	private $db;
	public $id;
	public $title;
	public $question;
	public $author_id;
	public $correct_answer_id;
	public $answers;
	
	function Quiz($id=null, $title=null, $question=null, $user_id=null, $answers=null, $correct_answer_index=null)
	{
		$this->db = Database::getInstance();
		if($id != null) {
			$row = $this->db->getRow('ps10_Quiz', "id = ?", array($id));
			if($row == false)
				throw Exception("Invalid Quiz ID");
		
			$this->id = $row['id'];
			$this->title = $row['Title'];
			$this->question = $row['Question'];
			$this->author_id = $row['User_id'];
			$this->correct_answer_id = $row['Correct_Answer_id'];
			$this->loadAnswers();
		} else {
			$title = htmlspecialchars($title);
			$question = htmlspecialchars($question);
			$this->id = $this->db->insert('ps10_Quiz', array('Title', 'Question', 'User_id', 'Correct_Answer_id'),
				array('?', '?', '?', '?'), array($title, $question, $user_id, 1));
			$this->answers = array();
			foreach($answers as $text) {
				$text = htmlspecialchars($text);
				$this->answers[] = new Answer(null, $text, $this->id);
			}
			$this->db->update('ps10_Quiz', array('Correct_Answer_id'), 
				array('?'), "id = ?", array($this->answers[$correct_answer_index]->id, $this->id));
		}
	}
	
	private function loadAnswers()
	{
		$this->answers = array();
		foreach($this->db->getIds('ps10_Answer', "Quiz_id = ?", array($this->id)) as $answer_id)
			$this->answers[] = new Answer($answer_id);
	}
	
	public static function all($user_id=null)
	{
		$db = Database::getInstance();
		$all = array();
		$where = '';
		$params = array();
		if($user_id != null) {
			$where = 'User_id = ?';
			$params[] = $user_id;
		}
		foreach($db->getIds('ps10_Quiz', $where, $params) as $quiz_id)
			$all[] = new Quiz($quiz_id);
		return $all;
	}
	
	/**
	 * Calculates the score of $user on this quiz. Returns null of $user hasn't 
	 * taken this quiz.
	 * 
	 * @param User	$user	Instances of class User.
	 */
	public function getUserScore($user)
	{
		$row = $this->db->getRow('ps10_User_Completed_Quiz', "User_id = ? AND Quiz_id = ?", array($user->id, $this->id));
		if($row == false)
			return null;
		return ($row['Answer_id'] == $this->correct_answer_id) ? '100' : '0';
	}
	
	
	/**
	* Adds the user answer to the User Completed Quiz table.
	*
	* @param int $user_id 	Id of the user submitting their answer
	* @param int $user_answer 	integer representing the users answer for that quiz.
	*/
	public function setUserAnswer($user_id, $user_answer)
	{
		
		$this->db->insert("ps10_User_Completed_Quiz", array("User_id", "Quiz_id", "Answer_id"), array('?', '?', '?'), array($user_id, $this->id, $user_answer));
		
	}
	
	public function getTeacherName()
	{
		$user = new User($this->author_id);
		return $user->name;
	}
}

?>