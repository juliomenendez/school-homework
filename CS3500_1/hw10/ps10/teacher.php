<?php
error_reporting(-1);
session_start();
require_once('User.php');
require_once('Quiz.php');

$user = User::fromSession();
if(isset($_POST['submit'])) {
	if(!isset($_POST['title']) || empty($_POST['title']) || !isset($_POST['question']) || empty($_POST['question'])
		|| !isset($_POST['correct_answer']) || empty($_POST['correct_answer']) || !isset($_POST['answers']) 
		|| empty($_POST['answers'])) {	
		$error = "Please fill all the fields of the form";
	} else {
		new Quiz(null, $_POST['title'], $_POST['question'], $user->id, $_POST['answers'], $_POST['correct_answer']);
	}
}
?>
<html>
	<head>
		<title>Teacher section</title>
	</head>
	<body>
		<h1>List of quizzes:</h1>
		<?php $quizzes = Quiz::all($user->id); ?>
		<?php if(count($quizzes) > 0) { ?>
		<table>
			<tr>
				<th width="100">Title</th>
				<th>Question</th>
				
			</tr>
		<?php foreach($quizzes as $quiz) { ?>
			<tr>
				<td><?php echo $quiz->title; ?></td>
			    <td><?php echo $quiz->question; ?></td>
			</tr>
		<?php } ?>
		</table>
		<?php } else { ?>
			<h4>You don't have any quizzes.</h4>
		<?php } ?>
	<br/>
	<br/>
	<h1>New Quiz:</h1>
		<?php if(isset($error)) { ?>
			<p><?php echo $error; ?></p>
		<?php } ?>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<label for="title">Title:</label><input type="text" name="title" value="" id="title" />
			<label for="question">Question:</label><input type="text" name="question" value="" id="question" />
			<fieldset>
				<legend>Answers:</legend>
				<dl>
				<?php for($i=0; $i<4; $i++) { ?>
					<dt><input type="radio" name="correct_answer" value="<?php echo $i; ?>" /></dt>
					<dd><input type="text" name="answers[]" value="" id="answers" /></dd>
				<?php } ?>
				</dl>
			</fieldset>
			<input type="submit" name="submit" value="Create Quiz" id="submit">
		</form>
	</body>
</html>