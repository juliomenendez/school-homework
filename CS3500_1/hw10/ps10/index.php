<?php 
error_reporting(-1);
session_start();
require_once('User.php');


if(isset($_POST['submit'])) {
	if(!isset($_POST['name']) || empty($_POST['name'])) {
		$error = "Please enter a name.";
	} else {
		$user = new User(null, $_POST['name']);
		$_SESSION['ps10_user'] = $user->id;
		$user->showInterfaceFor($_POST['type']);
	}
}
?>
<html>
	<head>
		<title>CS3500 - PS10</title>
	</head>
	<body>
	<?php if(isset($error)) { ?>
		<p><?php echo $error; ?></p>
	<?php } ?>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<label for="name">Name</label><input type="text" name="name" value="" id="name">
			<select name="type" id="type">
				<option value="teacher">Teacher</option>
				<option value="student">Student</option>
			</select>
			<input type="submit" name="submit" value="Login" id="submit">
		</form>
	</body>
</html>