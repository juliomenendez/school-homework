<?php
require_once('Database.php');

/**
* Represents an answer in the database.
*/
class Answer
{
	public $id;
	public $text;
	public $quiz_id;
	
	/**
	 * Creates an instance.
	 * 
	 * @param int	$id	Id of the Answer being instantiated.
	 */
	function Answer($id=null, $text=null, $quiz_id=null)
	{
		$db = Database::getInstance();
		if($id != null) {
			$row = $db->getRow('ps10_Answer', "id = ?", array($id));
			if($row == false)
				throw Exception("Invalid Answer ID");
		
			$this->id = $row['id'];
			$this->text = $row['Text'];
			$this->quiz_id = $row['Quiz_id'];
		} else {
			$this->id = $db->insert('ps10_Answer', array('Text', 'Quiz_id'),
				array('?', '?'), array($text, $quiz_id));
			$this->text = $text;
			$this->quiz_id = $quiz_id;
		}
	}
}

?>