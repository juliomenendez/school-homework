﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatGUI
{
    public partial class ConnectForm : Form
    {
        public ConnectForm()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            string error = null;
            if (serverIP.Text.Length == 0 || serverPort.Text.Length == 0 || userName.Text.Length == 0)
            {
                error = "All fields are required.";
            }
            else
            {
                try
                {
                    int port = ServerPort;
                }
                catch
                {
                    error = "Invalid port number.";
                }
            }

            if (error != null)
            {
                MessageBox.Show(String.Format("{0}\nPlease try again.", error), "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.None;
            }
            else
                DialogResult = DialogResult.OK;
        }

        public string ServerIP
        {
            get
            {
                return serverIP.Text;
            }
        }

        public int ServerPort
        {
            get 
            {
                return Convert.ToInt16(serverPort.Text);
            } 
        }

        public string UserName
        {
            get
            {
                return userName.Text;
            }
        }
    }
}
