﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatGUI
{
    public partial class MainForm : Form
    {
        private ServerHandler serverHandler;
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Everytime the application is disconnected it disables the main form controls
        /// and shows the connect form.
        /// </summary>
        private void Connect()
        {
            ConnectForm connectForm = new ConnectForm();
            DialogResult dr = connectForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                serverHandler = new ServerHandler(connectForm.UserName, connectForm.ServerIP, connectForm.ServerPort);
                serverHandler.MessageReceived += new MessageReceivedHandler(serverHandler_MessageReceived);
                serverHandler.LoggedIn += new LoggedInHandler(serverHandler_LoggedIn);
                serverHandler.Joined += new JoinedHandler(serverHandler_Joined);
                serverHandler.Leaved += new LeavedHandler(serverHandler_Leaved);
                serverHandler.UserJoined += new UserJoinedHandler(serverHandler_UserJoined);
                serverHandler.UserLeaved += new UserLeavedHandler(serverHandler_UserLeaved);
                serverHandler.SessionCreated += new SessionCreatedHandler(serverHandler_SessionCreated);
                serverHandler.SessionClosed += new SessionClosedHandler(serverHandler_SessionClosed);
                serverHandler.Disconnected += new DisconnectedHandler(serverHandler_Disconnected);
                serverHandler.ExceptionRaised += new ExceptionRaisedHandler(serverHandler_ExceptionRaised);
                serverHandler.connect();
            }
            else
                Close();
        }

        void serverHandler_ExceptionRaised(string message)
        {
            MessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        void serverHandler_Disconnected(string reason)
        {
            Action op = () =>
            {
                serverConnectButton.Text = "Connect to server";
                serverHandler = null;
                updateFormTitle();
                enableControls(false);
                sessionList.Items.Clear();
                connectButton.Enabled = false;
                newSessionButton.Enabled = false;
                userList.Items.Clear();
                if (reason != null)
                {
                    MessageBox.Show(this, string.Format("Server disconnected.\n{0}", reason), "Server disconnected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                Connect();
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the SessionClosed event of the ServerHandler instance. Updates the session controls.
        /// </summary>
        /// <param name="name">name of the session closed</param>
        void serverHandler_SessionClosed(string name)
        {
            Action op = () => updateSessionControls(name, false);
            Invoke(op);
        }

        /// <summary>
        /// Handler for the SessionCreated event of the ServerHandler instance. Updates the session controls.
        /// </summary>
        /// <param name="name">name of the session created</param>
        /// <param name="mine">true if this session was created by me</param>
        void serverHandler_SessionCreated(string name, bool mine)
        {
            Action op = () => updateSessionControls(name, true);
            Invoke(op);
        }

        /// <summary>
        /// Helper method called from several event handlers to update the controls 
        /// related with the sessions. Basically updates the session list and enables 
        /// or disables the Connect button depending on the that list.
        /// </summary>
        /// <param name="sessionName">name of the session added/deleted</param>
        /// <param name="add">true if the session needs to be added to the list. false otherwise</param>
        void updateSessionControls(string sessionName, bool add = true)
        {
            if (add)
            {
                sessionList.Items.Add(sessionName);
                if(sessionName == serverHandler.Session)
                    sessionList.SetSelected(sessionList.Items.IndexOf(sessionName), true);
            }
            else
                sessionList.Items.Remove(sessionName);
            connectButton.Enabled = (sessionList.Items.Count > 0);
        }

        /// <summary>
        /// Handler for the UserLeaved event of the ServerHandler instance. Removes the username from 
        /// the user list.
        /// </summary>
        /// <param name="username">username leaving the session</param>
        void serverHandler_UserLeaved(string username)
        {
            Action op = () => userList.Items.Remove(username);
            Invoke(op);
        }

        /// <summary>
        /// Handler for the UserJoined event of the ServerHandler instance. Adds the username to the 
        /// user list.
        /// </summary>
        /// <param name="username">username joining the session</param>
        void serverHandler_UserJoined(string username)
        {
            Action op = () => userList.Items.Add(username);
            Invoke(op);
        }

        /// <summary>
        /// Handler for the Leaved event of the ServerHandler instance. Disables the controls
        /// that depends on a session.
        /// </summary>
        void serverHandler_Leaved()
        {
            Action op = () =>
            {
                userList.Items.Clear();
                updateFormTitle();
                enableControls(false);
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the Joined event of the ServerHandler instance. Enables the controls
        /// that depends on a session.
        /// </summary>
        /// <param name="name">name of the session joined</param>
        void serverHandler_Joined(string name)
        {
            Action op = () =>
            {
                updateFormTitle();
                enableControls();
            };
            Invoke(op);
        }

        /// <summary>
        /// Enables or disables the messaging controls.
        /// </summary>
        /// <param name="enable">if true enable the controls</param>
        void enableControls(bool enable = true)
        {
            messageInput.Enabled = enable;
            messageLog.Enabled = enable;
            messageLog.Text = "";
            sendMessage.Enabled = enable;
        }

        /// <summary>
        /// Handler for the LoggedIn event of the ServerHandler. Updates the main form title and
        /// enables the button to create new session.
        /// </summary>
        /// <param name="asUsername">username this client used to log in</param>
        void serverHandler_LoggedIn(string asUsername)
        {
            Action op = () =>
            {
                updateFormTitle();
                serverConnectButton.Text = "Disconnect from server";
                newSessionButton.Enabled = true;
            };
            Invoke(op);
        }

        /// <summary>
        /// Handler for the MessageReceived event of the ServerHandler. Adds the new message to the 
        /// message log control
        /// </summary>
        /// <param name="sender">username that sent the message</param>
        /// <param name="message">content of the message</param>
        void serverHandler_MessageReceived(string sender, string message)
        {
            Action op = () => updateMessageLog(sender, message);
            Invoke(op);
        }

        /// <summary>
        /// Adds a new message to the messageLog control. Usually call via Invoke.
        /// </summary>
        /// <param name="sender">user who sent the message</param>
        /// <param name="newMessage">new line to add to the messageLog control</param>
        void updateMessageLog(string sender, string newMessage)
        {
            messageLog.Text += string.Format("{0}: {1}\r\n", sender, newMessage);
        }

        /// <summary>
        /// Helper method to update the main form title.
        /// </summary>
        void updateFormTitle()
        {
            if(serverHandler != null)
                Text = string.Format("ChatGUI - Connected as {0}{1}", serverHandler.Username, 
                    (serverHandler.Session == null ? "" : (" in session " + serverHandler.Session)));
            else
                Text = "ChatGUI - Disconnected";
        }

        /// <summary>
        /// Handler for the Shown method of the main form. Shows the connect form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Shown(object sender, EventArgs e)
        {
            Connect();
        }

        /// <summary>
        /// Handler for the Click event of the create session button. Shows the 
        /// create session dialog. Makes the serverHandler create the session if needed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newSessionButton_Click(object sender, EventArgs e)
        {
            NewSessionForm sessionForm = new NewSessionForm();
            DialogResult dr = sessionForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                serverHandler.leaveSession();
                serverHandler.joinSession(sessionForm.SessionName);
            }
        }

        /// <summary>
        /// Handler for the Click event of the send message button. Sends the message and 
        /// clears the textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendMessage_Click(object sender, EventArgs e)
        {
            if (messageInput.Text.Length > 0)
            {
                serverHandler.sendMessage(messageInput.Text);
                messageInput.Text = "";
                messageInput.Focus();
            }
        }

        /// <summary>
        /// Handler for the Click event of the connect/disconnect button. If the selected
        /// session on the session list is the current active session then disconnects from it. 
        /// Otherwise disconnects from the current active session and connects to the selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connectButton_Click(object sender, EventArgs e)
        {
            string selectedSession = (string)sessionList.SelectedItem;
            if (serverHandler.Session != selectedSession)
            {
                if (serverHandler.Session != null)
                    serverHandler.leaveSession();
                serverHandler.joinSession(selectedSession);
            }
            else
                serverHandler.leaveSession();
        }

        /// <summary>
        /// Handler for the SelectedIndexChanged event of the session list. Changes the text of the 
        /// connect button to "Disconnect" if the new selected session is the same as the active 
        /// session, otherwise changes it to "Connect".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sessionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedSession = (string)sessionList.SelectedItem;
            if (serverHandler.Session == selectedSession)
                connectButton.Text = "Disconnect";
            else
                connectButton.Text = "Connect";
        }

        /// <summary>
        /// Handler for the Click event of the connect/disconnect button. If this client is connected then disconnect.
        /// Show the connect form otherwise.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serverConnectButton_Click(object sender, EventArgs e)
        {
            if (serverHandler != null)
                serverHandler.Disconnect();
            else
                Connect();
        }
    }
}
