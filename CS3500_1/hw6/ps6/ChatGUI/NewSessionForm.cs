﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatGUI
{
    public partial class NewSessionForm : Form
    {
        public NewSessionForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (name.Text.Length == 0)
            {
                MessageBox.Show("Name empty. Please try again.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.None;
            }
            else
                DialogResult = DialogResult.OK;
        }

        public string SessionName
        {
            get { return name.Text; }
        }
    }
}
