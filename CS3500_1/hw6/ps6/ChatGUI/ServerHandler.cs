﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;

namespace ChatGUI
{
    /// <summary>
    /// Delegate for the MessageReceived event of ServerHandler class. 
    /// Triggered when a new message arrives from the server.
    /// </summary>
    /// <param name="sender">username that sends the message</param>
    /// <param name="message">content of the message</param>
    public delegate void MessageReceivedHandler(string sender, string message);

    /// <summary>
    /// Delegate for the SessionCreated event of ServerHandler class.
    /// Triggered when a new session is created on the server.
    /// </summary>
    /// <param name="name">name of the new session</param>
    /// <param name="mine">true if the session was created by this client</param>
    public delegate void SessionCreatedHandler(string name, bool mine);

    /// <summary>
    /// Delegate for the Connected event of ServerHandler class.
    /// Triggered when the server connects successfully. 
    /// </summary>
    public delegate void ConnectedHandler();

    /// <summary>
    /// Delegate for the Disconnected event of the ServerHandler class.
    /// Triggered whenever the server disconnects.
    /// </summary>
    /// <param name="reason">the reason for the disconnection.</param>
    public delegate void DisconnectedHandler(string reason);

    /// <summary>
    /// Delegate for the UserJoined event of the ServerHandler class.
    /// Triggered when an user joins the current session of this client.
    /// </summary>
    /// <param name="username">username of the user</param>
    public delegate void UserJoinedHandler(string username);

    /// <summary>
    /// Delegate for the LoggedIn event of the ServerHandler class.
    /// Triggered when this client receives the confirmation that it was successfully 
    /// logged in to the server.
    /// </summary>
    /// <param name="asUsername">username used to log in</param>
    public delegate void LoggedInHandler(string asUsername);

    /// <summary>
    /// Delegate for the UserLeaved event of the ServerHandler class.
    /// Triggered when an user leaves the current session of this client.
    /// </summary>
    /// <param name="username">username of the user</param>
    public delegate void UserLeavedHandler(string username);

    /// <summary>
    /// Delegate for the SessionClosed event of the ServerHandler class.
    /// Triggered when a session is closed because no users are logged in it.
    /// </summary>
    /// <param name="name">name of the session</param>
    public delegate void SessionClosedHandler(string name);

    /// <summary>
    /// Delegate for the Leaved event of the ServerHandler class.
    /// Triggered when this client leaves the current session.
    /// </summary>
    public delegate void LeavedHandler();

    /// <summary>
    /// Delegate for the Joined event of the ServerHandler class.
    /// Triggered when this client joins a session.
    /// </summary>
    /// <param name="name">name of the session</param>
    public delegate void JoinedHandler(string name);

    /// <summary>
    /// Delegate for the ExceptionRaised event of the ServerHandler class.
    /// Triggered when there is an exception.
    /// </summary>
    /// <param name="message">message describing the exception</param>
    public delegate void ExceptionRaisedHandler(string message);

    class ServerHandler
    {
        private IPAddress address;
        private int port;
        private string username;
        private string session;
        private TcpClient tcpClient;
        private string readData;
        private byte[] readBuffer;

        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="username">Username to log in to the server</param>
        /// <param name="address">IP address of the server to connect. Defaults to 'localhost'</param>
        /// <param name="port">Port where the server is listening to. Defaults to 4000</param>
        public ServerHandler(string username, string address = "localhost", int port = 4000)
        {
            try
            {
                this.address = IPAddress.Parse(address == "localhost" ? "127.0.0.1" : address);
            }
            catch
            {
                TriggerDisconnected("Invalid IP address");
                return;
            }
            this.username = username;
            this.port = port;
            session = null;
            tcpClient = new TcpClient();
            readBuffer = new byte[tcpClient.ReceiveBufferSize];
            Connected += new ConnectedHandler(ServerHandler_Connected);
            Joined += new JoinedHandler(ServerHandler_Joined);
            Leaved += new LeavedHandler(ServerHandler_Leaved);
        }

        /// <summary>
        /// Handler for the Leaved event of this ServerHandler. Sets the session property to null.
        /// </summary>
        void ServerHandler_Leaved()
        {
            session = null;
        }

        /// <summary>
        /// Handler for the Joined event of this ServerHandler. Sets the session property to the 
        /// joined session name.
        /// </summary>
        /// <param name="name">name of the session</param>
        void ServerHandler_Joined(string name)
        {
            session = name;
        }

        /// <summary>
        /// Handler for the Connected event of this ServerHandler. Sends the username to be used
        /// by this client.
        /// </summary>
        void ServerHandler_Connected()
        {
            logIn();
        }

        /// <summary>
        /// Starts the async connect
        /// </summary>
        public void connect()
        {
            tcpClient.BeginConnect(address, port, tcpClientConnectCallback, null);
        }

        /// <summary>
        /// Method called from the TcpClient.BeginConnect call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void tcpClientConnectCallback(IAsyncResult result)
        {
            try
            {
                tcpClient.EndConnect(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(ex.ToString());
                return;
            }

            // Client connected.
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginRead call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void streamReadCallback(IAsyncResult result)
        {
            int readCount = 0;
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                readCount = stream.EndRead(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(string.Format("Error reading stream. {0}", ex.Message));
            }

            if (readCount == 0)
            {
                tcpClient.Close();
            }

            readData += Encoding.UTF8.GetString(readBuffer, 0, readCount);

            int index;
            while((index = readData.IndexOf(">\r\n")) > 0) 
            {
                string command = readData.Substring(0, index + 1);
                ProcessCommand(command);
                readData = readData.Substring(index + 1);
            }

            if(tcpClient.Connected)
                // Back to reading from the connection.
                stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);
        }

        /// <summary>
        /// Takes the string comming from the server, parses it and triggers the corresponding events.
        /// </summary>
        /// <param name="command">string comming from the server with a valid command</param>
        private void ProcessCommand(string command)
        {
            command = command.Trim();
            if (command[0] != '<' || command[command.Length - 1] != '>')
                return;

            // Removes < and > from the begining and end of the command
            command = command.Substring(1, command.Length - 2);
            string[] pieces = command.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (pieces.Length == 0)
                return;
            
            string keyword = pieces[0].ToLower();
            switch (keyword)
            {
                case "welcome":
                    if (Connected != null)
                        Connected();
                    break;
                case "quit":
                    TriggerDisconnected(null);
                    break;
                case "name":
                    if (LoggedIn != null)
                        LoggedIn(pieces[1]);
                    break;
                case "existing_session":
                case "starting_session":
                    if (SessionCreated != null)
                        SessionCreated(pieces[1], keyword == "session");
                    break;
                case "session_member":
                case "joining_session":
                    if (UserJoined != null)
                        UserJoined(pieces[1]);
                    break;
                case "session":
                    if (Joined != null)
                        Joined(pieces[1]);
                    break;
                case "leaving_session":
                    if (UserLeaved != null)
                        UserLeaved(pieces[1]);
                    break;
                case "message":
                    if (MessageReceived != null)
                    {
                        // Calculate the offset of the actual message being sent. That is
                        // the size of "message" + the size of the user name sending the 
                        // message plus 2 for the spaces joining "message", the user name
                        // and the message.
                        int headLength = pieces[0].Length + pieces[1].Length + 2;
                        string message = command.Substring(headLength);
                        MessageReceived(pieces[1], message);
                    }
                    break;
                case "ending_session":
                    if (SessionClosed != null)
                        SessionClosed(pieces[1]);
                    break;
                case "leave":
                    if (Leaved != null)
                        Leaved();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Helper method that triggers the Disconnected event.
        /// </summary>
        /// <param name="reason">reason of the disconnection</param>
        private void TriggerDisconnected(string reason)
        {
            if (Disconnected != null)
                Disconnected(reason);
        }

        /// <summary>
        /// Event definitions. For documentation about each event refer to the corresponding
        /// delegate.
        /// </summary>
        public event MessageReceivedHandler MessageReceived;
        public event SessionCreatedHandler SessionCreated;
        public event ConnectedHandler Connected;
        public event DisconnectedHandler Disconnected;
        public event UserJoinedHandler UserJoined;
        public event LoggedInHandler LoggedIn;
        public event UserLeavedHandler UserLeaved;
        public event SessionClosedHandler SessionClosed;
        public event LeavedHandler Leaved;
        public event JoinedHandler Joined;
        public event ExceptionRaisedHandler ExceptionRaised;

        /// <summary>
        /// Starts an async send to the server.
        /// </summary>
        /// <param name="command">command to send</param>
        private void sendCommand(string command)
        {
            byte[] outgoingBytes = Encoding.UTF8.GetBytes(command + "\n");
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginWrite(outgoingBytes, 0, outgoingBytes.Length, streamWriteCallback, null);
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginWrite call
        /// </summary>
        /// <param name="result">result of the async call</param>
        private void streamWriteCallback(IAsyncResult result)
        {
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                stream.EndWrite(result);
            }
            catch (Exception ex)
            {
                if (ExceptionRaised != null)
                    ExceptionRaised(string.Format("Error sending command to the server. {0}", ex.Message));
                else
                    throw new Exception("Error sending command to the server.", ex);
            }
        }

        /// <summary>
        /// Registers this clients with the username to use. 
        /// </summary>
        public void logIn()
        {
            sendCommand(string.Format("[name {0}]", username));
        }

        /// <summary>
        /// Joins a session. If the session doesn't exists the server creates it and 
        /// joins this client.
        /// </summary>
        /// <param name="name">name of the session to create</param>
        public void joinSession(string name)
        {
            sendCommand(string.Format("[session {0}]", name));
        }

        /// <summary>
        /// Leaves the current session.
        /// </summary>
        public void leaveSession()
        {
            sendCommand("[leave]");
        }

        /// <summary>
        /// Sends a message to the current session
        /// </summary>
        /// <param name="message">message to send</param>
        public void sendMessage(string message)
        {
            sendCommand(string.Format("[message {0}]", message));
        }

        public string Username
        {
            get { return username; }
        }

        public string Session
        {
            get { return session; }
        }

        /// <summary>
        /// Sends the QUIT command to the server
        /// </summary>
        public void Disconnect()
        {
            sendCommand("[quit]");
        }
    }
}
