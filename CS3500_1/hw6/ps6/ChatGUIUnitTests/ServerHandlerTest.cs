﻿using ChatGUI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.IO;

namespace ChatGUIUnitTests
{
    
    
    /// <summary>
    ///This is a test class for ServerHandlerTest and is intended
    ///to contain all ServerHandlerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ServerHandlerTest
    {
        private Random randomizer = new Random();
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        private string randomString(int size)
        {
            char[] buffer = new char[size];
            for (int i = 0; i < size; i++)
            {
                buffer[i] = chars[randomizer.Next(chars.Length)];
            }
            return new string(buffer);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            string target = Path.Combine(Directory.GetCurrentDirectory() + "ChatServer.exe");
            Process.Start("ChatServer");
        }
        
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            Process[] processes = Process.GetProcessesByName("ChatServer");
            if(processes.Length > 0)
                processes[0].Kill();
        }
        
        #endregion

        /// <summary>
        /// Helper method that returns an initialized ServerHandler_Accessor.
        /// </summary>
        /// <returns>a new instance of ServerHandler_Accessor</returns>
        private ServerHandler_Accessor GetHandlerConnected()
        {
            string username = randomString(7);
            string address = "localhost";
            int port = 4000;
            ServerHandler_Accessor serverHandler = new ServerHandler_Accessor(username, address, port);
            return serverHandler;
        }

        /// <summary>
        ///A test for ServerHandler Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void ServerHandlerConstructorTest()
        {
            string username = randomString(7);
            string address = "localhost";
            int port = 4000;
            ServerHandler_Accessor target = new ServerHandler_Accessor(username, address, port);
            Assert.AreEqual(username, target.Username);
            Assert.AreEqual(IPAddress.Parse("127.0.0.1"), target.address);
            Assert.AreEqual(port, target.port);
        }

        /// <summary>
        ///A test for Disconnect
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void DisconnectTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventDisconnect = new AutoResetEvent(false);
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            string disconnectReason = "";
            target.add_Connected(new ConnectedHandler(() =>
                {
                    resetEventConnect.Set();
                }));
            target.add_Disconnected(new DisconnectedHandler((string reason) =>
                {
                    disconnectReason = reason;
                    resetEventDisconnect.Set();
                }));
            target.connect();
            resetEventConnect.WaitOne();
            target.Disconnect();
            resetEventDisconnect.WaitOne();
            Assert.AreEqual(null, disconnectReason);
        }

        /// <summary>
        ///A test for Disconnect
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void DisconnectWithReasonTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventDisconnect = new AutoResetEvent(false);
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            string disconnectReason = "";
            target.add_Connected(new ConnectedHandler(() =>
                {
                    resetEventConnect.Set();
                }));
            target.add_Disconnected(new DisconnectedHandler((string reason) =>
                {
                    disconnectReason = reason;
                    resetEventDisconnect.Set();
                }));
            target.connect();
            resetEventConnect.WaitOne();
            Process[] processes = Process.GetProcessesByName("ChatServer");
            processes[0].Kill();
            resetEventDisconnect.WaitOne();
            Assert.IsTrue(disconnectReason.Length > 0);
        }

        /// <summary>
        ///A test for ServerHandler_Connected
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void ServerHandler_ConnectedTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            target.connect();
            resetEventConnect.WaitOne();
            Assert.IsTrue(target.tcpClient.Connected);
        }

        /// <summary>
        ///A test for ServerHandler_Joined
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void ServerHandler_JoinedTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventJoined = new AutoResetEvent(false);
            string joinedSessionName = "";
            target.add_Joined(new JoinedHandler((string sessionName) =>
                {
                    joinedSessionName = sessionName;
                    resetEventJoined.Set();
                }));
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            target.connect();
            resetEventConnect.WaitOne();
            string name = randomString(7);
            target.joinSession(name);
            resetEventJoined.WaitOne();
            Assert.AreEqual(target.Session, name);
            Assert.AreEqual(joinedSessionName, name);
        }

        /// <summary>
        ///A test for ServerHandler_Leaved
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void ServerHandler_LeavedTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventJoined = new AutoResetEvent(false);
            string joinedSessionName = "";
            target.add_Joined(new JoinedHandler((string sessionName) =>
            {
                joinedSessionName = sessionName;
                resetEventJoined.Set();
            }));
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            AutoResetEvent resetEventLeaved = new AutoResetEvent(false);
            target.add_Leaved(new LeavedHandler(() =>
                {
                    resetEventLeaved.Set();
                }));
            target.connect();
            resetEventConnect.WaitOne();
            string name = randomString(7);
            target.joinSession(name);
            resetEventJoined.WaitOne();
            target.leaveSession();
            resetEventLeaved.WaitOne();
            Assert.IsNull(target.Session);
        }

        /// <summary>
        ///A test for TriggerDisconnected
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void TriggerDisconnectedTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventDisconnect = new AutoResetEvent(false);
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            string disconnectReason = "";
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            target.add_Disconnected(new DisconnectedHandler((string reason) =>
            {
                disconnectReason = reason;
                resetEventDisconnect.Set();
            }));
            target.connect();
            resetEventConnect.WaitOne();
            string actualReason = "Unittest";
            target.TriggerDisconnected(actualReason);
            resetEventDisconnect.WaitOne();
            Assert.AreEqual(actualReason, disconnectReason);
        }

        /// <summary>
        ///A test for logIn
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void logInTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventLogin = new AutoResetEvent(false);
            string loggedUsername = "";
            target.add_LoggedIn(new LoggedInHandler((string username) =>
                {
                    loggedUsername = username;
                    resetEventLogin.Set();
                }));
            target.connect();
            // ServerHandler.Connected event calls to logIn internally.
            resetEventLogin.WaitOne();
            Assert.AreEqual(loggedUsername, target.Username);
        }

        /// <summary>
        ///A test for sendMessage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void sendMessageTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventJoined = new AutoResetEvent(false);
            target.add_Joined(new JoinedHandler((string sessionName) =>
            {
                resetEventJoined.Set();
            }));
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            AutoResetEvent resetEventMessage = new AutoResetEvent(false);
            string receivedMessage = null;
            string receivedUser = null;
            target.add_MessageReceived(new MessageReceivedHandler((string sender, string message) =>
                {
                    receivedMessage = message;
                    receivedUser = sender;
                    resetEventMessage.Set();
                }));
            target.connect();
            resetEventConnect.WaitOne();
            string name = randomString(7);
            target.joinSession(name);
            resetEventJoined.WaitOne();
            string actualMessage = randomString(255);
            target.sendMessage(actualMessage);
            resetEventMessage.WaitOne();
            Assert.AreEqual(actualMessage, receivedMessage);
            Assert.AreEqual(target.Username, receivedUser);
        }

        [TestMethod()]
        [DeploymentItem("ChatGUI.exe")]
        public void joinExistingSessionTest()
        {
            ServerHandler_Accessor target = GetHandlerConnected();
            AutoResetEvent resetEventJoined = new AutoResetEvent(false);
            target.add_Joined(new JoinedHandler((string sessionName) =>
            {
                resetEventJoined.Set();
            }));
            AutoResetEvent resetEventUserJoined = new AutoResetEvent(false);
            string joinedOtherUsername = "";
            target.add_UserJoined(new UserJoinedHandler((string otherUsername) =>
                {
                    joinedOtherUsername = otherUsername;
                    resetEventUserJoined.Set();
                }));
            AutoResetEvent resetEventUserLeaved = new AutoResetEvent(false);
            string leavedOtherUsername = "";
            target.add_UserLeaved(new UserLeavedHandler((string otherUsername) =>
                {
                    leavedOtherUsername = otherUsername;
                    resetEventUserLeaved.Set();
                }));
            AutoResetEvent resetEventConnect = new AutoResetEvent(false);
            target.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect.Set();
            }));
            target.connect();
            resetEventConnect.WaitOne();
            string actualSessionName = randomString(7);
            target.joinSession(actualSessionName);
            resetEventJoined.WaitOne();

            ServerHandler_Accessor target2 = GetHandlerConnected();
            AutoResetEvent resetEventJoined2 = new AutoResetEvent(false);
            target2.add_Joined(new JoinedHandler((string sessionName) =>
            {
                resetEventJoined2.Set();
            }));
            AutoResetEvent resetEventSessionCreated = new AutoResetEvent(false);
            string createdSessionName = null;
            bool createdSessionMine = true;
            target2.add_SessionCreated(new SessionCreatedHandler((string sessionName, bool mine) =>
            {
                createdSessionName = sessionName;
                createdSessionMine = mine;
                resetEventSessionCreated.Set();
            }));
            AutoResetEvent resetEventLeave = new AutoResetEvent(false);
            target2.add_Leaved(new LeavedHandler(() =>
                {
                    resetEventLeave.Set();
                }));
            AutoResetEvent resetEventConnect2 = new AutoResetEvent(false);
            target2.add_Connected(new ConnectedHandler(() =>
            {
                resetEventConnect2.Set();
            }));
            AutoResetEvent resetEventSessionClosed = new AutoResetEvent(false);
            string sessionClosedName = "";
            target2.add_SessionClosed(new SessionClosedHandler((string sessionName) =>
                {
                    sessionClosedName = sessionName;
                    resetEventSessionClosed.Set();
                }));
            target2.connect();
            resetEventConnect2.WaitOne();
            target2.joinSession(actualSessionName);
            resetEventJoined2.WaitOne();
            resetEventUserJoined.WaitOne();
            Assert.AreEqual(actualSessionName, createdSessionName);
            Assert.IsFalse(createdSessionMine);

            Assert.AreEqual(target2.Username, joinedOtherUsername);
            target2.leaveSession();
            resetEventLeave.WaitOne();
            resetEventUserLeaved.WaitOne();
            Assert.IsNull(target2.Session);
            Assert.AreEqual(target2.Username, leavedOtherUsername);

            target.leaveSession();
            resetEventSessionClosed.WaitOne();
            Assert.AreEqual(actualSessionName, sessionClosedName);
        }
    }
}
