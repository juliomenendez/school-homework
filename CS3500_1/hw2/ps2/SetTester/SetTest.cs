﻿using PS2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections;

namespace SetTester
{
    
    
    /// <summary>
    ///This is a test class for SetTest and is intended
    ///to contain all SetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SetTest
    {


        private TestContext testContextInstance;
        private Random randomizer = new Random();

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Set Constructor
        ///</summary>
        [TestMethod()]
        public void SetConstructorTest()
        {
            int n = randomizer.Next();
            Set target = new Set(n);
            Assert.AreEqual(target.Size, 1);
        }

        /// <summary>
        ///A test for Set Constructor
        ///</summary>
        [TestMethod()]
        public void SetConstructorTest1()
        {
            Set target = new Set();
            Assert.AreEqual(target.Size, 0);
        }

        /// <summary>
        ///A test for Contains
        ///</summary>
        [TestMethod()]
        public void ContainsTest()
        {
            int n1 = randomizer.Next();
            Set target = new Set(n1);
            Assert.IsTrue(target.Contains(n1));
            target.Remove(n1);
            Assert.IsFalse(target.Contains(n1));

            int count = randomizer.Next(10, 10000);
            while (count-- > 0)
            {
                int n = randomizer.Next();
                target.Insert(n);
                Assert.IsTrue(target.Contains(n));
            }
        }

        /// <summary>
        ///A test for Elements
        ///</summary>
        [TestMethod()]
        public void ElementsTest()
        {
            Set target = new Set();
            ArrayList elements = new ArrayList();
            int count = randomizer.Next(10, 10000);
            while (count-- > 0)
            {
                int n = randomizer.Next();
                elements.Add(n);
                target.Insert(n);
            }
            elements.Sort();
            int index = 0;
            foreach (int n in target.Elements())
            {
                Assert.AreEqual(elements[index++], n);
            }
        }

        /// <summary>
        ///A test for Elements enumerator inserting new element after first MoveNext
        ///</summary>
        [TestMethod()]
        public void ElementsEnumeratorWithInsertionTest()
        {
            Set target = new Set();
            int count = randomizer.Next(10, 10000);
            while (count-- > 0)
            {
                target.Insert(randomizer.Next());
            }

            IEnumerator enumerator = target.Elements().GetEnumerator();
            enumerator.MoveNext();
            target.Insert(randomizer.Next());
            Exception ex1 = null;
            try
            {
                enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                ex1 = ex;
            }
            Assert.IsInstanceOfType(ex1, typeof(InvalidOperationException));
        }

        /// <summary>
        ///A test for Elements enumerator deleting element after first MoveNext
        ///</summary>
        [TestMethod()]
        public void ElementsEnumeratorWithDeletionTest()
        {
            Set target = new Set();
            target.Insert(randomizer.Next());
            int rnd = randomizer.Next();
            target.Insert(rnd);
            target.Insert(randomizer.Next());
            IEnumerator enumerator = target.Elements().GetEnumerator();
            enumerator.MoveNext();
            target.Remove(rnd);
            Exception ex1 = null;
            try
            {
                enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                ex1 = ex;
            }
            Assert.IsInstanceOfType(ex1, typeof(InvalidOperationException));
        }


        /// <summary>
        ///A test for Elements enumerator inserting element before first MoveNext
        ///</summary>
        [TestMethod()]
        public void ElementsEnumeratorWithInsertionTest1()
        {
            Set target = new Set();
            IEnumerator enumerator = target.Elements().GetEnumerator();
            target.Insert(randomizer.Next());
            Exception ex1 = null;
            bool enumMove = false;
            try
            {
                enumMove = enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                ex1 = ex;
            }
            Assert.IsNotInstanceOfType(ex1, typeof(InvalidOperationException));
            Assert.IsTrue(enumMove);
        }

        /// <summary>
        ///A test for Elements enumerator deleting element before first MoveNext
        ///</summary>
        [TestMethod()]
        public void ElementsEnumeratorWithDeletionTest1()
        {
            Set target = new Set();
            IEnumerator enumerator = target.Elements().GetEnumerator();
            int n = randomizer.Next();
            target.Insert(randomizer.Next());
            target.Insert(n);
            target.Insert(randomizer.Next());
            target.Remove(n);
            Exception ex1 = null;
            bool enumMove = false;
            try
            {
                enumMove = enumerator.MoveNext();
            }
            catch (Exception ex)
            {
                ex1 = ex;
            }
            Assert.IsNotInstanceOfType(ex1, typeof(InvalidOperationException));
            Assert.IsTrue(enumMove);
        }

        /// <summary>
        ///A test for Insert
        ///</summary>
        [TestMethod()]
        public void InsertTest()
        {
            Set target = new Set();
            int count = randomizer.Next(10, 10000);
            ArrayList elements = new ArrayList();
            while (count-- > 0)
            {
                int n = randomizer.Next();
                elements.Add(n);
                target.Insert(n);
            }
            Assert.AreEqual(elements.Count, target.Size);
            foreach (int n in elements)
                Assert.IsTrue(target.Contains(n));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            Set target = new Set();
            int n = randomizer.Next();
            target.Insert(n);
            target.Remove(n);
            Assert.IsFalse(target.Contains(n));
            Assert.AreEqual(0, target.Size);
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringTest()
        {
            Set target = new Set();
            ArrayList elements = new ArrayList();
            int count = randomizer.Next(10, 1000);
            while (count-- > 0)
            {
                int n = randomizer.Next();
                target.Insert(n);
                elements.Add(n);
            }
            elements.Sort();
            string expected = "{";
            int i = 0;
            foreach(int n in elements)
            {
                expected += n.ToString();
                if (++i < elements.Count)
                    expected += ", ";
            }
            expected += "}";
            Assert.AreEqual(expected, target.ToString());
        }

        /// <summary>
        ///A test for Size
        ///</summary>
        [TestMethod()]
        public void SizeTest()
        {
            Set target = new Set();
            Assert.AreEqual(0, target.Size);
            int count = randomizer.Next(10, 10000);
            int count2 = count;
            while (count-- > 0)
            {
                int n = randomizer.Next();
                target.Insert(n);
            }
            Assert.AreEqual(count2, target.Size);
        }
    }
}
