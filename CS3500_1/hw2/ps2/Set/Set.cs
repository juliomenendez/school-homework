﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PS2
{
    public class Set
    {
        private int[] elements;
        private int count = 0;
        private bool iterating = false;
        private bool modifiedWhileIterating = false;

        /// <summary>
        /// Creates an empty Set
        /// </summary>
        public Set()
        {
            elements = new int[10];
        }

        /// <summary>
        /// Creates a Set with <paramref name="n"/> in it.
        /// </summary>
        /// <param name="n">Integer to be inserted</param>
        public Set(int n) : this()
        {
            Insert(n);
        }

        /// <summary>
        /// Number of elements in this Set
        /// </summary>
        public int Size 
        { 
            get { return count; } 
        }

        /// <summary>
        /// Finds out if <paramref name="n"/> is in this Set.
        /// </summary>
        /// <param name="n">Integer to search for</param>
        /// <returns>True if <paramref name="n"/>is in this Set</returns>
        public bool Contains(int n)
        {
            int pos = searchElement(n);
            return pos < count && elements[pos] == n;
        }

        /// <summary>
        /// Searchs for <paramref name="n"/> in the elements of this Set using binary search.
        /// If <paramref name="n"/> don't exists then the returned indexed represents where it
        /// should be if it existed.
        /// </summary>
        /// <param name="n">Integer to search for</param>
        /// <returns>Index of <paramref name="n"/> if it exists. Index where it should be inserted if not exists</returns>
        private int searchElement(int n)
        {
            int start = 0;
            int end = count - 1;
            int middle;

            while (start <= end)
            {
                middle = start + (end - start) / 2;
                if (n == elements[middle])
                    return middle;
                if (n > elements[middle])
                    start = middle + 1;
                else
                    end = middle - 1;
            }
            if (start < count && n > elements[start])
                return start + 1;
            return start;
        }

        /// <summary>
        /// Inserts <paramref name="n"/> in this Set and updates the Size of it. 
        /// If <paramref name="n"/> already exists it's not inserted.
        /// </summary>
        /// <param name="n">Integer to insert</param>
        public void Insert(int n)
        {
            if (iterating)
                modifiedWhileIterating = true;
            int pos = searchElement(n);
            if (pos > count - 1 || elements[pos] != n)
            {
                int newSize = count == elements.Length ? elements.Length + elements.Length / 2 : elements.Length;
                int[] newElements = new int[newSize];
                for(int i=0; i<pos; i++)
                {
                    newElements[i] = elements[i];
                }
                newElements[pos] = n;
                for(int i=pos; i<count; i++)
                {
                    newElements[i + 1] = elements[i];
                }   
                elements = newElements;
                count++;
            }
        }

        /// <summary>
        /// Removes <paramref name="n"/> from this Set if it exists.
        /// </summary>
        /// <param name="n">Integer to remove</param>
        public void Remove(int n)
        {
            if (iterating)
                modifiedWhileIterating = true;
            if (Contains(n))
            {
                int[] newElements = new int[elements.Length];
                int currentIndex = 0;
                foreach(int current in elements)
                {
                    if (current != n)
                        newElements[currentIndex] = current;
                    currentIndex++;
                }
                elements = newElements;
                count--;
            }
        }

        /// <summary>
        /// Returns the string representation of this Set
        /// </summary>
        /// <returns>String that represents this Set</returns>
        public override string ToString()
        {
            String res = "{";
            for (int i = 0; i < count; i++)
            {
                res += elements[i].ToString();
                if (i < count - 1)
                    res += ", ";
            }
            return res + "}";
        }

        /// <summary>
        /// Returns a iterable of the elements in this Set in ascending order
        /// </summary>
        /// <returns>IEnumerable with the integers in this Set</returns>
        public IEnumerable<int> Elements()
        {
            modifiedWhileIterating = false;
            for (int i = 0; i < count; i++)
            {
                if (modifiedWhileIterating)
                    throw new InvalidOperationException();
                else
                {
                    iterating = true;
                    yield return elements[i];
                }
            }
        }
    }
}
