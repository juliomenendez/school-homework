﻿// Written by Joe Zachary for CS 3500, September 2011
// Version 1.6

// Change History:

// 9/15/11 10:28 a.m. 
// Added paragraph to AbstractSpreadsheet explaining about empty spreadsheets
// and empty cells.

// 9/15/11 5:24 p.m.
// Added exception documentation to Read and Save methods

// 9/16/11 9:45 a.m.
// Clarified specification of Save method. 
// Added new version of GetCellsToRecalculate.

// 9/16/11 10:32 p.m.
// Clarified GetCellContents slightly

// 9/17/11 7:54 p.m.
// Fixed typo

// 9/19/11 9:43 p.m.
// Removed extraneous braces in Visit method that completely changed
// the behavior of the method.  Thanks Spencer Phippen!



using System;
using System.IO;
using System.Collections.Generic;

namespace SS
{


    /// <summary>
    /// Thrown to indicate that a change to a cell will cause a circular dependency.
    /// </summary>
    public class CircularException : Exception
    {
    }


    /// <summary>
    /// Thrown to report that a formula isn't formatted properly.
    /// The inherited Message property should give details.
    /// </summary>
    public abstract class BadFormulaException : Exception
    {
    }


    /// <summary>
    /// Thrown to indicate that an attempt to write a spreadsheet to a file failed.
    /// The inherited Message property should give details.
    /// </summary>
    public abstract class SpreadsheetWriteException : Exception
    {
    }


    /// <summary>
    /// Thrown to indicate that an attempt to read a spreadsheet from a file failed.
    /// The inherited Message property should give details.
    /// </summary>
    public abstract class SpreadsheetReadException : Exception
    {
    }


    /// <summary>
    /// Used as the value of a cell that contains a formula whose evaluation
    /// causes an error (such as division by zero).  Please note: This is not
    /// an exception, which is why it is not derived from Exception.
    /// </summary>
    public abstract class FormulaError
    {
        /// <summary>
        /// The reason for the FormulaError
        /// </summary>
        public abstract String Reason { get; }
    }



    /// <summary>
    /// An AbstractSpreadsheet object represents the state of a simple spreadsheet.  All of
    /// the spreadsheets that you have seen have probably consisted of cells arranged in
    /// a rectangular grid.  The spreadsheets that this class represents consist of named
    /// cells, but the cells are not necessarily arranged into rows and columns.
    /// 
    /// 
    /// A cell has a contents and a value.  The contents of a cell can be either a string
    /// or a formula.  (By analogy, the contents of a cell in Excel is what is displayed on
    /// the editing line when the cell is selected.)  
    /// 
    /// The value of a cell can be one of three things: a string, a floating-point number,
    /// or an error.  (By analogy, the value of an Excel cell is what is displayed in that
    /// cell's position in the grid.)
    /// 
    /// In an empty spreadsheet, every possible name corresponds to an empty cell.  The contents
    /// and the value of an empty cell are both empty strings.  This means that no cell name can             ADDED 9/15/11 10:27 a.m.
    /// ever be undefined.
    /// 
    /// If a cell's contents is a string that can be parsed as a C# double, its value is
    /// that double.  Otherwise, its value is the string.  If a cell's contents is a formula,
    /// its value is the value of the formula.  This will either be a double or an error;
    /// see below for details.
    /// 
    /// The amount of memory required to store a Spreadsheet object must be proportional
    /// to the number of formulas and non-empty strings it contains.  This means that cells
    /// containing empty strings should not be stored in the underlying data structure.
    /// 
    /// The syntax of formulas is inspired by Microsoft Excel, but for ease of programming
    /// uses postfix instead of infix notation.  A formula is a postfix expression composed 
    /// of the four binary operator symbols (+, -, *, /), numeric constants (written using 
    /// C# double syntax), and cell names (written using C# variable name syntax).  All 
    /// tokens must be separated by one or more spaces.
    /// 
    /// Here are some examples:
    /// 2 HELLO +                (Adds 2 and the value of cell HELLO)
    /// A1 B1 + 2 /              (Averages the values of cells A1 and A2)
    /// A B C D E + + + +        (Adds the values of cells A, B, C, D, and E)
    /// 
    /// If a formula depends on a cell whose value is not a number, the formula has an
    /// error value.  If evaluating a formula requires division by zero, the formula has
    /// an error value.  Otherwise, the value of the formula will be a double.
    /// </summary>
    public abstract class AbstractSpreadsheet
    {

        /// <summary>
        /// Writes the spreadsheet to the specified file.  Throws a SpreadsheetWriteException
        /// if the operation is unsuccessful.  If there are n non-empty cells in the spreadsheet,
        /// the written file should consist of 2n lines.  Each pair of lines should consist of
        /// the name of a cell on one line and the contents of that cell on the next.
        /// 
        /// The lines that describe a cell must come after the lines that describe the
        /// non-empty cells on which it depends.                                                             CLARIFIED 9/16/11 9:45 a.m.
        /// 
        /// For example, suppose
        /// A contains = D
        /// B contains 8
        /// C contains Hello world!
        /// D contains = B B *
        /// 
        /// The written file should look like this (although the entry for C could be moved
        /// elsewhere, since nothing depends on it):
        /// B
        /// 8
        /// C
        /// Hello world!
        /// D
        /// = B B *
        /// A
        /// = D
        /// </summary>
        /// <param name="filename">File to write to</param>
        /// <exception cref="SpreadsheetWriteException">If save is unsuccessful</exception>
        public abstract void Save(String filename);


        /// <summary>
        /// Creates and returns a new spreadsheet that is intialized using the information
        /// in the named file.  The information must be formatted and organized as specified
        /// in the description of the Save method.  Throws a SpreadsheetReadException if
        /// the file cannot be read or if it is formatted or organized incorrectly.
        /// </summary>
        /// <param name="filename">File to read from</param>
        /// <exception cref="SpreadsheetReadException">If read is unsuccessful</exception>
        public abstract AbstractSpreadsheet Read(String filename);



        /// <summary>
        /// Enumerates the names of all the non-empty cells in the spreadsheet.
        /// </summary>
        /// <returns>Enumeration of non-empty cell names</returns>
        public abstract IEnumerable<String> GetAllCellNames();



        /// <summary>
        /// Has the spreadsheet changed since creation or the last successful write
        /// (whichever is most recent)?
        /// </summary>
        public abstract bool Changed
        {
            get;
            protected set;
        }


        /// <summary>
        /// Returns the value (as opposed to the contents) of the named cell.  See the class comment
        /// for the distinction between cell value and cell contents.
        /// 
        /// If the cell is empty, its value is an empty string.
        /// If the cell contains a string that can be parsed as a C# double, its value is that double.
        /// If the cell contains any other string, its value is that string.
        /// If the cell contains a formula, its value is the result of evaluating the formula.  That
        /// requires a bit more explanation.
        /// 
        /// Here is a recursive syntax for postfix formulas, along with the rule for evaluating each
        /// type of formula.  Here, [name] stands for a cell name (written in C# variable syntax),
        /// [number] stands for a double (written in C# double syntax), and [formula1] and [formula2]
        /// stand for formulas.
        /// 
        /// [number]                       The double to which [number] parses
        /// 
        /// [name]                         The value of the cell named "name"
        /// 
        /// [formula1] [formula2] +        If the values of [formula1] and [formula2] are doubles,
        ///                                their sum.  Otherwise, a FormulaError object.
        ///                                
        /// [formula1] [formula2] -        If the values of [formula1] and [formula2] are doubles,
        ///                                their difference.  Otherwise, a FormulaError object.   
        ///                                
        /// [formula1] [formula2] *        If the values of [formula1] and [formula2] are doubles,
        ///                                their product.  Otherwise, a FormulaError object. 
        ///                                
        /// [formula1] [formula2] /        If the values of [formula1] and [formula2] are doubles,
        ///                                a FormulaError object (if the value of [formula2] is 0)
        ///                                or their quotient.  Otherwise, a FormulaError object.
        /// </summary>
        /// <param name="name">Cell name</param>
        /// <returns>Value of named cell</returns>
        public abstract Object GetCellValue(String name);


        /// <summary>
        /// Returns the contents (as opposed to the value) of the named cell (in string form).  See the class comment
        /// for the distinction between cell value and cell contents.
        /// 
        /// If the cell is empty, its contents (in string form) is an empty string.
        /// If the cell contains a string, its contents (in string form) is that string.
        /// If the cell contains a formula, its contents (in string form) is that formula written in string form
        /// with "= " prepended.
        /// </summary>
        /// <param name="name">Cell name</param>
        /// <returns>Cell contents (in string form)</returns>
        public abstract String GetCellContents(String name);


        /// <summary> 
        /// If newContents begins with "=" but the remainder cannot be parsed as a formula,
        /// throws a BadFormulaException and makes no change to the spreadsheet.  See the
        /// documentation for GetCellValue for the specification of formula syntax.
        /// 
        /// If newContents begins with "=" and the remainder can be parsed as a formula, but
        /// the formula's insertion into the named cell would cause a circular
        /// dependency, throws a CircularException and makes no change to the spreadsheet.
        /// A circular dependency occurs when a formula, either directly or indirectly,
        /// depends on its own value.
        /// 
        /// Otherwise, uses newContents to change the contents of the named cell.
        /// 
        /// If newContents is empty, the cell becomes empty.
        /// If newContents begins with "=", the cell contains the formula that follows.
        /// Otherwise, the cell contains newContents.
        /// 
        /// The method returns an enumeration containing name plus the names of all
        /// other cells whose value depends, directly or indirectly, on the named cell.
        /// </summary>
        /// <param name="name">Name of cell being changed</param>
        /// <param name="newContents">New value of named cell</param>
        /// <returns>Enumeration of names of cells that depend on the named cell</returns>
        /// <exception cref="BadFormulaException">If formula can't be parsed</exception>
        /// <exception cref="CircularException">If formula would cause circular dependency</exception>
        public abstract IEnumerable<String> SetCellContents(String name, String newContents);


        /// <summary>
        /// Returns an enumeration of all the addresses whose values depend directly on the
        /// value of the named cell.  In other words, returns an enumeration of all the
        /// names of cells that contain formulas containing name.
        /// 
        /// For example, suppose that
        /// A contains 3
        /// B contains A A *
        /// C contains B A +
        /// D contains B C +
        /// The dependents of A are B and C
        /// </summary>
        /// <param name="name">Name of cell</param>
        /// <returns>Enumeration of direct dependents</returns>
        protected abstract IEnumerable<String> GetDependents(String name);


        /// <summary>
        /// A convenience method for invoking the other version of GetCellsToRecalculate
        /// with a singleton set of names.
        /// </summary>
        /// <param name="name">Name of cell that has changed</param>
        /// <returns>Enumeration of names of cells that need to be recalculated</returns>
        /// <exception cref="CircularException">If circular dependency is discovered</exception>
        /// <returns></returns>
        protected IEnumerable<String> GetCellsToRecalculate(String name)
        {
            return GetCellsToRecalculate(new List<String>() { name });
        }


        /// <summary>
        /// Given the names of one or more cells that have changed, returns an                               MODIFIED 9/16/11 9:45 a.m.
        /// enumeration of the names of all cells whose values must be recalculated.
        /// Furthermore, the names are enumerated in the order in which
        /// the calculations should be done.  If a circular dependency is
        /// discovered, a CircularException is thrown.
        /// 
        /// For example, suppose that 
        /// A contains 5
        /// B contains 7
        /// C contains A B +
        /// D contains A C *
        /// E contains 15
        /// 
        /// If A and B have changed, then A, B, and C, and D must be recalculated,
        /// and they must be recalculated in either the order ABCD or BACD.  The
        /// method will produce one of those enumerations.
        /// 
        /// Please note that this method depends on the abstract GetDependents.
        /// It won't work until GetDependents is implemented correctly.
        /// </summary>
        /// <param name="name">Name of cell that has changed</param>
        /// <returns>Enumeration of names of cells that need to be recalculated</returns>
        /// <exception cref="CircularException">If circular dependency is discovered</exception>
        /// 

        protected IEnumerable<String> GetCellsToRecalculate(IEnumerable<String> names)
        {
            LinkedList<String> changed = new LinkedList<String>();
            HashSet<String> visited = new HashSet<String>();
            foreach (String name in names)
            {
                if (!visited.Contains(name))
                {
                    Visit(name, name, visited, changed);
                }
            }
            return changed;
        }


        // A helper for the GetCellsToRecalculate method
        private void Visit(String start, String name, HashSet<String> visited, LinkedList<String> changed)
        {
            visited.Add(name);
            foreach (String n in GetDependents(name))
            {
                if (n.Equals(start))
                {
                    throw new CircularException();
                }
                else if (!visited.Contains(n))
                {
                    Visit(start, n, visited, changed);
                }
            }
            changed.AddFirst(name);
        }


    }
}

