﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace SpreadsheetTests
{
    
    
    /// <summary>
    ///This is a test class for CellTest and is intended
    ///to contain all CellTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CellTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Cell Constructor
        ///</summary>
        [TestMethod()]
        public void CellConstructorTest()
        {
            string name = "A";
            string content = "1.2";
            Cell target = new Cell(name, content);
            Assert.AreEqual(target.Name, name);
            Assert.AreEqual(target.Content, content);
        }

        /// <summary>
        ///A test for Cell Constructor
        ///</summary>
        [TestMethod()]
        public void CellConstructorTest1()
        {
            string name = "A";
            Cell target = new Cell(name);
            Assert.AreEqual(target.Name, name);
            Assert.AreEqual(target.Content, "");
        }

        /// <summary>
        ///A test for AddDependent
        ///</summary>
        [TestMethod()]
        public void AddDependentTest()
        {
            Cell target = new Cell("A");
            Cell dependent = new Cell("B");
            target.AddDependent(dependent);
            IEnumerable<Cell> dependents = target.GetDependents();
            Assert.IsTrue(dependents.ToList().Contains(dependent));
        }

        /// <summary>
        ///A test for GetDependents
        ///</summary>
        [TestMethod()]
        public void GetDependentsTest()
        {
            Cell target = new Cell("A");
            Cell dependent1 = new Cell("B");
            Cell dependent2 = new Cell("C");
            Cell dependent3 = new Cell("D");
            target.AddDependent(dependent1);
            target.AddDependent(dependent2);
            target.AddDependent(dependent3);
            List<Cell> dependents = target.GetDependents().ToList();
            Assert.AreEqual(dependents.Count, 3);
            Assert.IsTrue(dependents.Contains(dependent1));
            Assert.IsTrue(dependents.Contains(dependent2));
            Assert.IsTrue(dependents.Contains(dependent3));
        }

        /// <summary>
        ///A test for RemoveDependent
        ///</summary>
        [TestMethod()]
        public void RemoveDependentTest()
        {
            Cell target = new Cell("A");
            Cell dependent1 = new Cell("B");
            Cell dependent2 = new Cell("C");
            Cell dependent3 = new Cell("D");
            target.AddDependent(dependent1);
            target.AddDependent(dependent2);
            target.AddDependent(dependent3);
            target.RemoveDependent(dependent2);
            List<Cell> dependents = target.GetDependents().ToList();
            Assert.AreEqual(dependents.Count, 2);
            Assert.IsTrue(dependents.Contains(dependent1));
            Assert.IsFalse(dependents.Contains(dependent2));
            Assert.IsTrue(dependents.Contains(dependent3));
        }

        /// <summary>
        ///A test for Content
        ///</summary>
        [TestMethod()]
        public void ContentTest()
        {
            Cell target = new Cell("A", "some text");
            Assert.AreEqual(target.Content, "some text");
            target.Content = "1.2";
            Assert.AreEqual(target.Content, "1.2");
            target.Content = "A B +";
            Assert.AreEqual(target.Content, "A B +");
        }

        /// <summary>
        ///A test for Value
        ///</summary>
        [TestMethod()]
        public void ValueTest()
        {
            Cell target = new Cell("A");
            target.Value = 1.2;
            Assert.AreEqual(target.Value, 1.2);
            target.Value = "random text";
            Assert.AreEqual(target.Value, "random text");
        }
        
        /// <summary>
        ///A test for Value with Formula
        ///</summary>
        [TestMethod()]
        public void ValueWithFormulaTest()
        {
            Cell target = new Cell("A");
            target.Value = new Formula("1 2 +", new Dictionary<string, Cell>());
            Assert.AreEqual(target.Value, 3.0);
        }
    }
}
