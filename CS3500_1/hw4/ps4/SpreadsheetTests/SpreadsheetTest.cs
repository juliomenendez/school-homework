﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SpreadsheetTests
{
    
    
    /// <summary>
    ///This is a test class for SpreadsheetTest and is intended
    ///to contain all SpreadsheetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SpreadsheetTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Spreadsheet Constructor
        ///</summary>
        [TestMethod()]
        public void SpreadsheetConstructorTest()
        {
            Spreadsheet target = new Spreadsheet();
            Assert.IsFalse(target.Changed);
            Assert.AreEqual(target.GetAllCellNames().ToList().Count, 0);
        }

        /// <summary>
        ///A test for GetAllCellNames
        ///</summary>
        [TestMethod()]
        public void GetAllCellNamesTest()
        {
            Spreadsheet target = new Spreadsheet();
            target.SetCellContents("A", "1.2");
            target.SetCellContents("B", "= A 1 +");
            target.SetCellContents("C", "45");
            target.SetCellContents("D", "= C B /");
            List<string> names = target.GetAllCellNames().ToList();
            Assert.AreEqual(names.Count, 4);
            Assert.IsTrue(names.Contains("A"));
            Assert.IsTrue(names.Contains("B"));
            Assert.IsTrue(names.Contains("C"));
            Assert.IsTrue(names.Contains("D"));
        }

        /// <summary>
        ///A test for GetCellContents
        ///</summary>
        [TestMethod()]
        public void GetCellContentsTest()
        {
            Spreadsheet target = new Spreadsheet();
            target.SetCellContents("A", "just text");
            Assert.AreEqual(target.GetCellContents("A"), "just text");
            target.SetCellContents("B", "1.2");
            Assert.AreEqual(target.GetCellContents("B"), "1.2");
            target.SetCellContents("C", "= B B *");
            Assert.AreEqual(target.GetCellContents("C"), "= B B *");
            Assert.AreEqual(target.GetCellContents("E"), "");
        }

        /// <summary>
        ///A test for GetCellValue
        ///</summary>
        [TestMethod()]
        public void GetCellValueTest()
        {
            Spreadsheet target = new Spreadsheet();
            target.SetCellContents("A", "just text");
            Assert.AreEqual(target.GetCellValue("A"), "just text");
            target.SetCellContents("B", "1.2");
            Assert.AreEqual(target.GetCellValue("B"), 1.2);
            target.SetCellContents("C", "= B B *");
            Assert.AreEqual(target.GetCellValue("C"), 1.2 * 1.2);
            target.SetCellContents("D", "= C B / 100 +");
            Assert.AreEqual(target.GetCellValue("D"), 101.2);
        }

        /// <summary>
        ///A test for Read
        ///</summary>
        [TestMethod()]
        public void ReadTest()
        {
            Spreadsheet target = new Spreadsheet();
            string[] lines = {"A", "100", "B", "= A 5.6 +", "C", "just a text", "D", "= A B /"};
            System.IO.File.WriteAllLines("readTest.txt", lines);
            Spreadsheet ss2 = (Spreadsheet)target.Read("C:\\readTest.txt");
            Assert.AreEqual(ss2.GetCellContents("A"), "100");
            Assert.AreEqual(ss2.GetCellContents("B"), "= A 5.6 +");
            Assert.AreEqual(ss2.GetCellContents("C"), "just a text");
            Assert.AreEqual(ss2.GetCellContents("D"), "= A B /");
        }

        /// <summary>
        ///A test for Save
        ///</summary>
        [TestMethod()]
        public void SaveTest()
        {
            Spreadsheet target = new Spreadsheet();
            target.SetCellContents("A", "1.2");
            target.SetCellContents("B", "= A 1 +");
            target.SetCellContents("C", "45");
            target.SetCellContents("D", "= C B /");
            target.Save("C:\\saveTest.txt");

            Spreadsheet ss2 = (Spreadsheet) target.Read("C:\\saveTest.txt");
            Assert.AreEqual(ss2.GetCellContents("A"), "1.2");
            Assert.AreEqual(ss2.GetCellContents("B"), "= A 1 +");
            Assert.AreEqual(ss2.GetCellContents("C"), "45");
            Assert.AreEqual(ss2.GetCellContents("D"), "= C B /");
            Assert.AreNotSame(ss2, target);
        }

        /// <summary>
        ///A test for SetCellContents
        ///</summary>
        [TestMethod()]
        public void SetCellContentsTest()
        {
            Spreadsheet target = new Spreadsheet();
            List<string> deps = target.SetCellContents("A", "1.34").ToList();
            Assert.AreEqual(deps.Count, 0);
            Assert.AreEqual(target.GetCellContents("A"), "1.34");
            deps = target.SetCellContents("B", "= A 1 +").ToList();
            Assert.AreEqual(0, deps.Count);
            deps = target.SetCellContents("A", "2").ToList();
            Assert.AreEqual(1, deps.Count);
            Exception ex1 = null;
            try
            {
                deps = target.SetCellContents("C", "= C 1 +").ToList();
            }
            catch (Exception e1)
            {
                ex1 = e1;
            }
            Assert.IsInstanceOfType(ex1, typeof(CircularException));
            Assert.AreEqual(target.GetCellContents("C"), "");
        }

        /// <summary>
        ///A test for Changed
        ///</summary>
        [TestMethod()]
        public void ChangedTest()
        {
            Spreadsheet target = new Spreadsheet();
            Assert.IsFalse(target.Changed);
            target.SetCellContents("A", "some text");
            Assert.IsTrue(target.Changed);
            target.Save("C:\\changedTest.txt");
            Assert.IsFalse(target.Changed);
        }


        /// <summary>
        ///A test for Changed
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Spreadsheet.dll")]
        public void ChangedTest1()
        {
            Spreadsheet_Accessor target = new Spreadsheet_Accessor(); 
            target.Changed = true;
            Assert.AreEqual(target.Changed, true);
        }

        /// <summary>
        ///A test for GetCellsToRecalculate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Spreadsheet.dll")]
        public void GetCellsToRecalculate()
        {
            Spreadsheet_Accessor target = new Spreadsheet_Accessor();
            target.SetCellContents("A", "5");
            target.SetCellContents("B", "7");
            target.SetCellContents("C", "= A 15 +");
            target.SetCellContents("D", "= A B *");
            target.SetCellContents("E", "100");
            List<string> toCalc = target.GetCellsToRecalculate("A").ToList();
            Assert.IsFalse(toCalc.Contains("E"));
            Assert.IsTrue(toCalc.IndexOf("A") < toCalc.IndexOf("C"));
            Assert.IsTrue(toCalc.IndexOf("B") < toCalc.IndexOf("D"));
            Assert.IsTrue(toCalc.IndexOf("A") < toCalc.IndexOf("D"));
        }

        /// <summary>
        /// A test for GetDependents
        /// </summary>
        [TestMethod()]
        [DeploymentItem("Spreadsheet.dll")]
        public void GetDependentsTest()
        {
            Spreadsheet_Accessor target = new Spreadsheet_Accessor();
            target.SetCellContents("A", "1.2");
            target.SetCellContents("B", "= A 1 +");
            target.SetCellContents("C", "45");
            target.SetCellContents("D", "= C B /");
            List<string> deps = target.GetDependents("A").ToList();
            Assert.AreEqual(deps.Count, 1);
            Assert.IsTrue(deps.Contains("B"));
        }
    }
}
