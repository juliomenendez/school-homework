﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ChatServer
{

    /// <summary>
    /// Represents a simple chat service.  This one merely echoes back lines to
    /// whatever client connects.
    /// </summary>
    class ChatService
    { 
        private TcpListener server;      // Listens for incoming connection requests

        /// <summary>
        /// Creates a ChatService that listens on port 4000.
        /// </summary>
        public ChatService()
        {
            server = new TcpListener(IPAddress.Any, 4000);
            server.Start();
            server.BeginAcceptSocket(ConnectionRequested, null);
        }

        /// <summary>
        /// When a request comes in, creates a new ClientConnection object.
        /// </summary>
        /// <param name="result"></param>
        public void ConnectionRequested(IAsyncResult result)
        {
            Socket s = server.EndAcceptSocket(result);
            server.BeginAcceptSocket(ConnectionRequested, null);
            new ClientConnection(s);
        }

    }


    /// <summary>
    /// Represents a connection with a remote client.
    /// </summary>
    class ClientConnection
    {

        private Socket socket;           // The socket through which the connection is made
        private String incoming;         // Incoming string that is being accumulated
        //private byte[] incomingBuffer;   // For receiving incoming data

        // Encoding used for incoming/outgoing data
        private static System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();


        /// <summary>
        /// Creates a ClientConnection from the (presumed) connected socket.
        /// </summary>
        public ClientConnection(Socket s)
        {
            // Record the socket
            socket = s;

            // Set up the buffer to be used to receive data
            //incomingBuffer = new byte[1024];

            // Send a welcome message
            SendMessage("Welcome!\r\n");

            // Make an asynchronous receive request
            byte[] buffer = new byte[1024];
            socket.BeginReceive(buffer, 0, buffer.Length, 
                                SocketFlags.None, MessageReceived, buffer);
        }


        /// <summary>
        /// Called when some data has been received.
        /// </summary>
        public void MessageReceived(IAsyncResult result)
        {
            // Get the buffer to which the data was written.
            byte[] incomingBuffer = (byte[])(result.AsyncState);
            
            // Figure out how many bytes have come in
            int bytes = socket.EndReceive(result);

            // If no bytes were received, it means the client closed its side of the socket.
            // Report that to the console and close our socket.
            if (bytes == 0)
            {
                Console.WriteLine("Socket closed");
                socket.Close();
            }

            // Otherwise, decode and display the incoming bytes.  Then request more bytes.
            else
            {
                incoming += encoding.GetString(incomingBuffer, 0, bytes);
                Console.WriteLine(incoming);
                int index;
                while ((index = incoming.IndexOf('\n')) >= 0)
                {
                    String line = incoming.Substring(0, index);
                    if (line.EndsWith("\r"))
                    {
                        line = line.Substring(0, index - 1);
                    }
                    SendMessage(line + "\r\n");
                    incoming = incoming.Substring(index + 1);
                }
                byte[] newIncomingBuffer = new byte[1024];
                socket.BeginReceive(newIncomingBuffer, 0, newIncomingBuffer.Length, 
                    SocketFlags.None, MessageReceived, newIncomingBuffer);
            }
        }


        public void SendMessage(String message)
        {
            byte[] outgoingBuffer = encoding.GetBytes(message);
            socket.BeginSend(outgoingBuffer, 0, outgoingBuffer.Length, SocketFlags.None, MessageSent, null);
        }


        public void MessageSent(IAsyncResult result)
        {
            socket.EndReceive(result);
        }

    }
}
