﻿// Written by Joe Zachary for CS 3500, September 2011.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rectangles
{

    /// <summary>
    /// Represents a rectangle.
    /// </summary>
    public class Rectangle
    {

        private int _width;    // Width of rectangle, must be non-negative
        private int _height;   // Height of rectangle, must be non-negative


        /// <summary>
        /// Creates a rectangle
        /// </summary>
        /// <param name="w">height of rectangle</param>
        /// <param name="h">width of rectangle</param>
        public Rectangle(int w, int h)
        {
            Width = w;
            Height = h;
        }


        /// <summary>
        /// For getting/setting a non-negative width
        /// </summary>
        public virtual int Width
        {
            get { return _width; }
            set { if (value < 0) throw new ArgumentException(); else _width = value; }
        }


        /// <summary>
        /// For getting/setting a non-negative height
        /// </summary>
        public virtual int Height
        {
            get { return _height; }
            set { if (value < 0) throw new ArgumentException(); else _height = value; }
        }


        /// <summary>
        /// Returns the area of the rectangle
        /// </summary>
        public int Area()
        {
            return _width * _height;
        }

    }


    /// <summary>
    /// Represents Squares, which are a kind of rectangle
    /// </summary>
    public class Square: Rectangle {

        /// <summary>
        /// Constructs a Square
        /// </summary>
        /// <param name="s">length of side</param>
        public Square (int s) : base(s,s) {
        }


        /// <summary>
        /// Changes the side length (height/width) of Square.
        /// </summary>
        public override int Width {
	        set {base.Width = value; base.Height = value;}
        }

        /// <summary>
        /// Changes the side length (height/width) of Square
        /// </summary>
        public override int Height
        {
            set { base.Height = value; base.Width = value; }
        }

    }


    

    public class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle(2, 5);
            Square s = new Square(5);
            DoubleWidth(r);
            DoubleWidth(s);
            Console.WriteLine(r.Area());
            Console.WriteLine(s.Area());
            Console.ReadLine();
        }

        /// <summary>
        /// Doubles the width of r without changing its height.  According to the
        /// specs of Rectangle, this should work.  But when passed a Square, it
        /// doesn't.  This is because Square violates the Liskov Substitution
        /// Principle in the way it overrides Width and Height.
        /// </summary>
        /// <param name="r"></param>

        static void DoubleWidth(Rectangle r)
        {
            r.Width = 2 * r.Width;
        }
    }

}





