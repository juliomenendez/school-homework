﻿// Written by Joe Zachary for CS 3500, September 2011

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            // Sample array
            int[] numbers = { 5, 2, 7, 1, 9 };

            // Passing isEven (defined below) as a delegate
            Console.WriteLine(numbers.Any(isEven));

            // Passing a lambda expression instead of isEven
            Console.WriteLine(numbers.Any(n => n % 2 == 0));

            // Computing the product of all the numbers in the array
            Console.WriteLine(numbers.Aggregate(1, (x, y) => x * y));

            // Sorting the array using compare as the comparison method
            Array.Sort(numbers, compare);
            Print(numbers);

            // Using a method defined below that takes a delegate as
            // its parameter.
            ApplyAll(numbers, x => x * x);
            Print(numbers);

            // Sample list
            List<int> list = new List<int>() { 5, 2, 7, 1, 9 };
            Print(list);

            // Using an extension method defined below
            list.Replace(x => x * x);
            Print(list);

            Console.ReadLine();
        }


        // Type definition of a delegate called transformer that takes
        // an integer as a parameter and returns an int.
        public delegate int transformer(int n);

        // Method that takes a transformer as one of its parameters
        static void ApplyAllAlt(int[] A, transformer f)
        {
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = f(A[i]);
            }
        }

        // Same as ApplyAll, but uses Func to define the delegate type.
        static void ApplyAll(int[] A, Func<int, int> f)
        {
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = f(A[i]);
            }
        }


        // Reports whether its parameter is even
        static bool isEven(int x)
        {
            return x % 2 == 0;
        }


        // Compares n and m according to which is smaller,
        // modulo 3.
        static int compare(int n, int m)
        {
            if (n % 3 < m % 3)
            {
                return -1;
            }
            else if (n % 3 > m % 3)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        // Prints an ICollection
        static void Print(ICollection<int> a)
        {
            Console.Write("[ ");
            foreach (int n in a)
            {
                Console.Write(n + " ");
            }
            Console.WriteLine("]");
        }


    }



    // A class that contains an extension
    public static class Extensions
    {
        // An extension that takes a delegate as its parameter
        public static void Replace(this List<int> list, Func<int, int> f)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = f(list[i]);
            }
        }
    }

}
