﻿// Written by Joe Zachary for CS 3500, September 2011

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interfaces
{


    // In this version, the GetEnumerator method returns an explicitly 
    // constructed IEnumerator object.

    public class MyArray : IEnumerable<String>
    {
        private String[] data;
        private int size;

        public MyArray()
        {
            data = new String[20];
            size = 0;
        }

        public void Add(String s)
        {
            // Fails when array fills up!
            data[size++] = s;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return new StringListEnumerator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        // This class is used to create enumerators for StringList objects.

        private class StringListEnumerator : IEnumerator<String>
        {
            private MyArray array;
            private int location;

            public StringListEnumerator(MyArray a)
            {
                array = a;
                location = -1;
            }

            public string Current
            {
                get { return array.data[location]; }
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return ++location < array.size;
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            object System.Collections.IEnumerator.Current
            {
                get { return Current; }
            }
        }

    }


    // This version uses the iterator pattern instead of constructing
    // an explicit IEnumerable object

    public class MyArrayAlt : IEnumerable<String>
    {
        private String[] data;
        private int size;

        public MyArrayAlt()
        {
            data = new String[20];
            size = 0;
        }

        public void Add(String s)
        {
            // Need to make this array dynamic!
            data[size++] = s;
        }

        // Note how much simpler it is to use an iterator!
        public IEnumerator<string> GetEnumerator()
        {
            for (int i = 0; i < size; i++)
            {
                yield return data[i];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


    }



    // This one also uses the iterator pattern

    class BinaryTree : IEnumerable<int>
    {
        private int value;
        private BinaryTree left;
        private BinaryTree right;

        public BinaryTree(int a)
        {
            value = a;
            left = right = null;
        }

        public BinaryTree(int a, BinaryTree l, BinaryTree r)
        {
            value = a;
            left = l;
            right = r;
        }

        public IEnumerator<int> GetEnumerator()
        {
            if (left != null)
            {
                foreach (int n in left)
                {
                    yield return n;
                }
            }
            yield return value;
            if (right != null)
            {
                foreach (int n in right)
                {
                    yield return n;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }


    public class Program
    {

        // A few test cases

        public static void Main(string[] args)
        {

            // Create a MyArray
            MyArrayAlt array = new MyArrayAlt() { "a", "b", "c" };

            // Iterate over MyArray
            foreach (String ss in array)
            {
                Console.WriteLine(ss);
            }

            // Iterate over MyArray a different way
            IEnumerator<String> e = array.GetEnumerator();
            while (e.MoveNext())
            {
                Console.WriteLine(e.Current);
            }


            // Create a MyArrayAlt
            MyArrayAlt arrayAlt = new MyArrayAlt() { "a", "b", "c" };

            // Iterate over MyArrayAlt
            foreach (String ss in array)
            {
                Console.WriteLine(ss);
            }

            // Iterate over MyArrayAlt a different way
            e = array.GetEnumerator();
            while (e.MoveNext())
            {
                Console.WriteLine(e.Current);
            }


            // Create a BinaryTree
            BinaryTree t1 = new BinaryTree(5);
            BinaryTree t2 = new BinaryTree(3);
            BinaryTree tree = new BinaryTree(8, t1, t2);

            // Iterate over BinaryTree
            foreach (int n in tree)
            {
                Console.WriteLine(n);
            }

            // Iterate over MyArrayAlt a different way
            var ee = tree.GetEnumerator();
            while (ee.MoveNext())
            {
                Console.WriteLine(ee.Current);
            }


            Console.ReadLine();

        }
    }
}
