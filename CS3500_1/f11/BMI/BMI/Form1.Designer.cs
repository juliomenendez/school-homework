﻿namespace BMI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.heightLabel = new System.Windows.Forms.Label();
            this.weightLabel = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.TextBox();
            this.width = new System.Windows.Forms.TextBox();
            this.calcButton = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.TextBox();
            this.heightError = new System.Windows.Forms.Label();
            this.weightError = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(44, 36);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(55, 13);
            this.heightLabel.TabIndex = 0;
            this.heightLabel.Text = "Height (in)";
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(44, 77);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(58, 13);
            this.weightLabel.TabIndex = 1;
            this.weightLabel.Text = "Weight (lb)";
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(159, 36);
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(100, 20);
            this.height.TabIndex = 2;
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(159, 70);
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(100, 20);
            this.width.TabIndex = 3;
            // 
            // calcButton
            // 
            this.calcButton.Location = new System.Drawing.Point(47, 121);
            this.calcButton.Name = "calcButton";
            this.calcButton.Size = new System.Drawing.Size(75, 23);
            this.calcButton.TabIndex = 4;
            this.calcButton.Text = "Calculate";
            this.calcButton.UseVisualStyleBackColor = true;
            this.calcButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(159, 121);
            this.result.Name = "result";
            this.result.ReadOnly = true;
            this.result.Size = new System.Drawing.Size(100, 20);
            this.result.TabIndex = 5;
            // 
            // heightError
            // 
            this.heightError.AutoSize = true;
            this.heightError.Location = new System.Drawing.Point(322, 42);
            this.heightError.Name = "heightError";
            this.heightError.Size = new System.Drawing.Size(82, 13);
            this.heightError.TabIndex = 6;
            this.heightError.Text = "Enter a number!";
            this.heightError.Visible = false;
            // 
            // weightError
            // 
            this.weightError.AutoSize = true;
            this.weightError.Location = new System.Drawing.Point(325, 76);
            this.weightError.Name = "weightError";
            this.weightError.Size = new System.Drawing.Size(82, 13);
            this.weightError.TabIndex = 7;
            this.weightError.Text = "Enter a number!";
            this.weightError.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "English",
            "Metric"});
            this.comboBox1.Location = new System.Drawing.Point(47, 3);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.Text = "English";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 262);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.weightError);
            this.Controls.Add(this.heightError);
            this.Controls.Add(this.result);
            this.Controls.Add(this.calcButton);
            this.Controls.Add(this.width);
            this.Controls.Add(this.height);
            this.Controls.Add(this.weightLabel);
            this.Controls.Add(this.heightLabel);
            this.Name = "MainWindow";
            this.Text = " BMI Calculator";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.TextBox height;
        private System.Windows.Forms.TextBox width;
        private System.Windows.Forms.Button calcButton;
        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Label heightError;
        private System.Windows.Forms.Label weightError;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

