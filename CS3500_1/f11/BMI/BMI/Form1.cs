﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BMI
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            heightError.Visible = false;
            weightError.Visible = false;
            double h, w;
            if (!Double.TryParse(height.Text, out h))
            {
                heightError.Visible = true;
                return;
            }
            if (!Double.TryParse(width.Text, out w))
            {
                weightError.Visible = true;
                return;
            }
            result.Text = (h*w).ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
