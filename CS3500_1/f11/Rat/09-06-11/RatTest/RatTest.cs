﻿using Rat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RatTest
{
    
    
    /// <summary>
    ///This is a test class for RatTest and is intended
    ///to contain all RatTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RatTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Rat Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest()
        {
            int n = 6; // TODO: Initialize to an appropriate value
            int d = -15; // TODO: Initialize to an appropriate value
            Rat_Accessor target = new Rat_Accessor(n, d);
            Assert.AreEqual(-2, target.Numer);
            Assert.AreEqual(5, target.Denom);
        }

        /// <summary>
        ///A test for Rat Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest1()
        {
            int n = 18; // TODO: Initialize to an appropriate value
            Rat_Accessor target = new Rat_Accessor(n);
            Assert.AreEqual(n, target.Numer);
            Assert.AreEqual(1, target.Denom);
        }

        /// <summary>
        ///A test for Rat Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest2()
        {
            Rat_Accessor target = new Rat_Accessor();
            Assert.AreEqual(0, target.Numer);
            Assert.AreEqual(1, target.Denom);
        }

        /// <summary>
        ///A test for Equals
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void EqualsTest()
        {
            Rat_Accessor target = new Rat_Accessor(3, 4);
            object o = new Rat_Accessor(6, 8);
            bool actual;
            actual = target.Equals(o);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for GetHashCode
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void GetHashCodeTest()
        {
            Rat_Accessor target1 = new Rat_Accessor(5, 6);
            Rat_Accessor target2 = new Rat_Accessor(10, 12);
            Assert.AreEqual(true, target1.GetHashCode() == target2.GetHashCode());
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void ToStringTest()
        {
            Rat_Accessor target = new Rat_Accessor(9,-27);
            Assert.AreEqual("-1/3", target.ToString());
        }

        /// <summary>
        ///A test for op_Addition
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_AdditionTest()
        {
            Rat_Accessor r1 = new Rat_Accessor(10000000, 200000000);
            Rat_Accessor r2 = new Rat_Accessor(10000000, 300000000);
            Rat_Accessor expected = new Rat_Accessor(5, 6);
            Assert.AreEqual((r1 + r2).Numer, 5);
            Assert.AreEqual((r1 + r2).Denom, 6);
        }

        /// <summary>
        ///A test for op_Equality
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_EqualityTest()
        {
            Rat_Accessor r1 = null; // TODO: Initialize to an appropriate value
            Rat_Accessor r2 = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = (r1 == r2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for op_Inequality
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_InequalityTest()
        {
            Rat_Accessor r1 = null; // TODO: Initialize to an appropriate value
            Rat_Accessor r2 = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = (r1 != r2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Denom
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void DenomTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Denom;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Numer
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void NumerTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Numer;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
