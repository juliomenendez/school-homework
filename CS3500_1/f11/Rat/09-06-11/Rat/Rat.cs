﻿// Written by Joe Zachary for CS 3500, August 2011.
// Version 2.0, as of end of lecture on 9/6/2011

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rat
{

    /// <summary>
    /// Provides rational numbers that can be expressed as ratios
    /// of 32-bit integers.  Rats are immutable.
    /// </summary>
    class Rat
    {
        // Abstraction function: 
        // A Rat represents the rational _num/_den

        // Representation invariant:
        //  _den > 0
        //  gcd(_num, _den) = 1
        private int _num;
        private int _den;


        /// <summary>
        /// Creates 0
        /// </summary>
        public Rat()
            : this(0, 1)
        {
        }

        /// <summary>
        /// Creates n
        /// </summary>
        /// <param name="n">Numerator, where denominator is 1</param>
        public Rat(int n)
            : this(n, 1)
        {
        }

        /// <summary>
        /// Creates n/d.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if d == 0</exception>
        /// <param name="n">Numerator</param>
        /// <param name="d">Denominator, which must not be zero</param>
        public Rat(int n, int d)
        {
            if (d == 0)
            {
                throw new ArgumentException("Zero denominator not allowed");
            }
            int g = n.Gcd(d);
            if (d > 0)
            {
                _num = n / g;
                _den = d / g;
            }
            else
            {
                _num = -n / g;
                _den = -d / g;
            }
        }


        /// <summary>
        /// Numerator when Rat is expressed in lowest terms
        /// </summary>
        public int Numer
        {
            // We allow clients to retrieve the numerator using notation
            // like r.Numer, but provide no way to set it.
            get { return _num; }
        }


        /// <summary>
        /// Denominator when Rat is expressed in lowest terms
        /// </summary>

        public int Denom
        {
            // We allow clients to retrieve the denominator using notation
            // like r.Denom, but provide no way to set it.
            get { return _den; }
        }


        /// <summary>
        /// Returns the sum of r1 and r2.
        /// </summary>
        /// <exception cref="System.OverflowException">When arithmetic overflow</exception>
        /// <param name="r1">Addend</param>
        /// <param name="r2">Addend</param>
        /// <returns></returns>
        public static Rat operator +(Rat r1, Rat r2)
        {
            checked
            {
                return new Rat(r1.Numer * r2.Denom + r1.Denom * r2.Numer, 
                               r1.Denom * r2.Denom);
            }
        }


        /// <summary>
        /// Returns a unique string representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Denom == 1)
            {
                return Numer.ToString();
            }
            else
            {
                return Numer + "/" + Denom;
            }
        }


        /// <summary>
        /// Reports whether this and o are the same rational number.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public override bool Equals(object o)
        {
            // Cast o to be a Rat.  If the cast fails, we get null back.
            Rat r = o as Rat;

            // Make sure r is non-null and its numerator and denominator
            // the same as those of this.
            return
                !ReferenceEquals(r, null) &&
                this.Numer == r.Numer &&
                this.Denom == r.Denom;
        }


        /// <summary>
        /// Tests for equality
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator ==(Rat r1, Rat r2)
        {
            if (ReferenceEquals(r1, null)) {
                return ReferenceEquals(r2, null);
            }
            else
            {
                return r1.Equals(r2);
            }
        }


        /// <summary>
        /// Casts an int to a Rat
        /// </summary>
        /// <param name="n">Int to be cast</param>
        /// <returns></returns>
        public static implicit operator Rat(int n)
        {
            return new Rat(n);
        }



        /// <summary>
        /// Tests for inequality
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static bool operator !=(Rat r1, Rat r2)
        {
            return !(r1 == r2);
        }


        /// <summary>
        /// Returns a hash code for this Rat.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Numer ^ Denom;
        }

    }



    static class Extensions
    {

        /// <summary>
        /// Returns the GCD of a and b.  
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Gcd(this int a, int b)
        {
            // This is implemented as an extension to the
            // Int32 (int) class.  This means that you can
            // invoke it with, e.g., 12.Gcd(8).
            a = Math.Abs(a);
            b = Math.Abs(b);
            while (b > 0)
            {
                int temp = a % b;
                a = b;
                b = temp;
            }
            return a;
        }
    }
}
