﻿using Rat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RatTester
{
    
    
    /// <summary>
    ///This is a test class for RatTest and is intended
    ///to contain all RatTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RatTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Rat Constructor.   Via the ToString
        ///method, we assert that the proper Rat has
        ///been constructed.
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest1a()
        {
            Rat_Accessor target = new Rat_Accessor(-5, -15);
            Assert.AreEqual("1/3", target.ToString());
        }

        /// <summary>
        ///A test for Rat Constructor.  Here we pass zero as a
        ///denominator, which should trigger an exception.  Note
        ///the use of the ExpectedException annotation.  If an
        ///exception is *not* thrown, the test case will fail.
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        [ExpectedException(typeof(ArgumentException))]
        public void RatConstructorTest1b()
        {
            Rat_Accessor target = new Rat_Accessor(5, 0);
        }

        /// <summary>
        ///A test for Rat Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest2()
        {
            int n = 0; // TODO: Initialize to an appropriate value
            Rat_Accessor target = new Rat_Accessor(n);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Rat Constructor
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void RatConstructorTest3()
        {
            Rat_Accessor target = new Rat_Accessor();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Equals
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void EqualsTest()
        {
            Assert.IsTrue(new Rat_Accessor(1, 2) == new Rat_Accessor(5, 10));
        }

        /// <summary>
        ///A test for GetHashCode
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void GetHashCodeTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.GetHashCode();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void ToStringTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.ToString();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for op_Addition
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_AdditionTest()
        {
            Rat_Accessor r1 = new Rat_Accessor(1, 2);
            Rat_Accessor r2 = new Rat_Accessor(1, 3);
            Assert.AreEqual("5/6", (r1+r2).ToString());
        }

        /// <summary>
        ///A test for op_Equality
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_EqualityTest()
        {
            Rat_Accessor r1 = null; // TODO: Initialize to an appropriate value
            Rat_Accessor r2 = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = (r1 == r2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for op_Implicit
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_ImplicitTest()
        {
            int n = 0; // TODO: Initialize to an appropriate value
            Rat_Accessor expected = null; // TODO: Initialize to an appropriate value
            Rat_Accessor actual;
            actual = n;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for op_Inequality
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void op_InequalityTest()
        {
            Rat_Accessor r1 = null; // TODO: Initialize to an appropriate value
            Rat_Accessor r2 = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = (r1 != r2);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Denom
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void DenomTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Denom;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Numer
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Rat.exe")]
        public void NumerTest()
        {
            Rat_Accessor target = new Rat_Accessor(); // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Numer;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
