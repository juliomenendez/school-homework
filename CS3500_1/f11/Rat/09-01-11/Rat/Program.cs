﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rat
{
    class Program
    {
        // For playing around with the Rat Class
        static void Main(string[] args)
        {
            Rat r1 = new Rat();      // 0
            Rat r2 = new Rat(7);     // 7
            Rat r3 = new Rat(3, 6);  // 1/2

            // Accessing properties
            Console.WriteLine(r3.Numer + " " + r3.Denom);

            // Accessing an extension method
            Console.WriteLine(12.Gcd(8));

            Console.ReadLine();
        }
    }
}
