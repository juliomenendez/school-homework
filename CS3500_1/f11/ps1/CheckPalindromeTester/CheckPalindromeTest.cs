﻿using FilterLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CheckPalindromeTester
{
    
    
    /// <summary>
    ///This is a test class for FiltersTest and is intended
    ///to contain all FiltersTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CheckPalindromeTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for CheckPalindrome
        ///</summary>
        [TestMethod()]
        public void PalindromesTest1()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("a");
            IEnumerator<String> result = Filters.CheckPalindrome(inputs).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("a", result.Current);
            Assert.IsFalse(result.MoveNext());
        }


        /// <summary>
        ///A test for CheckPalindrome
        ///</summary>
        [TestMethod()]
        public void PalindromesTest2()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("ab");
            IEnumerator<String> result = Filters.CheckPalindrome(inputs).GetEnumerator();
            Assert.IsFalse(result.MoveNext());
        }


        /// <summary>
        ///A test for CheckPalindrome
        ///</summary>
        [TestMethod()]
        public void PalindromesTest3()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("abcdcba");
            IEnumerator<String> result = Filters.CheckPalindrome(inputs).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("abcdcba", result.Current);
            Assert.IsFalse(result.MoveNext());
        }


        /// <summary>
        ///A test for CheckPalindrome
        ///</summary>
        [TestMethod()]
        public void PalindromesTest4()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("xyx");
            inputs.AddLast("xyz");
            IEnumerator<String> result = Filters.CheckPalindrome(inputs).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("xyx", result.Current);
            Assert.IsFalse(result.MoveNext());
        }


        /// <summary>
        ///A test for CheckPalindrome
        ///</summary>
        [TestMethod()]
        public void PalindromesTest5()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("deed");
            inputs.AddLast("ddde");
            inputs.AddLast("deedeed");
            inputs.AddLast("dd");
            inputs.AddLast("ddddddddddddddddddddddddddde");
            inputs.AddLast("ee");
            IEnumerator<String> result = Filters.CheckPalindrome(inputs).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("deed", result.Current);
            result.MoveNext();
            Assert.AreEqual("deedeed", result.Current);
            result.MoveNext();
            Assert.AreEqual("dd", result.Current);
            result.MoveNext();
            Assert.AreEqual("ee", result.Current);
            result.MoveNext();
            Assert.IsFalse(result.MoveNext());
        }

    }
}
