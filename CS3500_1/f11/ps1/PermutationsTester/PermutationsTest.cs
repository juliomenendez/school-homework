﻿using FilterLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace PermutationsTester
{
    
    
    /// <summary>
    ///This is a test class for FiltersTest and is intended
    ///to contain all FiltersTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PermutationsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Permutations
        ///</summary>
        [TestMethod()]
        public void PermutationsTest1()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("a");
            IEnumerator<String> result = Filters.Permutations(inputs).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("a", result.Current);
        }

        /// <summary>
        ///A test for Permutations
        ///</summary>
        [TestMethod()]
        public void PermutationsTest2()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("ab");
            SortedSet<String> results = new SortedSet<String>();
            foreach (String s in Filters.Permutations(inputs))
            {
                results.Add(s);
            }
            SortedSet<String> expected = new SortedSet<String>();
            expected.Add("ba");
            expected.Add("ab");
            Assert.IsTrue(expected.SetEquals(results));
        }


        /// <summary>
        ///A test for Permutations
        ///</summary>
        [TestMethod()]
        public void PermutationsTest3()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("abc");
            SortedSet<String> results = new SortedSet<String>();
            foreach (String s in Filters.Permutations(inputs))
            {
                results.Add(s);
            }
            SortedSet<String> expected = new SortedSet<String>();
            expected.Add("abc");
            expected.Add("acb");
            expected.Add("bac");
            expected.Add("bca");
            expected.Add("cab");
            expected.Add("cba");
            Assert.IsTrue(expected.SetEquals(results));
        }


        /// <summary>
        ///A test for Permutations
        ///</summary>
        [TestMethod()]
        public void PermutationsTest4()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("abc");
            inputs.AddLast("xy");
            SortedSet<String> results = new SortedSet<String>();
            foreach (String s in Filters.Permutations(inputs))
            {
                results.Add(s);
            }
            SortedSet<String> expected = new SortedSet<String>();
            expected.Add("abc");
            expected.Add("acb");
            expected.Add("bac");
            expected.Add("bca");
            expected.Add("cab");
            expected.Add("cba");
            expected.Add("xy");
            expected.Add("yx");
            Assert.IsTrue(expected.SetEquals(results));
        }


        /// <summary>
        ///A test for Permutations
        ///</summary>
        [TestMethod()]
        public void PermutationsTest5()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("abc");
            inputs.AddLast("xy");
            inputs.AddLast("def");
            SortedSet<String> results = new SortedSet<String>();
            foreach (String s in Filters.Permutations(inputs))
            {
                results.Add(s);
            }
            SortedSet<String> expected = new SortedSet<String>();
            expected.Add("abc");
            expected.Add("acb");
            expected.Add("bac");
            expected.Add("bca");
            expected.Add("cab");
            expected.Add("cba");
            expected.Add("xy");
            expected.Add("yx");
            expected.Add("def");
            expected.Add("dfe");
            expected.Add("efd");
            expected.Add("edf");
            expected.Add("fed");
            expected.Add("fde");
            Assert.IsTrue(expected.SetEquals(results));
        }
    }
}
