﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilterLibrary
{
    public class Filters
    {

        public static IEnumerable<String> ConvertCase(IEnumerable<String> inputs, bool upper)
        {
            foreach (String s in inputs)
            {
                if (upper)
                {
                    yield return s.ToUpper();
                }
                else
                {
                    yield return s.ToLower();
                }
            }
        }


        public static IEnumerable<String> CheckPalindrome(IEnumerable<String> inputs)
        {
            foreach (string s in inputs)
            {
                if (IsPalindrome(s)) yield return s;
            }
        }

        private static bool IsPalindrome(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] != s[s.Length - i - 1]) return false;
            }
            return true;
        }


        public static IEnumerable<String> Permutations(IEnumerable<String> inputs)
        {
            foreach (String s in inputs)
            {
                foreach (String r in Permutations(s))
                {
                    yield return r;
                }
            }
        }

        public static IEnumerable<String> Permutations(String s)
        {
            if (s.Length < 2)
            {
                yield return s;
            }
            else
            {
                String first = s.Remove(s.Length - 1);
                String last = s.Substring(s.Length - 1);
                foreach (String r in Permutations(first))
                {
                    for (int i = 0; i <= r.Length; i++)
                    {
                        yield return r.Insert(i, last);
                    }
                }
            }
        }


        public static void WriteToConsole(IEnumerable<String> lines)
        {
            foreach (String s in lines)
            {
                Console.WriteLine(s);
            }
        }


        public static IEnumerable<String> ReadFromConsole()
        {
            String line;
            while ((line = Console.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }


}
