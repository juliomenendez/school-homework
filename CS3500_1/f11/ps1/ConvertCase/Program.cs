﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FilterLibrary;

namespace ConvertCase
{
    class Program
    {
        static void Main(string[] args)
        {
            bool upper = args.Length == 1 && args[0] == "upper";
            Filters.WriteToConsole(FilterLibrary.Filters.ConvertCase(Filters.ReadFromConsole(), upper));
        }
    }
}
