﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FilterLibrary;


namespace Permutations
{
    class Permutations
    {
        static void Main(string[] args)
        {
            Filters.WriteToConsole(FilterLibrary.Filters.Permutations(Filters.ReadFromConsole()));
        }
    }
}
