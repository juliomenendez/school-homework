﻿using FilterLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ConvertCaseTester
{
    
    
    /// <summary>
    ///This is a test class for FiltersTest and is intended
    ///to contain all FiltersTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConvertCaseTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for ConvertCase
        ///</summary>
        [TestMethod()]
        public void ConvertCaseTest1()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("Experiment");
            IEnumerator<String> result = Filters.ConvertCase(inputs, true).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("EXPERIMENT", result.Current);
        }


        /// <summary>
        ///A test for ConvertCase
        ///</summary>
        [TestMethod()]
        public void ConvertCaseTest2()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("Experiment");
            IEnumerator<String> result = Filters.ConvertCase(inputs, false).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("experiment", result.Current);
        }

        /// <summary>
        ///A test for ConvertCase
        ///</summary>
        [TestMethod()]
        public void ConvertCaseTest3()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("Hello there");
            inputs.AddLast("THIS IS a TEST");
            IEnumerator<String> result = Filters.ConvertCase(inputs, true).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("HELLO THERE", result.Current);
            result.MoveNext();
            Assert.AreEqual("THIS IS A TEST", result.Current);
        }


        /// <summary>
        ///A test for ConvertCase
        ///</summary>
        [TestMethod()]
        public void ConvertCaseTest4()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("Hello there");
            inputs.AddLast("THIS IS a TEST");
            IEnumerator<String> result = Filters.ConvertCase(inputs, false).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("hello there", result.Current);
            result.MoveNext();
            Assert.AreEqual("this is a test", result.Current);
        }


        /// <summary>
        ///A test for ConvertCase
        ///</summary>
        [TestMethod()]
        public void ConvertCaseTest5()
        {
            LinkedList<String> inputs = new LinkedList<String>();
            inputs.AddLast("Hello there");
            inputs.AddLast("THIS IS a TEST");
            inputs.AddLast("Now Is The Time");
            IEnumerator<String> result = Filters.ConvertCase(inputs, false).GetEnumerator();
            result.MoveNext();
            Assert.AreEqual("hello there", result.Current);
            result.MoveNext();
            Assert.AreEqual("this is a test", result.Current);
            result.MoveNext();
            Assert.AreEqual("now is the time", result.Current);
            Assert.IsFalse(result.MoveNext());
        }


    }
}
