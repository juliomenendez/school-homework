﻿using ChatServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Sockets;

namespace ServerTester
{
    
    
    /// <summary>
    ///This is a test class for ChatServiceTest and is intended
    ///to contain all ChatServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ChatServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test (that doesn't quite work) for ChatService Constructor
        ///</summary>
        [TestMethod()]
        public void ChatServiceConstructorTest1()
        {
            // Start a new ChatService on port 4001
            ChatService server = new ChatService(4001);

            // Attempt to connect to the server.  Upon success, obtain the socket
            // and work with that.
            TcpClient client1 = new TcpClient("localhost", 4001);
            Socket socket1 = client1.Client;

            // See if we receive any bytes at all from the server.  This initiates an asynchronous
            // read operation.  When some bytes are ready to be read, the CheckResponse method is
            // called.
            byte[] input = new byte[1024];
            IAsyncResult r = socket1.BeginReceive(input, 0, 1024, SocketFlags.None, 
                                                  CheckResponse1, socket1);

            // Close down the client and the server.
            client1.Close();
            server.Shutdown();
        }

        // CheckResponse1 will be called when some bytes have arrived following the call to BeginReceive
        // above.  We obtain the socket on which the read was initiated (via the payload), find out
        // how many bytes were received, and assert that the amount was non-zero.

        // There is a big problem with this approach.  The main testing thread will continue on after
        // the call to BeginReceive and will close down both the client and the server.  As a result, when 
        // CheckResponse1 is eventually called, it won't work properly.  Furthermore, assertions made on 
        // anything other than the main testing thread are immaterial.
        public void CheckResponse1(IAsyncResult r)
        {
            Socket s = (Socket) r.AsyncState;
            int bytesRead = s.EndReceive(r);
            Assert.AreNotEqual(0, bytesRead);
        }



        /// <summary>
        ///A modified test (that still doesn't quite work) for ChatService Constructor
        ///</summary>
        [TestMethod()]
        public void ChatServiceConstructorTest2()
        {
            // Start a new ChatService on port 4002
            ChatService server = new ChatService(4002);

            // Attempt to connect to the server.  Upon success, obtain the socket
            // and work with that.
            TcpClient client1 = new TcpClient("localhost", 4002);
            Socket socket1 = client1.Client;

            // See if we receive any bytes at all from the server.  This initiates an asynchronous
            // read operation.  When some bytes are ready to be read, the CheckResponse method is
            // called.
            byte[] input = new byte[1024];
            IAsyncResult r = socket1.BeginReceive(input, 0, 1024, SocketFlags.None,
                                                  CheckResponse2, socket1);

            // Wait until the receiving thread signals that it has received bytes before making
            // the assertions and closing the client and server.  Unfortunately, the main thread might
            // get to run before CheckResponse2 is called, in which case this won't work right.
            // Sometimes it works, sometimes it doesn't.
            r.AsyncWaitHandle.WaitOne();
            Assert.AreNotEqual(0, bytesRead);

            // Close down the client and the server.
            client1.Close();
            server.Shutdown();

            // (1) 
            //r.AsyncWaitHandle.WaitOne();
            //bytesRead = socket1.EndReceive(r);
            //Assert.AreNotEqual(0, bytesRead);
            //client1.Close();
        }


        // Communicates bytes read across threads.
        private int bytesRead = 0;

        // When this is called, it saves the bytes read into a member variable so that the main testing thread
        // can make an assertion about it.
        public void CheckResponse2(IAsyncResult r)
        {
            Socket s = (Socket) r.AsyncState;
            bytesRead = s.EndReceive(r);
        }



        /// <summary>
        ///A modified test (that actually works) for ChatService Constructor
        ///</summary>
        [TestMethod()]
        public void ChatServiceConstructorTest3()
        {
            // Start a new ChatService on port 4003
            ChatService server = new ChatService(4003);

            // Attempt to connect to the server.  Upon success, obtain the socket
            // and work with that.
            TcpClient client1 = new TcpClient("localhost", 4003);
            Socket socket1 = client1.Client;

            // See if we receive any bytes at all from the server.  This initiates an asynchronous
            // read operation.  When some bytes are ready to be read, the CheckResponse method is
            // called.
            byte[] input = new byte[1024];
            IAsyncResult r = socket1.BeginReceive(input, 0, 1024, SocketFlags.None,
                                                  CheckResponse3, socket1);

            // This will block until some bytes are received.
            int bytesRead = socket1.EndReceive(r);

            // Now it's safe to make the assertion and close things down.
            Assert.AreNotEqual(0, bytesRead);
            client1.Close();
            server.Shutdown();
        }


        // The callback now does nothing.
        public void CheckResponse3(IAsyncResult r)
        {
        }
    }
}
