﻿// Written by Joe Zachary for CS 3500, October 2011
// Version 1.8

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace ChatServer
{

    /// <summary>
    /// Represents a simple chat service.  This one follows a simple chat protocol that
    /// is documented in the PS 5 handout.
    /// </summary>
    public class ChatService
    {
        private TcpListener server;      // Listens for incoming connection requests

        /// <summary>
        /// Creates a ChatService that listens on the specified port.
        /// </summary>
        public ChatService(int port)
        {
            server = new TcpListener(IPAddress.Any, port);
            connections = new HashSet<ClientConnection>();
            sessions = new Dictionary<string, HashSet<ClientConnection>>();
            server.Start();
            server.BeginAcceptSocket(ConnectionRequested, null);
        }


        /// <summary>
        /// Shuts down the server and closes all sockets.
        /// </summary>
        public void Shutdown()
        {
            server.Stop();
            lock (this)
            {
                foreach (ClientConnection c in connections)
                {
                    c.CloseClient();
                }
            }
        }

        /// <summary>
        /// When a request comes in, creates a new ClientConnection object.
        /// </summary>
        /// <param name="result"></param>
        public void ConnectionRequested(IAsyncResult result)
        {
            try
            {
                Socket s = server.EndAcceptSocket(result);
                server.BeginAcceptSocket(ConnectionRequested, null);
                new ClientConnection(s, this);
            }
            catch (Exception)
            {
            }
        }

        // A mapping from chat session names to the set of clients participating in that session.
        private Dictionary<String, HashSet<ClientConnection>> sessions;

        // The set of all ClientConnections
        private HashSet<ClientConnection> connections;


        /// <summary>
        /// Records a new chatter and sends list of existing sessions
        /// </summary>
        internal void AddChatter(ClientConnection client)
        {
            lock (this)
            {
                connections.Add(client);
                client.SendMessage("<WELCOME>");
                foreach (String session in sessions.Keys)
                {
                    client.SendMessage("<EXISTING_SESSION " + session + ">");
                }
            }
        }


        /// <summary>
        /// Acknowledges that the client has provided a name.
        /// </summary>
        internal void NameChatter(ClientConnection client)
        {
            lock (this)
            {
                client.SendMessage("<NAME " + client.GetName() + ">");
            }
        }


        /// <summary>
        /// Removes a chatter, possibly removing from a session
        /// </summary>
        internal void RemoveChatter(ClientConnection client)
        {
            lock (this)
            {
                if (client.GetSession() != null)
                {
                    StopChatting(client);
                }
                connections.Remove(client);
                client.SendMessage("<QUIT>");
            }
        }


        /// <summary>
        /// Adds a client to a session and sends notifications
        /// </summary>
        internal void StartChatting(ClientConnection client)
        {
            lock (this)
            {
                client.SendMessage("<SESSION " + client.GetSession() + ">");
                HashSet<ClientConnection> members;
                if (!sessions.TryGetValue(client.GetSession(), out members))
                {
                    members = new HashSet<ClientConnection>();
                    sessions[client.GetSession()] = members;
                    foreach (ClientConnection c in connections)
                    {
                        c.SendMessage("<STARTING_SESSION " + client.GetSession() + ">");
                    }
                }
                foreach (ClientConnection c in members)
                {
                    client.SendMessage("<SESSION_MEMBER " + c.GetName() + ">");
                }
                members.Add(client);
                foreach (ClientConnection c in members)
                {
                    c.SendMessage("<JOINING_SESSION " + client.GetName() + ">");
                }
            }
        }


        /// <summary>
        /// Removes a client from a session and sends notifications
        /// </summary>
        internal void StopChatting(ClientConnection client)
        {
            lock (this)
            {
                client.SendMessage("<LEAVE>");
                HashSet<ClientConnection> members;
                if (client.GetSession() != null && sessions.TryGetValue(client.GetSession(), out members))
                {
                    foreach (ClientConnection c in members)
                    {
                        c.SendMessage("<LEAVING_SESSION " + client.GetName() + ">");
                    }
                    members.Remove(client);
                    if (members.Count == 0)
                    {
                        sessions.Remove(client.GetSession());
                        foreach (ClientConnection c in connections)
                        {
                            c.SendMessage("<ENDING_SESSION " + client.GetSession() + ">");
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Broadcasts a line of text to the participants in a session.
        /// </summary>
        internal void Broadcast(ClientConnection client, string line)
        {
            lock (this)
            {
                HashSet<ClientConnection> members;
                if (sessions.TryGetValue(client.GetSession(), out members))
                {
                    foreach (ClientConnection c in members)
                    {
                        c.SendMessage("<MESSAGE " + client.GetName() + " " + line + ">");
                    }
                }
            }
        }

    }


    /// <summary>
    /// Represents a connection with a remote client.
    /// </summary>
    class ClientConnection
    {
        // Current state of the client connection, where
        //   starting = connected but no name provided
        //   named = name provided but no session chosen
        //   chatting = name and session provided
        //   finished = finished with session
        private enum ClientState { starting, named, chatting, finished };

        // The top-level ChatService object
        private ChatService server;

        private Socket socket;           // The socket through which the connection is made
        private String incoming;         // Incoming string that is being accumulated
        private byte[] incomingBuffer;   // For receiving incoming data
        private ClientState state;       // State of client
        private String name;             // Name of client
        private String session;          // Name of session

        // Encoding used for incoming/outgoing data
        private static System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();


        /// <summary>
        /// Creates a ClientConnection from the (presumed) connected socket.
        /// </summary>
        public ClientConnection(Socket s, ChatService server)
        {
            // Record the socket
            socket = s;

            // Record the server
            this.server = server;

            // Set up the buffer to be used to receive data
            incomingBuffer = new byte[1024];

            // Send welcome message and list of current sessions
            server.AddChatter(this);

            // We're in the starting state
            state = ClientState.starting;

            // Wait for incoming bytes.
            try
            {
                socket.BeginReceive(incomingBuffer, 0, incomingBuffer.Length, SocketFlags.None, MessageReceived, null);
            }
            catch (Exception)
            {
                server.RemoveChatter(this);
            }
        }


        /// <summary>
        /// Called when some data has been received.
        /// </summary>
        private void MessageReceived(IAsyncResult result)
        {
            // Figure out how many bytes have come in
            int bytes = 0;

            try
            {
                bytes = socket.EndReceive(result);
            }
            catch (Exception)
            {
            }

            // If no bytes were received, it means the client closed its side of the socket.
            // If an exception was thrown, it means that we've shut down the socket elsewhere.
            // If either case, stop chatting on this socket and close it.
            if (bytes == 0)
            {
                Console.WriteLine("Socket closed");
                server.StopChatting(this);
                session = null;
                server.RemoveChatter(this);
                socket.Close();
            }

            // Otherwise, decode and process the incoming bytes.  Then request more bytes, unless we are
            // quitting.
            else
            {
                incoming += encoding.GetString(incomingBuffer, 0, bytes);
                Console.WriteLine(incoming);
                int index;
                while ((index = incoming.IndexOf('\n')) >= 0)
                {
                    String line = incoming.Substring(0, index);
                    if (line.EndsWith("\r"))
                    {
                        line = line.Substring(0, index - 1);
                    }
                    ProcessLine(line);
                    incoming = incoming.Substring(index + 1);
                }

                // Do this in a try block in case the socket has been shut down and closed.
                try
                {
                    socket.BeginReceive(incomingBuffer, 0, incomingBuffer.Length, SocketFlags.None, MessageReceived, null);
                }
                catch (Exception)
                {
                    server.StopChatting(this);
                    session = null;
                    server.RemoveChatter(this);
                }
            }
        }


        /// <summary>
        /// Extracts and returns the command name and the argument (if any).  If this
        /// isn't a command, of it isn't formatted correctly, returns false.  Otherwise,
        /// returns true.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private bool GetCommand(String line, out String command, out String argument)
        {
            command = argument = null;
            line = line.Trim();
            if (line.Length < 3 || line[0] != '[' || line[line.Length - 1] != ']')
            {
                return false;
            }
            else
            {
                // Remove square brackets
                line = line.Substring(1, line.Length - 2);

                // There should be at least one token
                string[] tokens = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length == 0)
                {
                    return false;
                }

                // Get the command
                String rawCommand = tokens[0];
                command = rawCommand.ToLower();

                // A quit or leave command should have no argument
                if (command == "quit" || command == "leave")
                {
                    return tokens.Length == 1;
                }

                // A name or session command should have one argument
                if (command == "name" || command == "session")
                {
                    if (tokens.Length != 2)
                    {
                        return false;
                    }
                    else
                    {
                        argument = tokens[1];
                        return true;
                    }
                }

                // A message command has everything else as its argument
                if (command == "message")
                {
                    int index = line.IndexOf(rawCommand) + rawCommand.Length;
                    argument = line.Substring(index);
                    if (argument.Length > 0)
                    {
                        argument = argument.Substring(1);
                    }
                    return true;
                }

                // None of the above.
                return false;

            }
        }


        /// <summary>
        /// Processes a line received from the client.  Returns true if
        /// we are quitting and false otherwise.
        /// </summary>
        /// <param name="line"></param>
        private void ProcessLine(String line)
        {
            // Get the command, if any
            String command, argument;
            if (!GetCommand(line, out command, out argument))
            {
                return;
            }

            // What we do depends on what state we're in
            switch (state)
            {
                case ClientState.starting:
                    // Quit
                    if (command == "quit")
                    {
                        server.RemoveChatter(this);
                        state = ClientState.finished;
                        CloseClient();
                    }
                    // Give name
                    else if (command == "name")
                    {
                        name = argument;
                        server.NameChatter(this);
                        state = ClientState.named;
                    }
                    break;

                case ClientState.named:
                    // Quit
                    if (command == "quit")
                    {
                        server.RemoveChatter(this);
                        session = null;
                        state = ClientState.finished;
                        CloseClient();
                    }
                    // Choose session
                    else if (command == "session")
                    {
                        session = argument;
                        server.StartChatting(this);
                        state = ClientState.chatting;
                    }
                    break;

                case ClientState.chatting:
                    // Leave session
                    if (command == "leave")
                    {
                        server.StopChatting(this);
                        session = null;
                        state = ClientState.named;
                    }
                    // Quit
                    else if (command == "quit")
                    {
                        server.StopChatting(this);
                        session = null;
                        server.RemoveChatter(this);
                        state = ClientState.finished;
                        CloseClient();
                    }
                    // Message sent
                    else if (command == "message")
                    {
                        server.Broadcast(this, argument);
                    }
                    break;

                case ClientState.finished:
                    break;
            }
        }


        // Close down the socket.
        public void CloseClient()
        {
            try
            {
                socket.Shutdown(SocketShutdown.Both);
                IAsyncResult r = socket.BeginDisconnect(false, (x) => { }, null);
                socket.EndDisconnect(r);
            }
            catch (Exception)
            {
            }
        }


        // Send a message followed by a newline.
        public void SendMessage(String message)
        {
            byte[] outgoingBuffer = encoding.GetBytes(message + "\r\n");
            try
            {
                socket.BeginSend(outgoingBuffer, 0, outgoingBuffer.Length, SocketFlags.None, MessageSent, null);
            }
            catch (Exception)
            {
            }
        }


        // Acknowledge receipt
        private void MessageSent(IAsyncResult result)
        {
            try
            {
                socket.EndSend(result);
            }
            catch (Exception)
            {
            }
        }


        // Get the session name in which the client is participating, if any
        public string GetSession()
        {
            return session;
        }

        // Get the name of the client, if any
        public string GetName()
        {
            return name;
        }
    }
}
