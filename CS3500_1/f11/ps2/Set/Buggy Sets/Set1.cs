﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PS2
{
	public class Set
	{
		/// <summary>
		/// Array of ints containing the values in the set.
		/// </summary>
		private int[] _values;

		/// <summary>
		/// Counts the number of times a call has been made either to Insert
		/// or Remove.
		/// </summary>
		private int _modifyCallCount;

		/// <summary>
		/// Default constructor. Constructs and empty set.
		/// </summary>
		public Set()
		{
			//_values = new int[0];
		}

		/// <summary>
		/// Constructs a set containing n as its only element.
		/// </summary>
		/// <param name="n"></param>
		public Set(int n)
		{
			_values = new int[] { n };
		}

		/// <summary>
		/// Gets the the number of unique values in the set.
		/// </summary>
		public int Size
		{
			get { return _values.Length; }
		}

		/// <summary>
		/// Inserts a value into the set. If n is already contained in the set,
		/// no change is made.
		/// </summary>
		/// <param name="n"></param>
		public void Insert(int n)
		{
			_modifyCallCount++;
			if (!this.Contains(n))
			{
				//
				// Copy the array into one of size + 1, add the new item, and sort.
				//
				int[] result = new int[_values.Length + 1];
				result[_values.Length] = n;
				Array.Copy(_values, result, _values.Length);
				Array.Sort(result);
				_values = result;
			}
		}

		/// <summary>
		/// Removes a value from the set. If n is not already in the set, no
		/// change is made.
		/// </summary>
		/// <param name="n"></param>
		public void Remove(int n)
		{
			_modifyCallCount++;
			if (this.Contains(n))
			{
				int[] result = new int[_values.Length-1];
				int index = 0;
				foreach (int value in _values)
				{
					if (value != n)
					{
						result[index++] = value;
					}
				}
				_values = result;
			}
		}

		/// <summary>
		/// Returns a string representation of the set in standard set notation.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return "{" + string.Join(", ", _values) + "}";
		}

		/// <summary>
		/// True if the set contains the value n. False otherwise.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public bool Contains(int n)
		{
			return _values.Contains(n);
		}

		/// <summary>
		/// Returns an IEnumerable object of all the values in the set.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<int> Elements()
		{
			int callCount = _modifyCallCount;
			foreach (int value in _values)
			{
				yield return value;
				if (callCount != _modifyCallCount)
				{
					throw new InvalidOperationException("Set was modified");
				}
			}
		}
	}
}
