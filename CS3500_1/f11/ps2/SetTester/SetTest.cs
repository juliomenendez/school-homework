﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS2;

namespace SetTester
{
    
    
    /// <summary>
    ///This is a test class for SetTest and is intended
    ///to contain all SetTest Unit Tests
    ///</summary>
	[TestClass()]
	public class SetTest
	{


		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		/// <summary>
		/// Returns a Set object populated with all of the values between
		/// minValue and maxValue, inclusive.
		/// </summary>
		/// <param name="minValue"></param>
		/// <param name="maxValue"></param>
		/// <returns></returns>
		static Set GetFilledSet(int minValue, int maxValue)
		{
			var result = new Set();
			for (int i = minValue; i <= maxValue; i++)
			{
				result.Insert(i);
			}
			return result;
		}

		/// <summary>
		/// Removes every element from a set.
		/// </summary>
		/// <param name="set"></param>
		static void ClearSet(Set set)
		{
			int[] values = new int[set.Size];
			Array.Copy(set.Elements().ToArray(), values, set.Size);
			foreach (int value in values)
			{
				set.Remove(value);
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion


		/// <summary>
		///A test for Set Constructor
		///</summary>
		[TestMethod()]
		public void SetConstructorTest()
		{
			int n = 1337;
			Set target = new Set(n);

			Assert.IsTrue(target.Size == 1);

			var enumerator = target.Elements().GetEnumerator();
			enumerator.MoveNext();
			Assert.AreEqual(n, enumerator.Current);
			Assert.IsFalse(enumerator.MoveNext());
		}

		/// <summary>
		///A test for Set Constructor
		///</summary>
		[TestMethod()]
		public void SetConstructorTest1()
		{
			Set target = new Set();

			Assert.IsTrue(target.Size == 0);

			var enumerator = target.Elements().GetEnumerator();
			Assert.IsFalse(enumerator.MoveNext());
		}

		/// <summary>
		///A test for Contains
		///</summary>
		[TestMethod()]
		public void ContainsTest()
		{
			Set target = new Set();

			// Contains should be false for any value we pass to an empty set.
			for (int i = -500; i < 500; i++)
			{
				Assert.IsFalse(target.Contains(i));
			}

			// After filling the set with some values, assert that they are all there.
			for (int i = -500; i < 500; i++)
			{
				target.Insert(i);
			}
			for (int i = -500; i < 500; i++)
			{
				Assert.IsTrue(target.Contains(i));
			}

			// Another check on values that really should be present.
			foreach (int value in target.Elements())
			{
				Assert.IsTrue(target.Contains(value));
			}

			// Test that a set that contains values returns false on values we know
			// are not actually in the set.
			for (int i = 500; i < 1000; i++)
			{
				Assert.IsFalse(target.Contains(i));
			}
		}

		/// <summary>
		///A test for Elements
		///</summary>
		[TestMethod()]
		public void ElementsTest()
		{
			Set target = new Set();

			var enumerator = target.Elements().GetEnumerator();
			Assert.IsFalse(enumerator.MoveNext());

			// Put some test data in the set.
			var testValues = new int[] { 19, -5, 4 };
			foreach (int value in testValues)
			{
				target.Insert(value);
			}

			// Now assert that we can enumerate as many items as there were
			// tests data values.
			enumerator = target.Elements().GetEnumerator();
			for (int i = 0; i < testValues.Length; i++)
			{
				Assert.IsTrue(enumerator.MoveNext());
			}
			Assert.IsFalse(enumerator.MoveNext());

			// Start over and assert that the values that get enumerated are
			// the values we inserted above.
			enumerator = target.Elements().GetEnumerator();
			foreach (int value in testValues)
			{
				enumerator.MoveNext();
				Assert.IsTrue(testValues.Contains(enumerator.Current));
			}
			Assert.IsFalse(enumerator.MoveNext());

			// Test that the values that get enumerated come out in ascending
			// order.
			int previous = Int32.MinValue;
			foreach (int value in target.Elements())
			{
				Assert.IsTrue(value > previous);
				previous = value;
			}
		}

		/// <summary>
		///A test for Insert
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			Set target = new Set();
			Assert.AreEqual(target.Size, 0);

			const int TEST_SIZE = 100;

			// Assert that the set grows by one with each unique insertion.
			// Also test that prior to insertion, Contains() is false and is
			// true after.
			for (int i = 0; i < TEST_SIZE; i++)
			{
				Assert.IsFalse(target.Contains(i));
				target.Insert(i);
				Assert.IsTrue(target.Contains(i));
				Assert.AreEqual(target.Size, i+1);
			}

			// Assert that inserting a value that is already in the set does
			// not change the size of the set.
			for (int i = 0; i < TEST_SIZE; i++)
			{
				target.Insert(i);
				Assert.AreEqual(target.Size, TEST_SIZE);
			}

			const int TEST_VALUE = -1337;
			target = new Set(TEST_VALUE);
			for (int i = 0; i < 1000; i++)
			{
				Assert.IsTrue(target.Contains(TEST_VALUE));
				Assert.AreEqual(target.Size, 1);
			}
		}

		/// <summary>
		///A test for Remove
		///</summary>
		[TestMethod()]
		public void RemoveTest()
		{
			// Test that removing items from an empty set amounts to no change.
			Set target = new Set();
			target.Remove(4040);
			target.Remove(1234);
			Assert.AreEqual(target.Size, 0);

			target = new Set(1111);
			target.Remove(2535253);
			Assert.AreEqual(target.Size, 1); // Nothing was removed
			target.Remove(1111);
			Assert.AreEqual(target.Size, 0); // Now we got it.


			//
			// Test that the order in which we remove items from the set makes
			// no difference.
			//

			target = GetFilledSet(-25, 24);
			int size = target.Size;
			for (int i = -25; i <= 24; i++)
			{
				Assert.IsTrue(target.Contains(i));
				target.Remove(i);
				Assert.IsFalse(target.Contains(i));
				Assert.AreEqual(target.Size, --size);
			}
			Assert.AreEqual(target.Size, 0);

			target = GetFilledSet(-25, 24);
			size = target.Size;
			for (int i = 24; i >= -25; i--)
			{
				Assert.IsTrue(target.Contains(i));
				target.Remove(i);
				Assert.IsFalse(target.Contains(i));
				Assert.AreEqual(target.Size, --size);
			}
			Assert.AreEqual(target.Size, 0);
		}

		/// <summary>
		///A test for ToString
		///</summary>
		[TestMethod()]
		public void ToStringTest()
		{
			Set target = new Set();
			Assert.AreEqual("{}", target.ToString());

			target.Insert(0);
			Assert.AreEqual("{0}", target.ToString());

			target.Insert(0);
			Assert.AreEqual("{0}", target.ToString());

			target.Insert(-1);
			Assert.AreEqual("{-1, 0}", target.ToString());

			target.Insert(50);
			Assert.AreEqual("{-1, 0, 50}", target.ToString());

			target.Insert(16);
			Assert.AreEqual("{-1, 0, 16, 50}", target.ToString());

			target.Remove(80);
			Assert.AreEqual("{-1, 0, 16, 50}", target.ToString());

			target.Remove(0);
			Assert.AreEqual("{-1, 16, 50}", target.ToString());

			ClearSet(target);
			Assert.AreEqual("{}", target.ToString());
		}

		/// <summary>
		///A test for Size
		///</summary>
		[TestMethod()]
		public void SizeTest()
		{
			Set target = new Set();
			Assert.AreEqual(target.Size, 0);

			target = new Set(100);
			Assert.AreEqual(target.Size, 1);

			target.Insert(100);
			Assert.AreEqual(target.Size, 1);

			target.Insert(99);
			Assert.AreEqual(target.Size, 2);

			target.Remove(100);
			Assert.AreEqual(target.Size, 1);

			target.Remove(100);
			Assert.AreEqual(target.Size, 1);

			target.Remove(150);
			Assert.AreEqual(target.Size, 1);

			target.Remove(99);
			Assert.AreEqual(target.Size, 0);

			for (int i = 0; i < 1000; i++)
			{
				target.Remove(i);
				Assert.AreEqual(target.Size, 0);
			}
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void ExceptionTestImplicit1()
		{
			Set target = new Set();
			target.Insert(1);
			target.Insert(2);
			target.Insert(3);

			foreach (int value in target.Elements())
			{
				target.Insert(1);
			}
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void ExceptionTestImplicit2()
		{
			Set target = new Set();
			target.Insert(9);

			foreach (int value in target.Elements())
			{
				target.Remove(0);
			}
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void ExceptionTestExplicit1()
		{
			Set target = new Set();
			target.Insert(0);

			var e = target.Elements().GetEnumerator();
			e.MoveNext();
			target.Remove(0);
			e.MoveNext();
		}

		[TestMethod]
		public void ExceptionTestExplicit2()
		{
			Set target = new Set();
			target.Insert(90);

			var e = target.Elements().GetEnumerator();
			target.Remove(0);
			e.MoveNext();
			Assert.AreEqual(90, e.Current);

			target.Insert(90);
			bool caughtException = false;
			try
			{
				e.MoveNext();
			}
			catch (InvalidOperationException)
			{
				caughtException = true;
			}
			Assert.IsTrue(caughtException);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void ExceptionTestExplicit3()
		{
			Set target = GetFilledSet(0, 20);
			var e1 = target.Elements().GetEnumerator();
			var e2 = target.Elements().GetEnumerator();
			e1.MoveNext();
			e1.MoveNext();
			target.Insert(0);

			// Even though e1 has called MoveNext() and we've since inserted a
			// value, e2 has not begun enumerating.
			Assert.IsTrue(e2.MoveNext());

			// This will throw an exception.
			e1.MoveNext();
		}
	}
}
