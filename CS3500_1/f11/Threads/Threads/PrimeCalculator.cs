﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.ComponentModel;
using System.Text;

public class PrimeCalculator
{

    public static int FindPrimeN(int n)
    {

        int prime = 2;
        int candidate = 3;
        while (n > 1)
        {
            if (IsPrime(candidate))
            {
                prime = candidate;
                n--;
            }
            candidate += 2;
        }
        return prime;
    }


    public static bool IsPrime(int p)
    {
        for (int i = 3; i <= Math.Sqrt(p); i++)
        {
            if (p % i == 0) return false;
        }
        return true;
    }

        
}