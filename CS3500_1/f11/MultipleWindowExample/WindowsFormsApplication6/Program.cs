﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApplication6
{
    static class Program
    {
        /// <summary>
        /// Example application that brings up two frames, both of which have to
        /// be closed to end execution.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // When a form is run via Application.Run, the main event thread (and thus
            // the application) ends when that frame is closed.  
            //Application.Run(new Form1());

            // When an ApplicationContext is run, it decides when to end the main event
            // thread.
            Application.Run(new MyContext());
        }
    }


    /// <summary>
    /// A custom ApplicationContext.
    /// </summary>
    class MyContext : ApplicationContext
    {
        // Keeps track of how many top-level forms are open.
        private int openWindowCount = 0;

        // Opens two windows and waits for both to be closed before exiting the thead.
        public MyContext()
        {
            // Create two windows
            Form1 form1 = new Form1();
            Form1 form2 = new Form1();

            // Title them
            form1.Text = "First One";
            form2.Text = "Second One";

            // Arrange for a callback when they are closed
            form1.Closed += DealWithClosedWindow;
            form2.Closed += DealWithClosedWindow;

            // Display them
            form1.Show();
            form2.Show();

            // Record that there are two open windows
            openWindowCount += 2;

        }


        /// <summary>
        /// Called whenever a window closes.  If there are no more windows open, exits the
        /// main event thread, ending the application.
        /// </summary>
        private void DealWithClosedWindow(object sender, EventArgs e)
        {
            // Decrement count of open windows
            openWindowCount--;

            // If there are no more, exit the thread.
            if (openWindowCount <= 0)
            {
                ExitThread();
            }
        }

    }
}
