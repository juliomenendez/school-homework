﻿// Written by Joe Zachary for CS 3500, September 2010
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inherit
{


    /// <summary>
    /// An Animal object represents various aspects of a real
    /// animal.  This is an abstract class, so an Animal may
    /// not be directly constructed.  Instead, you must
    /// instantiate one of its derived classes.
    /// </summary>
    public abstract class Animal {

        /// <summary>
        /// Create an Animal with the specified name.
        /// </summary>
        /// <param name="n"></param>
        public Animal (String n) {
            Name = n;
        }

        /// <summary>
        /// Obtain the name of the Animal.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// All Animals say something.  This is abstract, which
        /// will force derived classes to override.
        /// </summary>
        /// <returns></returns>
        public abstract String Speak();

        /// <summary>
        /// An upper-case version of what the animal Speaks plus
        /// any other stuff appended on the end.
        /// </summary>
        /// <returns></returns>
        public virtual String Shout()
        {
            return Speak().ToUpper();
        }

        /// <summary>
        /// By default, an Animal has 8 legs.
        /// </summary>
        public virtual int LegCount
        {
            get { return 8; }
        }


        // Liskov substitution principle
        // Any specified property of the base class must also be true
        // of any derived class.

        /// <summary>
        /// Returns what an animal says when it speaks, repeated n times,
        /// where n > 0.  Behavior is unspecified when n <= 0.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public virtual String SpeakRepeatedly(int n)
        {
            if (n <= 0) return "-----";
            String result = "";
            while (n > 0)
            {
                result += Speak() + " ";
                n--;
            }
            return result;
        }

    }


    /// <summary>
    /// A Dog is a kind of Animal
    /// </summary>
    public class Dog : Animal
    {

        /// <summary>
        /// A Dog has a name and a breed
        /// </summary>
        /// <param name="n"></param>
        /// <param name="b"></param>
        public Dog(String n, String b): base(n)
        {
            Breed = b;
        }

        /// <summary>
        /// Obtain the breed of the Dog
        /// </summary>
        public String Breed { get; private set; }

        /// <summary>
        /// What a dog says.
        /// </summary>
        /// <returns></returns>
        public override String Speak()
        {
            return "woof";
        }

        public override sealed int LegCount
        {
            get
            {
                return 4;
            }
        }


        /// <summary>
        /// Returns what an animal says when it speaks, repeated n times,
        /// where n >= 1.  Behavior is unspecified when n <= 1.
        /// Not a violation of Liskov substitution principle!
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public override string SpeakRepeatedly(int n)
        {
            if (n < 1)
            {
                return "";
            }
            String result = "";
            while (n > 0)
            {
                result += Speak() + " ";
                n--;
            }
            return result;
        }
    }



    /// <summary>
    /// A Spider is a kind of Animal
    /// </summary>
    public class Spider : Animal
    {

        /// <summary>
        /// A Spider has a name and may be poisonous
        /// </summary>
        /// <param name="n"></param>
        /// <param name="b"></param>
        public Spider(String n, bool p)
            : base(n)
        {
            IsPoisonous = p;
        }

        /// <summary>
        /// Find out whether the spider is poisonous
        /// </summary>
        public bool IsPoisonous { get; private set; }

        /// <summary>
        /// What a spider says.
        /// </summary>
        /// <returns></returns>
        public override String Speak()
        {
            return "spin";
        }

        /// <summary>
        /// Overrides Shout and violates Liskov subsitution principle in the process
        /// </summary>
        /// <returns></returns>
        public override string Shout()
        {
            return base.Shout() + "!!!!!!";
        }

    }


}
