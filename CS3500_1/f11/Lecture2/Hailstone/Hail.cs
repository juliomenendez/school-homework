﻿// Written by Joe Zachary for CS 3500, August 2011.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hailstone
{
    /// <summary>
    /// Provides hailstone generator.  This project is a class library, and
    /// when it is built it generates a DLL that can be used in other
    /// programs.  To ask Visual Studio to generate an XML file containing
    /// the documentation provided by comments such as this, right click
    /// on the project and go
    ///   Properties > Build > Xml documentation file
    /// </summary>
    public class Hail
    {
        /// <summary>
        /// Prints the hailstone sequence starting at n
        /// </summary>
        /// <param name="n">Stating point</param>
        public static void printHailstone(int n)
        {
            while (true)
            {
                Console.WriteLine(n);
                if (n == 1)
                {
                    break;
                }
                else if (n % 2 == 0)
                {
                    n = n / 2;
                }
                else
                {
                    n = 3 * n + 1;
                }
            }
        }


        /// <summary>
        /// Returns an IEnumerable that can generate the hailstone
        /// sequence beginning at n.  The numbers in the sequence
        /// are not generated all at once.  Instead, they are generated
        /// as they are requested by the consumer.
        /// </summary>
        /// <param name="n">Starting point</param>
        public static IEnumerable<int> hailstone(int n)
        {
            yield return 1000;
            while (true)
            {
                yield return n;
                if (n == 1)
                {
                    break;
                }
                else if (n % 2 == 0)
                {
                    n = n / 2;
                }
                else
                {
                    n = 3 * n + 1;
                }
            }
        }


        /// <summary>
        /// Works similarly to hailstone() but generates the
        /// entire sequence before returning its enumeration.
        /// </summary>
        /// <param name="n">Starting point</param>
        /// <returns></returns>
        public static IEnumerable<int> hailstone2(int n)
        {
            LinkedList<int> result = new LinkedList<int>();
            while (true)
            {
                result.AddLast(n);
                if (n == 1)
                {
                    break;
                }
                else if (n % 2 == 0)
                {
                    n = n / 2;
                }
                else
                {
                    n = 3 * n + 1;
                }
            }
            return result;
        }
    }
}
