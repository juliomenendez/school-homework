﻿namespace PrimeNumberGenerator
{
    partial class PrimeCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.calculate = new System.Windows.Forms.Button();
            this.count = new System.Windows.Forms.NumericUpDown();
            this.bits = new System.Windows.Forms.NumericUpDown();
            this.primes = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.count)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bits)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.primes);
            this.splitContainer1.Size = new System.Drawing.Size(847, 262);
            this.splitContainer1.SplitterDistance = 196;
            this.splitContainer1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.calculate);
            this.panel1.Controls.Add(this.count);
            this.panel1.Controls.Add(this.bits);
            this.panel1.Location = new System.Drawing.Point(16, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(178, 183);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Primes";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Digits";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(58, 151);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(120, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // calculate
            // 
            this.calculate.Location = new System.Drawing.Point(58, 104);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(117, 23);
            this.calculate.TabIndex = 2;
            this.calculate.Text = "Calculate";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // count
            // 
            this.count.Location = new System.Drawing.Point(58, 56);
            this.count.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.count.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(120, 20);
            this.count.TabIndex = 1;
            this.count.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bits
            // 
            this.bits.Increment = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.bits.Location = new System.Drawing.Point(58, 12);
            this.bits.Maximum = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            this.bits.Minimum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.bits.Name = "bits";
            this.bits.Size = new System.Drawing.Size(120, 20);
            this.bits.TabIndex = 0;
            this.bits.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
            // 
            // primes
            // 
            this.primes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.primes.Location = new System.Drawing.Point(0, 0);
            this.primes.Multiline = true;
            this.primes.Name = "primes";
            this.primes.ReadOnly = true;
            this.primes.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.primes.Size = new System.Drawing.Size(647, 262);
            this.primes.TabIndex = 0;
            this.primes.WordWrap = false;
            // 
            // PrimeCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 262);
            this.Controls.Add(this.splitContainer1);
            this.Name = "PrimeCalculator";
            this.Text = "Prime Number Calculator";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.count)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bits)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.NumericUpDown count;
        private System.Windows.Forms.NumericUpDown bits;
        private System.Windows.Forms.TextBox primes;

    }
}

