﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;


namespace PrimeNumberGenerator
{
    public partial class PrimeCalculator : Form
    {

        private Primes primeSource;

        public PrimeCalculator()
        {
            InitializeComponent();
            primeSource = new Primes(this);
        }


        public void DisplayPrime(String s)
        {
            primes.AppendText(s + "\r\n");
            progressBar1.Value++;
            if (progressBar1.Value == progressBar1.Maximum)
            {
                calculate.Text = "Calculate";
            }
        }


        private void calculate_Click(object sender, EventArgs e)
        {
            if (calculate.Text == "Calculate")
            {
                primes.Clear();
                progressBar1.Maximum = (int)count.Value;
                progressBar1.Value = 0;
                //primeSource.calculatePrimes((int)bits.Value, (int)count.Value);
                Task t = new Task(() =>
                    primeSource.calculatePrimes((int)bits.Value, (int)count.Value));
                t.Start();
                calculate.Text = "Stop";
            }
            else
            {
                primeSource.KillYourself();
                calculate.Text = "Calculate";
            }
        }

    }
}
