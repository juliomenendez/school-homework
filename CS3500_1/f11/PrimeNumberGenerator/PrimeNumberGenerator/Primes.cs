﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

namespace PrimeNumberGenerator
{
    class Primes
    {
        private Random rand;
        private PrimeCalculator calculator;


        public Primes(PrimeCalculator pc)
        {
            calculator = pc;
            rand = new Random();
        }
 

        public void calculatePrimes (int bits, int count) {
            stop = false;
            for (int i = 0; i < count; i++) {
                if (stop) return;
                BigInteger prime = findPrime(bits);
                //calculator.DisplayPrime(prime.ToString());
                if (stop) return;
                Action op = () => calculator.DisplayPrime(prime.ToString());
                calculator.Invoke(op);
            }
        }

        public BigInteger findPrime(int bits)
        {
            byte[] randomBytes = new byte[(int)Math.Ceiling(bits / 8.0) + 1];
            rand.NextBytes(randomBytes);
            randomBytes[randomBytes.Length-1] = 0;
            BigInteger b = new BigInteger(randomBytes);
            Thread.Sleep(2*bits);  // Simulates checking for primality
            return b;
        }


        private Boolean stop;

        public void KillYourself()
        {
            stop = true;
        }
    }
}
    