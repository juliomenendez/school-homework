﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpreadsheetTester
{


    /// <summary>
    ///This is a test class for SpreadsheetTest and is intended
    ///to contain all SpreadsheetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FivePointTests: Utilities
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        public void MediumSheet(AbstractSpreadsheet ss)
        {
            Set(ss, "A1", "1.0");
            Set(ss, "A2", "2.0");
            Set(ss, "A3", "3.0");
            Set(ss, "A4", "4.0");
            Set(ss, "B1", "= A1 A2 +");
            Set(ss, "B2", "= A3 A4 *");
            Set(ss, "C1", "= B1 B2 +");
            VV(ss, "A1", 1.0, "A2", 2.0, "A3", 3.0, "A4", 4.0, "B1", 3.0, "B2", 12.0, "C1", 15.0);
            Set(ss, "A1", "2.0");
            VV(ss, "A1", 2.0, "A2", 2.0, "A3", 3.0, "A4", 4.0, "B1", 4.0, "B2", 12.0, "C1", 16.0);
            Set(ss, "B1", "= A1 A2 /");
            VV(ss, "A1", 2.0, "A2", 2.0, "A3", 3.0, "A4", 4.0, "B1", 1.0, "B2", 12.0, "C1", 13.0);
        }

        [TestMethod()]
		[TestCategory("Five")]
		public void MediumSave()
        {
            Spreadsheet ss = new Spreadsheet();
            MediumSheet(ss);
            ss.Save("save7.txt");
            AbstractSpreadsheet s = ss.Read("save7.txt");
            VV(s, "A1", 2.0, "A2", 2.0, "A3", 3.0, "A4", 4.0, "B1", 1.0, "B2", 12.0, "C1", 13.0);
        }


        public void MakeTree(Spreadsheet ss, int line)
        {
            Set(ss, "A" + (line + 0), "1.0");
            Set(ss, "A" + (line + 1), "2.0");
            Set(ss, "A" + (line + 2), "3.0");
            Set(ss, "A" + (line + 3), "4.0");
            Set(ss, "A" + (line + 4), "5.0");
            Set(ss, "A" + (line + 5), "6.0");
            Set(ss, "A" + (line + 6), "7.0");
            Set(ss, "A" + (line + 7), "7.0");

            Set(ss, "B" + (line + 0), "= A" + (line + 0) + " A" + (line + 1) + " +");
            Set(ss, "B" + (line + 1), "= A" + (line + 2) + " A" + (line + 3) + " -");
            Set(ss, "B" + (line + 2), "= A" + (line + 4) + " A" + (line + 5) + " *");
            Set(ss, "B" + (line + 3), "= A" + (line + 6) + " A" + (line + 7) + " /");

            Set(ss, "C" + (line + 0), "= B" + (line + 0) + " B" + (line + 1) + " +");
            Set(ss, "C" + (line + 1), "= B" + (line + 2) + " B" + (line + 3) + " -");

            Set(ss, "D" + (line + 0), "= C" + (line + 0) + " C" + (line + 1) + " *");
        }

        [TestMethod()]
		[TestCategory("Five")]
        public void SmallTreeTest()
        {
            Spreadsheet ss = new Spreadsheet();
            MakeTree(ss, 1);
            VV(ss, "D1", 58.0);
        }


        [TestMethod()]
		[TestCategory("Five")]
		public void MediumTreeTest()
        {
            Spreadsheet ss = new Spreadsheet();
            MakeTree(ss, 1);
            MakeTree(ss, 9);
            Set(ss, "E1", "= D1 D9 +");
            VV(ss, "E1", 116.0);
        }


        [TestMethod()]
		[TestCategory("Five")]
		public void LargeTreeTest()
        {
            Spreadsheet ss = new Spreadsheet();
            MakeLargeTree(ss);
            VV(ss, "F1", 116.0);
        }

        public Spreadsheet MakeLargeTree(Spreadsheet ss)
        {
            MakeTree(ss, 1);
            MakeTree(ss, 9);
            Set(ss, "E1", "= D1 D9 +");
            MakeTree(ss, 20);
            MakeTree(ss, 29);
            Set(ss, "E20", "= D20 D29 /");
            Set(ss, "F1", "= E1 E20 *");
            return ss;
        }


    }
}
