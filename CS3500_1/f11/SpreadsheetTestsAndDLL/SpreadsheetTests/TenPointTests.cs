﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpreadsheetTester
{


    /// <summary>
    ///This is a test class for SpreadsheetTest and is intended
    ///to contain all SpreadsheetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TenPointTests: Utilities
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A long formula test
        ///</summary>
        [TestMethod()]
		[TestCategory("Ten")]
		public void LongFormulaTest()
        {
            Spreadsheet s = new Spreadsheet();
            s.SetCellContents("sum", "= a0 a1 +");
            int i = 0;
            int depth = 100;
            for (i = 0; i < depth * 2; i += 2)
            {
                s.SetCellContents("a" + i, "= a" + (i + 2) + " a" + (i + 3) + " +");
                s.SetCellContents("a" + (i + 1), "= a" + (i + 2) + " a" + (i + 3) + " +");
            }
            s.SetCellContents("a" + i, "1");
            s.SetCellContents("a" + (i + 1), "1");
            Assert.AreEqual(Math.Pow(2, depth + 1), s.GetCellValue("sum"));
            s.SetCellContents("a" + i, "0");
            Assert.AreEqual(Math.Pow(2, depth), s.GetCellValue("sum"));
            s.SetCellContents("a" + (i + 1), "0");
            Assert.AreEqual(0.0, s.GetCellValue("sum"));

            s.Save("save.txt");
            AbstractSpreadsheet r = s.Read("save.txt");
            Assert.AreEqual(0.0, s.GetCellValue("sum"));
            r.SetCellContents("a" + i, "1");
            r.SetCellContents("a" + (i + 1), "1");
            Assert.AreEqual(Math.Pow(2, depth + 1), r.GetCellValue("sum"));
        }





        [TestMethod()]
		[TestCategory("Ten")]
		public void StressTest()
        {
            Spreadsheet ss = new Spreadsheet();
            new FivePointTests().MakeLargeTree(ss);

            // Does it compute properly
            VV(ss, "F1", 116.0);

            // Division by zero detection?
            Set(ss, "A8", "0.0");
            Assert.IsInstanceOfType(ss.GetCellValue("F1"), typeof(FormulaError));

            // Change it back
            Set(ss, "A8", "7.0");
            VV(ss, "F1", 116.0);

            // Dependency on string detection?
            Set(ss, "C1", "hello");
            Assert.IsInstanceOfType(ss.GetCellValue("F1"), typeof(FormulaError));
            
            // Change it back
            Set(ss, "C1", "= B1 B2 +");
            VV(ss, "F1", 116.0);
            
            // Circular dependency
            try
            {
                Set(ss, "B1", "= E1 E1 +");
                Assert.Fail();
            }
            catch (CircularException)
            {
            }
            VV(ss, "F1", 116.0);

            // Change in middle of tree
            Set(ss, "C1", "0.0");
            VV(ss, "C1", 0.0);
            VV(ss, "F1", 58.0);
            Set(ss, "C1", "= B1 B2 +");
            VV(ss, "F1", 116.0);

            // Lots of changes
            for (int i = 1000; i >= 1; i--)
            {
                Set(ss, "A20", "" + (1.0 * i));
                VV(ss, "F1", (i+1) * 58.0);
            }

            // Replace with a formula
            Set(ss, "C2", "= A1 A20 +");
            VV(ss, "F1", 62.0);

            // Change a dependee
            Set(ss, "A1", "0.0");
            VV(ss, "F1", 59.0);

        }

    }
}
