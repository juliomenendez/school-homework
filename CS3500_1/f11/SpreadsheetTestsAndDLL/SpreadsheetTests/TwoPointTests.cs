﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace SpreadsheetTester
{


    /// <summary>
    ///This is a test class for SpreadsheetTest and is intended
    ///to contain all SpreadsheetTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TwoPointTests: Utilities
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
		[TestCategory("Two")]
		public void EmptySheet()
        {
            Spreadsheet ss = new Spreadsheet();
            VV(ss, "A", "");
            VC(ss, "A", "");
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void OneString()
        {
            Spreadsheet ss = new Spreadsheet();
            OneString(ss);
        }

        public void OneString(Spreadsheet ss)
        {
            Set(ss, "B", "hello");
            VV(ss, "B", "hello");
            VC(ss, "B", "hello");
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void OneNumber()
        {
            Spreadsheet ss = new Spreadsheet();
            OneNumber(ss);
        }

        public void OneNumber(Spreadsheet ss)
        {
            Set(ss, "C", "17.5");
            VV(ss, "C", 17.5);
            VC(ss, "C", "17.5");
        }


        [TestMethod()]
		[TestCategory("Two")]
        public void OneFormula()
        {
            Spreadsheet ss = new Spreadsheet();
            OneFormula(ss);
        }

        public void OneFormula(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "B", "5.2");
            Set(ss, "C", "= A B +");
            VV(ss, "A", 4.1, "B", 5.2, "C", 9.3);
            VC(ss, "A", "4.1", "B", "5.2", "C", "= A B +");
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void Changed()
        {
            Spreadsheet ss = new Spreadsheet();
            Assert.IsFalse(ss.Changed);
            Set(ss, "C", "17.5");
            Assert.IsTrue(ss.Changed);
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void Circular()
        {
            Spreadsheet ss = new Spreadsheet();
            Circular(ss);
        }

        public void Circular(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "B", "5.2");
            try
            {
                Set(ss, "C", "= A C +");
                Assert.Fail();
            }
            catch (CircularException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void BadVariable()
        {
            Spreadsheet ss = new Spreadsheet();
            BadVariable(ss);
        }

        public void BadVariable(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "#B#", "5.2");
            try
            {
                Set(ss, "C", "= A #B# +");
                Assert.Fail();
            }
            catch (BadFormulaException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void TooFewOperands()
        {
            Spreadsheet ss = new Spreadsheet();
            TooFewOperands(ss);
        }

        public void TooFewOperands(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            try
            {
                Set(ss, "C", "= A +");
                Assert.Fail();
            }
            catch (BadFormulaException)
            {
            }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void TooManyOperands()
        {
            Spreadsheet ss = new Spreadsheet();
            TooFewOperands(ss);
        }

        public void TooManyOperands(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            try
            {
                Set(ss, "C", "= A A A +");
                Assert.Fail();
            }
            catch (BadFormulaException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void DivisionByZero()
        {
            Spreadsheet ss = new Spreadsheet();
            DivisionByZero(ss);
        }

        public void DivisionByZero(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "B", "0.0");
            Set(ss, "C", "= A B /");
            Assert.IsInstanceOfType(ss.GetCellValue("C"), typeof(FormulaError));
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void EmptyArgument()
        {
            Spreadsheet ss = new Spreadsheet();
            EmptyArgument(ss);
        }

        public void EmptyArgument(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "C", "= A B +");
            Assert.IsInstanceOfType(ss.GetCellValue("C"), typeof(FormulaError));
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void StringArgument()
        {
            Spreadsheet ss = new Spreadsheet();
            StringArgument(ss);
        }

        public void StringArgument(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "B", "hello");
            Set(ss, "C", "= A B +");
            Assert.IsInstanceOfType(ss.GetCellValue("C"), typeof(FormulaError));
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void ErrorArgument()
        {
            Spreadsheet ss = new Spreadsheet();
            ErrorArgument(ss);
        }

        public void ErrorArgument(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "B", "");
            Set(ss, "C", "= A B +");
            Set(ss, "D", "= C");
            Assert.IsInstanceOfType(ss.GetCellValue("D"), typeof(FormulaError));
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void NumberFormula1()
        {
            Spreadsheet ss = new Spreadsheet();
            NumberFormula1(ss);
        }

        public void NumberFormula1(Spreadsheet ss)
        {
            Set(ss, "A", "4.1");
            Set(ss, "C", "= A 4.2 +");
            VV(ss, "C", 8.3);
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void NumberFormula2()
        {
            Spreadsheet ss = new Spreadsheet();
            NumberFormula2(ss);
        }

        public void NumberFormula2(Spreadsheet ss)
        {
            Set(ss, "A", "= 4.6");
            VV(ss, "A", 4.6);
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void RepeatSimpleTests()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A", "17.32");
            Set(ss, "B", "This is a test");
            Set(ss, "C", "= A A +");
            OneString(ss);
            OneNumber(ss);
            OneFormula(ss);
            Circular(ss);
            BadVariable(ss);
            TooFewOperands(ss);
            TooManyOperands(ss);
            DivisionByZero(ss);
            StringArgument(ss);
            ErrorArgument(ss);
            NumberFormula1(ss);
            NumberFormula2(ss);
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void Formulas()
        {
            Spreadsheet ss = new Spreadsheet();
            Formulas(ss);
        }

        public void Formulas(Spreadsheet ss)
        {
            Set(ss, "A", "4.4");
            Set(ss, "B", "2.2");
            Set(ss, "C", "= A B +");
            Set(ss, "D", "= A B -");
            Set(ss, "E", "= A B *");
            Set(ss, "F", "= A B /");
            VV(ss, "C", 6.6, "D", 2.2, "E", 4.4 * 2.2, "F", 2.0);
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void CellNames1()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A", "2.0");
            Set(ss, "B", "hello");
            Set(ss, "C", "= A A +");
            HashSet<string> names = new HashSet<string>(ss.GetAllCellNames());
            HashSet<string> expected = new HashSet<string>() { "A", "B", "C" };
            Assert.IsTrue(expected.SetEquals(names));
        }

        [TestMethod()]
		[TestCategory("Two")]
		public void CellNames2()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A", "2.0");
            Set(ss, "B", "hello");
            Set(ss, "C", "= A A +");
            HashSet<string> names = new HashSet<string>(ss.GetAllCellNames());
            HashSet<string> expected = new HashSet<string>() { "A", "B", "C" };
            Assert.IsTrue(expected.SetEquals(names));
            Set(ss, "B", "");
            expected.Remove("B");
            names = new HashSet<string>(ss.GetAllCellNames());
            Assert.IsTrue(expected.SetEquals(names));
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void Multiple()
        {
            Spreadsheet s1 = new Spreadsheet();
            Spreadsheet s2 = new Spreadsheet();
            Set(s1, "X", "hello");
            Set(s2, "X", "goodbye");
            VV(s1, "X", "hello");
            VV(s2, "X", "goodbye");
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest1()
        {
            Spreadsheet ss = new Spreadsheet();
            try
            {
                ss.Save("x:\\missing\\save.txt");
                Assert.Fail();
            }
            catch (SpreadsheetWriteException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest2()
        {
            Spreadsheet ss = new Spreadsheet();
            try
            {
                ss.Read("x:\\missing\\save.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest3()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A", "hello");
            ss.Save("save1.txt");
            using (StreamReader reader = new StreamReader("save1.txt"))
            {
                Assert.AreEqual("A", reader.ReadLine());
                Assert.AreEqual("hello", reader.ReadLine());
                Assert.IsNull(reader.ReadLine());
            }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest4()
        {
            using (StreamWriter writer = new StreamWriter("save2.txt"))
            {
                writer.WriteLine("A");
                writer.WriteLine("2.5");
                writer.WriteLine("B");
            }
            try
            {
                Spreadsheet ss = new Spreadsheet();
                ss.Read("save2.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest5()
        {
            using (StreamWriter writer = new StreamWriter("save3.txt"))
            {
                writer.WriteLine("A");
                writer.WriteLine("");
                writer.WriteLine("B");
                writer.WriteLine("hello");
            }
            try
            {
                Spreadsheet ss = new Spreadsheet();
                ss.Read("save3.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest6()
        {
            using (StreamWriter writer = new StreamWriter("save4.txt"))
            {
                writer.WriteLine("A");
                writer.WriteLine("hello");
                writer.WriteLine("A");
                writer.WriteLine("goodbye");
            }
            try
            {
                Spreadsheet ss = new Spreadsheet();
                ss.Read("save4.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest7()
        {
            using (StreamWriter writer = new StreamWriter("save5.txt"))
            {
                writer.WriteLine("A");
                writer.WriteLine("= B B +");
                writer.WriteLine("B");
                writer.WriteLine("3.5");
            }
            try
            {
                Spreadsheet ss = new Spreadsheet();
                ss.Read("save5.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void SaveTest8()
        {
            using (StreamWriter writer = new StreamWriter("save6.txt"))
            {
                writer.WriteLine("A");
                writer.WriteLine("= A #bogus# +");
            }
            try
            {
                Spreadsheet ss = new Spreadsheet();
                ss.Read("save6.txt");
                Assert.Fail();
            }
            catch (SpreadsheetReadException) { }
        }


        [TestMethod()]
		[TestCategory("Two")]
		public void ChangeTest1()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A1", "7.0");
            Set(ss, "A2", "8.0");
            Set(ss, "A3", "= A1 A2 +");
            VV(ss, "A3", 15.0);
            Set(ss, "A1", "5.0");
            VV(ss, "A3", 13.0);
        }

        [TestMethod()]
		[TestCategory("Two")]
		public void ChangeTest2()
        {
            Spreadsheet ss = new Spreadsheet();
            Set(ss, "A1", "5.0");
            Set(ss, "A2", "6.0");
            Set(ss, "A3", "= A1 A2 *");
            Set(ss, "A4", "= A3 A1 +");
            VV(ss, "A4", 35.0);
            Set(ss, "A3", "= A1 A2 +");
            VV(ss, "A4", 16.0);
        }

        

    }
}
