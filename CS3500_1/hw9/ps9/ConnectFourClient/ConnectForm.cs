﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConnectFourClient
{
    public partial class ConnectForm : Form
    {
        public string Username { get; set; }
        public string ServerIP { get; set; }
        public string ServerPort { get; set; }
        public string Password { get; set; }

        /// <summary>
        /// Create a new instance of the form.
        /// </summary>
        public ConnectForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handler for the Connect button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connectButton_Click(object sender, EventArgs e)
        {
            Username = usernameTextBox.Text.Trim();
            Password = passwordTextBox.Text.Trim();
            ServerIP = serverIpTextBox.Text.Trim();
            ServerPort = serverPortTextBox.Text.Trim();
            if (Username.Length == 0 || ServerIP.Length == 0 || ServerPort.Length == 0 || Password.Length == 0)
            {
                MessageBox.Show("All fields are required. Please try again.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.None;
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }

        /// <summary>
        /// Handler for the Quick button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Handler for the Form shown event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectForm_Shown(object sender, EventArgs e)
        {
            usernameTextBox.Text = Username;
            passwordTextBox.Text = Password;
            serverIpTextBox.Text = ServerIP;
            serverPortTextBox.Text = ServerPort;
        }
    }
}
