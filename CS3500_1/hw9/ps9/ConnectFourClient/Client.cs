﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;

namespace ConnectFourClient
{
    /// <summary>
    /// Delegate for the Disconnected event of the Client class.
    /// Triggered whenever the server disconnects.
    /// </summary>
    /// <param name="reason">the reason for the disconnection.</param>
    public delegate void DisconnectedHandler(string reason);

    /// <summary>
    /// Delegate for the rejected event triggered when a
    /// users credentials are not accepted by the server.
    /// </summary>
    public delegate void RejectedHandler();

    /// <summary>
    /// Delegate for the ExceptionRaised event of the Client class.
    /// Triggered when there is an exception.
    /// </summary>
    /// <param name="message">message describing the exception</param>
    public delegate void ExceptionRaisedHandler(string message);

    /// <summary>
    /// Delegate for the GameStarted event of the Client class.
    /// Triggered when the client receives the "play" command.
    /// </summary>
    /// <param name="opponentName">the name of the opponent</param>
    /// <param name="timeout">the number of seconds the player has to make a move</param>
    /// <param name="youColor">the color of this client</param>
    public delegate void GameStartedHandler(string opponentName, string winLossDrawRecord, string dateOfLastGame,  int timeout, string youColor);

    /// <summary>
    /// Delegate for the GameTicking event of the Client class.
    /// Triggered when the client receives the "tick" command.
    /// </summary>
    /// <param name="seconds">number of seconds remaining</param>
    public delegate void GameTickingHandler(string color, int seconds);

    /// <summary>
    /// Delegate for the LegalMovement event.
    /// </summary>
    public delegate void GameLegalHandler();

    /// <summary>
    /// Handler for recieving the movement of another player.
    /// </summary>
    /// <param name="column">column they moved to.</param>
    public delegate void GameMovementHandler(int column);

    /// <summary>
    /// Handles a game ending in a win.
    /// </summary>
    /// <param name="color">Color of the winning player.</param>
    public delegate void GameWinnerHandler(String color);

    /// <summary>
    /// Handles a player disconnecting.
    /// </summary>
    /// <param name="color">Color of the player who disconnected.</param>
    public delegate void GamePlayerDisconnectedHandler(String color);

    /// <summary>
    /// Handles a player resigning.
    /// </summary>
    /// <param name="color">Color of the player who resigned.</param>
    public delegate void GamePlayerResignedHandler(String color);

    /// <summary>
    /// Handler for a player running out of time.
    /// </summary>
    /// <param name="color">Color of the player who ran out of time.</param>
    public delegate void GameTimeoutHandler(string color);

    /// <summary>
    /// Handler for the game ending with a draw.
    /// </summary>
    public delegate void GameDrawHandler();

    class Client
    {
        private IPAddress address;
        private int port;
        private string username;
        private string password;
        private TcpClient tcpClient;
        private string readData;
        private byte[] readBuffer;
        private String lastReceivedCommand;
        private ArrayList lastReceivedArguments = new ArrayList();

        public string Name { get { return username; } }

        /// <summary>
        /// Event definitions. For documentation about each event refer to the corresponding
        /// delegate.
        /// </summary>
        public event DisconnectedHandler Disconnected;
        public event ExceptionRaisedHandler ExceptionRaised;
        public event GameStartedHandler GameStarted;
        public event GameTickingHandler GameTicking;
        public event GameLegalHandler GameLegal;
        public event GameMovementHandler GameMovement;
        public event GamePlayerDisconnectedHandler GamePlayerDisconnected;
        public event GamePlayerResignedHandler GamePlayerResigned;
        public event GameTimeoutHandler GameTimeout;
        public event GameWinnerHandler GameWinner;
        public event GameDrawHandler GameDraw;
        public event RejectedHandler Rejected;

        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="username">Username to log in to the server</param>
        /// <param name="address">IP address of the server to connect. Defaults to 'localhost'</param>
        /// <param name="port">Port where the server is listening to. Defaults to 4000</param>
        public Client(string username, string password, string address = "localhost", int port = 4000)
        {
            try
            {
                this.address = IPAddress.Parse(address == "localhost" ? "127.0.0.1" : address);
            }
            catch
            {
                TriggerDisconnected("Invalid IP address");
                return;
            }
            this.username = username;
            this.password = password;
            this.port = port;

            tcpClient = new TcpClient();
            tcpClient.BeginConnect(address, port, tcpClientConnectCallback, null);
        }

        /// <summary>
        /// Method called from the TcpClient.BeginConnect call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void tcpClientConnectCallback(IAsyncResult result)
        {
            try
            {
                tcpClient.EndConnect(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(ex.ToString());
                return;
            }

            // Login to the server.
            sendCommand(string.Format("@login\n#{0}\n#{1}", username, password));

            // Client connected.
            readBuffer = new byte[1024];
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginRead call.
        /// </summary>
        /// <param name="result">Result of the async call</param>
        private void streamReadCallback(IAsyncResult result)
        {
            int readCount = 0;
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                readCount = stream.EndRead(result);
            }
            catch (Exception ex)
            {
                TriggerDisconnected(string.Format("Error reading stream. {0}", ex.Message));
            }

            if (readCount == 0)
            {
                tcpClient.Close();
                TriggerDisconnected("");
            }

            readData += Encoding.UTF8.GetString(readBuffer, 0, readCount);

            int index;
            while ((index = readData.IndexOf("\n")) > 0)
            {
                string command = readData.Substring(0, index + 1);
                ProcessCommand(command);
                readData = readData.Substring(index + 1);
            }

            if (tcpClient.Connected)
                // Back to reading from the connection.
                stream.BeginRead(readBuffer, 0, readBuffer.Length, streamReadCallback, null);
        }

        /// <summary>
        /// Takes the string comming from the server, parses it and triggers the corresponding events.
        /// </summary>
        /// <param name="command">string comming from the server with a valid command</param>
        private void ProcessCommand(string command)
        {
            command = command.Trim();
            if (command[0] == '@')
            {
                lastReceivedCommand = command.Substring(1);
                lastReceivedArguments = new ArrayList();

                switch (lastReceivedCommand)
                {
                    case "legal":
                        if(GameLegal != null)
                            GameLegal();
                        break;
                    case "draw":
                        if (GameDraw != null)
                            GameDraw();
                        break;
                    case "full":
                        break;
                    case "rejected":
                        Rejected();
                        break;
                    case "accepted":
                        break;


                }
            }
            else if (command[0] == '#')
            {
                lastReceivedArguments.Add(command.Substring(1));
                if (lastReceivedArguments.Count == 1)
                {
                    string argument = (string)lastReceivedArguments[0];
                    switch (lastReceivedCommand)
                    {
                        case "ignoring":
                            break;
                        case "move":
                            if (GameMovement != null)
                                GameMovement(Convert.ToInt16(argument));
                                
                            break;
                        case "win":
                            if(GameWinner != null)
                                GameWinner(argument);
                            break;
                        case "disconnected":
                            if(GamePlayerDisconnected != null)
                                GamePlayerDisconnected(argument);
                            break;
                        case "resigned":
                            if(GamePlayerResigned != null)
                                GamePlayerResigned(argument);
                            break;
                        case "time":
                            if(GameTimeout != null)
                                GameTimeout(argument);
                            break;
                    }
                }
                if (lastReceivedArguments.Count == 2)
                {
                    string argument = (string)lastReceivedArguments[0];
                    string argument2 = (string)lastReceivedArguments[1];

                    switch (lastReceivedCommand)
                    {
                        case "tick":
                            if(GameTicking != null)
                                GameTicking(argument, Convert.ToInt16(argument2));
                            break;
                    }

                }
                if(lastReceivedArguments.Count == 5)
                {
                    string argument1 = (string)lastReceivedArguments[0];
                    string argument2 = (string)lastReceivedArguments[1];
                    string argument3 = (string)lastReceivedArguments[2];
                    string argument4 = (string)lastReceivedArguments[3];
                    string argument5 = (string)lastReceivedArguments[4];

                    switch (lastReceivedCommand)
                    {
                        case "play":
                            if(GameStarted != null)
                                GameStarted(argument1, argument2, argument3, Convert.ToInt16(argument4), argument5);
                            break;
                    }
                }  
            }


        }

        /// <summary>
        /// Starts an async send to the server.
        /// </summary>
        /// <param name="command">command to send</param>
        private void sendCommand(string command)
        {
            byte[] outgoingBytes = Encoding.UTF8.GetBytes(command + "\n");
            NetworkStream stream = tcpClient.GetStream();
            stream.BeginWrite(outgoingBytes, 0, outgoingBytes.Length, streamWriteCallback, null);
        }

        /// <summary>
        /// Method called from the NetworkStream.BeginWrite call
        /// </summary>
        /// <param name="result">result of the async call</param>
        private void streamWriteCallback(IAsyncResult result)
        {
            NetworkStream stream = tcpClient.GetStream();
            try
            {
                stream.EndWrite(result);
            }
            catch (Exception ex)
            {
                if (ExceptionRaised != null)
                    ExceptionRaised(string.Format("Error sending command to the server. {0}", ex.Message));
                else
                    throw new Exception("Error sending command to the server.", ex);
            }
        }

        /// <summary>
        /// Helper method that triggers the Disconnected event.
        /// </summary>
        /// <param name="reason">reason of the disconnection</param>
        private void TriggerDisconnected(string reason)
        {
            if (Disconnected != null)
                Disconnected(reason);
        }

        /// <summary>
        /// Sends a move command to the server.
        /// </summary>
        /// <param name="column">number of column to move to</param>
        public void sendMove(int column)
        {
            sendCommand(string.Format("@move\r\n#{0}", column));
        }

        /// <summary>
        /// Sends the resign command.
        /// </summary>
        public void Resign()
        {
            sendCommand("@resign");
        }
    }
}
