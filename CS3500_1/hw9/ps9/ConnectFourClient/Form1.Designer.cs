﻿namespace ConnectFourClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clockLabel = new System.Windows.Forms.Label();
            this.resignButton = new System.Windows.Forms.Button();
            this.opponentNameRadio = new System.Windows.Forms.RadioButton();
            this.youNameRadio = new System.Windows.Forms.RadioButton();
            this.opponentColorLabel = new System.Windows.Forms.Label();
            this.yourColorLabel = new System.Windows.Forms.Label();
            this.DateAndRecord = new System.Windows.Forms.Label();
            this.gameBoard = new ConnectFourClient.GameBoard();
            this.SuspendLayout();
            // 
            // clockLabel
            // 
            this.clockLabel.AutoSize = true;
            this.clockLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clockLabel.Location = new System.Drawing.Point(620, 61);
            this.clockLabel.Name = "clockLabel";
            this.clockLabel.Size = new System.Drawing.Size(96, 69);
            this.clockLabel.TabIndex = 5;
            this.clockLabel.Text = "30";
            this.clockLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resignButton
            // 
            this.resignButton.Location = new System.Drawing.Point(632, 143);
            this.resignButton.Name = "resignButton";
            this.resignButton.Size = new System.Drawing.Size(75, 23);
            this.resignButton.TabIndex = 6;
            this.resignButton.Text = "Resign";
            this.resignButton.UseVisualStyleBackColor = true;
            this.resignButton.Click += new System.EventHandler(this.resignButton_Click);
            // 
            // opponentNameRadio
            // 
            this.opponentNameRadio.Enabled = false;
            this.opponentNameRadio.Location = new System.Drawing.Point(670, 12);
            this.opponentNameRadio.Name = "opponentNameRadio";
            this.opponentNameRadio.Size = new System.Drawing.Size(108, 17);
            this.opponentNameRadio.TabIndex = 7;
            this.opponentNameRadio.TabStop = true;
            this.opponentNameRadio.UseVisualStyleBackColor = true;
            // 
            // youNameRadio
            // 
            this.youNameRadio.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.youNameRadio.Enabled = false;
            this.youNameRadio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.youNameRadio.Location = new System.Drawing.Point(555, 12);
            this.youNameRadio.Name = "youNameRadio";
            this.youNameRadio.Size = new System.Drawing.Size(108, 17);
            this.youNameRadio.TabIndex = 8;
            this.youNameRadio.TabStop = true;
            this.youNameRadio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.youNameRadio.UseVisualStyleBackColor = true;
            // 
            // opponentColorLabel
            // 
            this.opponentColorLabel.Location = new System.Drawing.Point(669, 32);
            this.opponentColorLabel.Name = "opponentColorLabel";
            this.opponentColorLabel.Size = new System.Drawing.Size(15, 15);
            this.opponentColorLabel.TabIndex = 9;
            // 
            // yourColorLabel
            // 
            this.yourColorLabel.Location = new System.Drawing.Point(648, 32);
            this.yourColorLabel.Name = "yourColorLabel";
            this.yourColorLabel.Size = new System.Drawing.Size(15, 15);
            this.yourColorLabel.TabIndex = 10;
            // 
            // DateAndRecord
            // 
            this.DateAndRecord.Location = new System.Drawing.Point(632, 219);
            this.DateAndRecord.Name = "DateAndRecord";
            this.DateAndRecord.Size = new System.Drawing.Size(146, 132);
            this.DateAndRecord.TabIndex = 12;
            this.DateAndRecord.Text = "label1";
            // 
            // gameBoard
            // 
            this.gameBoard.CurrentBrush = null;
            this.gameBoard.Location = new System.Drawing.Point(12, 12);
            this.gameBoard.Name = "gameBoard";
            this.gameBoard.Size = new System.Drawing.Size(512, 470);
            this.gameBoard.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 494);
            this.Controls.Add(this.DateAndRecord);
            this.Controls.Add(this.yourColorLabel);
            this.Controls.Add(this.opponentColorLabel);
            this.Controls.Add(this.gameBoard);
            this.Controls.Add(this.resignButton);
            this.Controls.Add(this.clockLabel);
            this.Controls.Add(this.youNameRadio);
            this.Controls.Add(this.opponentNameRadio);
            this.Name = "MainForm";
            this.Text = "Connect Four";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GameBoard gameBoard;
        private System.Windows.Forms.Label clockLabel;
        private System.Windows.Forms.Button resignButton;
        private System.Windows.Forms.RadioButton opponentNameRadio;
        private System.Windows.Forms.RadioButton youNameRadio;
        private System.Windows.Forms.Label opponentColorLabel;
        private System.Windows.Forms.Label yourColorLabel;
        private System.Windows.Forms.Label DateAndRecord;
    }
}

