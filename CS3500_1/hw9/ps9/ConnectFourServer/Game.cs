﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ConnectFourServer
{
    /// <summary>
    /// Determines the next adjacent position.
    /// </summary>
    /// <param name="row">current row</param>
    /// <param name="col">current column</param>
    /// <param name="newRow">adjacent row</param>
    /// <param name="newCol">adjacent column</param>
    delegate void NextAdjacent(int row, int col, out int newRow, out int newCol);

    /// <summary>
    /// Delegate for the GameEnded event
    /// </summary>
    /// <param name="game">the game ending</param>
    delegate void GameEnded(Game game);

    class Game
    {
        private Player player1, player2, currentPlayer = null;
        int timeout;
        int[,] board = new int[7, 6];
        CancellationTokenSource tickingCancellation;
        Task tickingTask;
        int gameId = -1;

        static private String sqlTemplate1 = @"SELECT COUNT(DISTINCT Game_ID, User_1_ID, User_2_ID) AS C FROM `juliog`.`User`
                                LEFT JOIN `juliog`.`Game` ON `juliog`.`User`.`User_ID` = `juliog`.`Game`.`User_1_ID` OR 
                                    `juliog`.`User`.`User_ID` = `juliog`.`Game`.`User_2_ID`
                                WHERE ((`juliog`.`Game`.`User_1_ID` = @playerId1 AND `juliog`.`Game`.`User_2_ID` = @playerId2) OR
                                    (`juliog`.`Game`.`User_1_ID` = @playerId2 AND `juliog`.`Game`.`User_2_ID` = @playerId1)) 
                                    AND `juliog`.`Game`.`Winner_ID` {0}";

        static private String sqlTemplate2 = @"SELECT Timestamp FROM `juliog`.`User`
                                LEFT JOIN `juliog`.`Game` ON `juliog`.`User`.`User_ID` = `juliog`.`Game`.`User_1_ID` OR 
                                    `juliog`.`User`.`User_ID` = `juliog`.`Game`.`User_2_ID`
                                WHERE ((`juliog`.`Game`.`User_1_ID` = @playerId1 AND `juliog`.`Game`.`User_2_ID` = @playerId2) OR
                                    (`juliog`.`Game`.`User_1_ID` = @playerId2 AND `juliog`.`Game`.`User_2_ID` = @playerId1))
                                ORDER BY Timestamp DESC
                                LIMIT 1";
        static private String connectionString =
           "server=atr.eng.utah.edu;database=juliog;uid=juliog;password=00665929";

        /// <summary>
        /// Creates a new game and starts waiting for the first player to move.
        /// </summary>
        /// <param name="player1">represents the first player that would become the black one</param>
        /// <param name="player2">represents the second player that would become the white one</param>
        /// <param name="timeout">>maximum time a player has to make a move</param>
        public Game(Player player1, Player player2, int timeout)
        {
            this.player1 = player1;
            this.player2 = player2;
            this.player1.Disconnected += new Disconnected(player_Disconnected);
            this.player2.Disconnected += new Disconnected(player_Disconnected);
            this.player1.Moved += new Moved(player_Moved);
            this.player2.Moved += new Moved(player_Moved);
            this.player1.Resign += new Resign(player_Resign);
            this.player2.Resign += new Resign(player_Resign);
            this.timeout = timeout;
            
            for (int i = 0; i < 7; i++)
                for (int j = 0; j < 6; j++)
                    board[i, j] = 0;

            int wins, losses, draws;
            string lastGame;
            RetrieveGameStats(out wins, out losses, out draws, out lastGame);

            UpdateGameStats();

            this.player1.setupGame("black", timeout, this.player2.Name, wins, losses, draws, lastGame);
            this.player2.setupGame("white", timeout, this.player1.Name, losses, wins, draws, lastGame);

            changeTurn();
        }

        /// <summary>
        /// Gets the game statistics for the 2 players of this game. NOTE that the values are 
        /// refering to player1.
        /// </summary>
        /// <param name="wins">number of wins of player1 when playing exactly with player2</param>
        /// <param name="losses">number of losses of player1 when playing exactly with player2</param>
        /// <param name="draws">number of draws of player1 when playing exactly with player2</param>
        /// <param name="lastGame">datetime of the last game player1 played against player2</param>
        private void RetrieveGameStats(out int wins, out int losses, out int draws, out string lastGame)
        {
            wins = losses = draws = 0;
            lastGame = "none";
            MySqlConnection conn = new MySqlConnection(connectionString);
            try
            {
                conn.Open();
                string[] conditions = { String.Format("= {0}", player1.UserID), String.Format("= {0}", player2.UserID),
                                          "IS NULL" };
                int[] results = new int[3];
                MySqlCommand command;
                MySqlDataReader reader;
                for(int i=0; i < conditions.Length; i++)
                {
                    command = conn.CreateCommand();
                    command.CommandText = String.Format(sqlTemplate1, conditions[i]);
                    command.Prepare();
                    command.Parameters.AddWithValue("@playerId1", player1.UserID);
                    command.Parameters.AddWithValue("@playerId2", player2.UserID);

                    reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        results[i] = reader.GetInt16("C");
                        
                    }
                    else
                    {
                        results[i] = 0;
                    }
                    reader.Close();
                }

                command = conn.CreateCommand();
                command.CommandText = String.Format(sqlTemplate2);
                command.Prepare();
                command.Parameters.AddWithValue("@playerId1", player1.UserID);
                command.Parameters.AddWithValue("@playerId2", player2.UserID);

                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    lastGame = DateTime.Parse(reader.GetString("Timestamp")).ToString();
                    Console.WriteLine(lastGame);
                }
                reader.Close();
                wins = results[0];
                losses = results[1];
                draws = results[2];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        private void UpdateGameStats(int winnerId=-1)
        {
            MySqlConnection conn = new MySqlConnection(connectionString);
            try
            {
                conn.Open();

                MySqlCommand command = conn.CreateCommand();
                if (gameId == -1)
                {
                    command.CommandText = "INSERT INTO Game(User_1_ID, User_2_ID) VALUES(@player1Id, @player2Id)";
                    command.Prepare();
                    command.Parameters.AddWithValue("@player1Id", player1.UserID);
                    command.Parameters.AddWithValue("@player2Id", player2.UserID);
                    command.ExecuteNonQuery();
                    gameId = (int)command.LastInsertedId;
                }
                else
                {
                    command.CommandText = "UPDATE Game SET Winner_ID=@winnerId WHERE Game_ID=@gameId";
                    command.Prepare();
                    if(winnerId == -1)
                        command.Parameters.AddWithValue("@winnerId", null);
                    else
                        command.Parameters.AddWithValue("@winnerId", winnerId);
                    command.Parameters.AddWithValue("@gameId", gameId);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// Triggered when the game ends.
        /// </summary>
        public event GameEnded GameEnded;

        /// <summary>
        /// Handles a player resigning.
        /// </summary>
        /// <param name="player"></param>
        void player_Resign(Player player)
        {
            UpdateGameStats(opponent(player).UserID);
            opponent(player).sendCommand(string.Format("@resigned\r\n#{0}", player.Color));
            forceEnd();
        }

        /// <summary>
        /// Handles a player moving.
        /// </summary>
        /// <param name="player">Player making the move.</param>
        /// <param name="pos">Position they moved to.</param>
        void player_Moved(Player player, int pos)
        {
            if (player != currentPlayer)
                return;

            int row, col;
            translateMoveCoords(pos, out row, out col);

            if (row == -1)
            {
                currentPlayer.sendCommand("@full");
                return;
            }
            tickingCancellation.Cancel();

            board[col, row] = (currentPlayer == player1) ? 1 : 2;

            bool won = false;
            bool draw = false;
            // Check if the player has 4 adjancent horizontally
            int total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r; newCol = c + 1; });
            if (total >= 3)
                won = true;

            if (!won)
            {
                // Check if the player has 4 adjancent vertically
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the player has 4 adjancent diagonally raising
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c + 1; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the player has 4 adjancent diagonally falling
                total =
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r - 1; newCol = c - 1; }) +
                    countAdjacent(row, col, (int r, int c, out int newRow, out int newCol) => { newRow = r + 1; newCol = c + 1; });
                if (total >= 3)
                    won = true;
            }

            if (!won)
            {
                // Check if the board is filled making the game a draw
                draw = true;
                for (int i = 0; i < 7; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        draw = board[i, j] != 0;
                        if (!draw)
                            break;
                    }
                    if (!draw)
                        break;
                }
            }

            string opponentCommand = string.Format("@move\r\n#{0}", pos);
            string secondaryCommand = "";
            if (won)
            {
                UpdateGameStats(currentPlayer.UserID);
                secondaryCommand = string.Format("@win\r\n#{0}", currentPlayer.Color);
            }
            else if (draw)
                secondaryCommand = "@draw";
            if (won || draw)
            {
                opponent(currentPlayer).sendCommand(string.Format("{0}\r\n{1}", opponentCommand, secondaryCommand));
                currentPlayer.sendCommand(string.Format("@legal\r\n{0}", secondaryCommand));
                forceEnd();
            }
            else
            {
                opponent(currentPlayer).sendCommand(opponentCommand);
                currentPlayer.sendCommand("@legal");
                changeTurn();
            }
        }

        /// <summary>
        /// Counts the number of adjacent positions with the same value
        /// </summary>
        /// <param name="row">row of the starting position</param>
        /// <param name="col">column of the starting position</param>
        /// <param name="nextAdjacent">method that calculates the next adjacent position</param>
        /// <returns></returns>
        int countAdjacent(int row, int col, NextAdjacent nextAdjacent)
        {
            int count = 0;
            int playerValue = board[col, row];
            int newRow, newCol;
            while (true)
            {
                try
                {
                    nextAdjacent(row, col, out newRow, out newCol);
                    if (newRow < 0 || newRow > 5 || newCol < 0 || newCol > 6 || board[newCol, newRow] != playerValue)
                    {
                        break;
                    }
                    else
                    {
                        count++;
                        row = newRow;
                        col = newCol;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return count;
        }
       
        /// <summary>
        /// Changes the current player and restarts the timer.
        /// </summary>
        void changeTurn()
        {
            
            currentPlayer = (currentPlayer == null) ? player1 : opponent(currentPlayer);
            tickingCancellation = new CancellationTokenSource();
            tickingTask = new Task(tickingHandler, tickingCancellation.Token);
            tickingTask.Start();
            
        }

        /// <summary>
        /// Decrements the move clock and waits for cancel if player moves before clock = 0.
        /// </summary>
        void tickingHandler()
        {
            lock (this)
            {
                if (tickingCancellation.IsCancellationRequested)
                    return;

                currentPlayer.Timeout--;
                string command;
                if (currentPlayer.Timeout == 0)
                {
                    command = string.Format("@time\r\n#{0}", currentPlayer.Color);
                }
                else
                {
                    command = string.Format("@tick\r\n#{0}\r\n#{1}", currentPlayer.Color, currentPlayer.Timeout.ToString());
                }

                player1.sendCommand(command);
                player2.sendCommand(command);

                if (currentPlayer.Timeout == 0)
                {
                    forceEnd();
                    return;
                }
                else
                {
                    Thread.Sleep(1000);
                    tickingHandler();
                }
            }
        }

        /// <summary>
        /// Handles a player disconnecting.
        /// </summary>
        /// <param name="player">The player that disconnected</param>
        void player_Disconnected(Player player)
        {
            tickingCancellation.Cancel();
            Player other = opponent(player);
            if (!other.IsDisconnected)
                UpdateGameStats(other.UserID);
            string commandFormat = "@disconnected\r\n#{0}";
            other.sendCommand(string.Format(commandFormat, player.Color));
        }

        /// <summary>
        /// Returns the player's opponent.
        /// </summary>
        /// <param name="player">a player instance in this game</param>
        /// <returns>the opponent to <paramref name="player"/> in this game</returns>
        Player opponent(Player player)
        {
            return (player == this.player1) ? this.player2 : this.player1;
        }

        /// <summary>
        /// Translates a one integer move position to a x,y coordinate compatible
        /// with the game board.
        /// </summary>
        /// <param name="movePos">position sent by the client</param>
        /// <param name="row">variable where to store the calculated row. If -1 then column is full.</param>
        /// <param name="col">variable where to store the calculated column</param>
        public void translateMoveCoords(int movePos, out int row, out int col)
        {
            row = -1;
            col = movePos-1;
            for (int i = 0; i < 6; i++)
            {
                if (board[col, i] == 0)
                {
                    row = i;
                    return;
                }
            }
        }

        /// <summary>
        /// Method called when the server shutsdown. Disconnects both players.
        /// </summary>
        public void forceEnd()
        {
            tickingCancellation.Cancel();
            player1.disconnectGracefully();
            player2.disconnectGracefully();
            if (GameEnded != null)
                GameEnded(this);
        }
    }
}
