﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Collections;
using MySql.Data.MySqlClient;

namespace ConnectFourServer
{
    /// <summary>
    /// Handler for the connected event.
    /// </summary>
    /// <param name="player">player instance that connected</param>
    /// <param name="name">name of the player</param>
    public delegate void Connected(Player player, string name);

    /// <summary>
    /// Handler for the disconnected event.
    /// </summary>
    /// <param name="player">player instance that disconnected</param>
    public delegate void Disconnected(Player player);

    /// <summary>
    /// Handler for the moved event.
    /// </summary>
    /// <param name="player">player instance that made the move</param>
    /// <param name="pos">position the player moved to</param>
    public delegate void Moved(Player player, int pos);

    /// <summary>
    /// Handler for the resign event.
    /// </summary>
    /// <param name="player">player instance that resigned</param>
    public delegate void Resign(Player player);

    /// <summary>
    /// Handler for the logged in event.
    /// </summary>
    /// <param name="player">Player that logged in.</param>
    /// <param name="userName">The player's user name</param>
    /// <param name="password">The player's password.</param>
    public delegate void LoggedIn(Player player, String userName, String password);


    /// <summary>
    /// Represents the player on the server. Handles all the communication with the client.
    /// </summary>
    public class Player
    {
        private Socket socket;
        private string name;
        private byte[] buffer = new byte[1024];
        private string receivedData;
        public int Timeout { get; set; }
        public Boolean IsDisconnected { get; set; }
        private int userId;

        private const String connectionString =
           "server=atr.eng.utah.edu;database=juliog;uid=juliog;password=00665929";

        /// <summary>
        /// Creates a player instance. Start listening on the socket.
        /// </summary>
        /// <param name="socket">Socket connected to the client.</param>
        public Player(Socket socket, int timeout)
        {
            Timeout = timeout;
            IsDisconnected = false;
            this.Connected += new ConnectFourServer.Connected(Player_Connected);
            this.Disconnected += new ConnectFourServer.Disconnected(Player_Disconnected);
            this.LoggedIn += new ConnectFourServer.LoggedIn(Player_LoggedIn);
            this.socket = socket;
            try
            {
                socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            catch (Exception)
            {

            }
        }

        public int UserID { get { return userId; } }

        /// <summary>
        /// Handles when a user attempts to log in.
        /// </summary>
        /// <param name="player">The player that logged in.</param>
        /// <param name="userName">The players user name.</param>
        /// <param name="password">The players password.</param>
        void Player_LoggedIn(Player player, string userName, string password)
        {
            MySqlConnection conn = new MySqlConnection(connectionString);
            try
            {
                conn.Open();
                MySqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT User_ID FROM User WHERE Name = @name AND Password = @password";
                command.Prepare();
                command.Parameters.AddWithValue("@name", userName);
                command.Parameters.AddWithValue("@password", password);

                MySqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    userId = reader.GetInt16("User_ID");
                    sendCommand("@accepted");
                    if (Connected != null)
                        Connected(this, userName);
                }
                else
                {
                    sendCommand("@rejected");
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Handles when a player disconnects.
        /// </summary>
        /// <param name="player">The player that disconnected.</param>
        void Player_Disconnected(Player player)
        {
            IsDisconnected = true;
        }

        /// <summary>
        /// Handles when the user sends the name to use in the game.
        /// </summary>
        /// <param name="player">player instance triggering the event</param>
        /// <param name="name">name of the player</param>
        void Player_Connected(Player player, string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Callback for Socket receiving.
        /// </summary>
        /// <param name="result">Represents the results from the receive</param>
        void ReceiveCallback(IAsyncResult result)
        {
            int receivedByteCount = 0;
            try
            {
                receivedByteCount = socket.EndReceive(result);
            }
            catch (Exception)
            {
            }

            if (receivedByteCount == 0)
            {
                socket.Close();
                Disconnected(this);
            }
            else
            {
                receivedData += Encoding.Default.GetString(buffer, 0, receivedByteCount);
                parseReceivedData();

                try
                {
                    socket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, null);
                }
                catch (Exception)
                {
                    socket.Close();
                    Disconnected(this);
                }
            }
        }

        /// <summary>
        /// Triggers when the user logs in.
        /// </summary>
        public event Connected Connected;

        /// <summary>
        /// Triggers whenever the user connection is lost.
        /// </summary>
        public event Disconnected Disconnected;

        /// <summary>
        /// Triggers when the user makes a move.
        /// </summary>
        public event Moved Moved;

        /// <summary>
        /// Triggers when the user resigns.
        /// </summary>
        public event Resign Resign;

        /// <summary>
        /// Triggers when a user logs in.
        /// </summary>
        public event LoggedIn LoggedIn;

        public string Color { get; set; }
        public string Name { get { return name; } }

        /// <summary>
        /// Sends a command to the client.
        /// </summary>
        /// <param name="command">command to send.</param>
        public void sendCommand(string command)
        {
            if (IsDisconnected)
                return;

            Console.WriteLine(command);
            lock (this)
            {
                byte[] buffer = Encoding.Default.GetBytes(command + "\r\n");
                try
                {
                    socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, SendCallback, null);
                }
                catch (Exception)
                {
                    Disconnected(this);
                }
            }
        }

        /// <summary>
        /// Callback for Socket sending
        /// </summary>
        /// <param name="result">Represents the results from the async send.</param>
        void SendCallback(IAsyncResult result)
        {
            try
            {
                socket.EndSend(result);
            }
            catch (Exception)
            {
                Disconnected(this);
            }
        }

        /// <summary>
        /// Setups the needed information to play on a game.
        /// </summary>
        /// <param name="color">color assigned to this player</param>
        /// <param name="timeout">maximum time the player has to make a move</param>
        /// <param name="opponent">name of the opponent</param>
        /// <param name="wins">Number of wins of this player against the current opponent</param>
        /// <param name="losses">Number of losses of this player against the current opponent</param>
        /// <param name="draws">Number of draws of this player against the current opponent</param>
        public void setupGame(string color, int timeout, string opponent, int wins, int losses, int draws, string lastGame)
        {
            Color = color;
            string gameStats = string.Format("{0}-{1}-{2}", wins.ToString(), losses.ToString(), draws.ToString());
            string command = string.Format("@play\r\n#{0}\r\n#{1}\r\n#{2}\r\n#{3}\r\n#{4}", opponent, gameStats, lastGame, timeout, color);
            sendCommand(command);
        }

        /// <summary>
        /// Parses the data received from the socket.
        /// </summary>
        public void parseReceivedData()
        {
            int index;
            string command;
            ArrayList arguments = new ArrayList();
            command = "";
            int waitForArgument = 0;
            string originalData = receivedData;

            while ((index = receivedData.IndexOf('\n')) > 0)
            {
                string line = receivedData.Substring(0, index);
                if(line.EndsWith("\r")) 
                {
                    line = line.Substring(0, index - 1);
                }
                if (waitForArgument > arguments.Count)
                {
                    arguments.Add(line);
                }
                else
                {
                    if (line == "@login")
                        waitForArgument = 2;
                    else if (line == "@move")
                        waitForArgument = 1;
                   
                    command = line;
                }
                receivedData = receivedData.Substring(index + 1);
            }

            bool commandHandled = false;
            switch (command)
            {
                case "@login":
                    if (arguments.Count == 2 && ((string)arguments[0]).StartsWith("#") && ((string)arguments[1]).StartsWith("#"))
                    {
   
                        commandHandled = true;
                        LoggedIn(this, ((string)arguments[0]).Substring(1), ((string)arguments[1]).Substring(1));
                    }
                    break;
                case "@move":
                    if (arguments.Count == 1 && ((string)arguments[0]).StartsWith("#"))
                    {
                        commandHandled = true;
                        Moved(this, Convert.ToInt16(((string)arguments[0]).Substring(1)));
                    }
                    break;
                case "@resign":
                    commandHandled = true;
                    Resign(this);
                    break;

            }

            Console.WriteLine(originalData);
            // If the command or argument hasn't been received entirely restore the received data so far
            // and hope the next receive callback will process it.
            if (!commandHandled)
                receivedData = originalData;
        }

        /// <summary>
        /// Closes the socket gracefully.
        /// </summary>
        public void disconnectGracefully()
        {
            IsDisconnected = true;
            try
            {
                socket.Shutdown(SocketShutdown.Both);
                IAsyncResult iasyncresult = socket.BeginDisconnect(false, (x) => { }, null);
                socket.EndDisconnect(iasyncresult);
            }
            catch (Exception)
            {
            }
        }
    }
       
}
