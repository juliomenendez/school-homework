﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.CodeDom.Compiler;

namespace SS
{
    public class GenericSpreadsheetWriteException : SpreadsheetWriteException
    {

    }

    public class GenericSpreadsheetReadException : SpreadsheetReadException
    {

    }

    public class GenericBadFormulaException : BadFormulaException
    {

    }

    public class GenericFormulaError : FormulaError
    {
        private string reason;

        public GenericFormulaError(string reason)
        {
            this.reason = reason;
        }

        public override string Reason
        {
            get { return reason; }
        }
    }


    public class Cell
    {
        private string name;
        private string content;
        private object value;
        private bool changed;
        private Dictionary<string, Cell> dependents;

        /// <summary>
        /// Creates a new Cell object with no content. Initializes the dependents dictionary with no elements.
        /// </summary>
        /// <param name="name">String that identifies this cell</param>
        public Cell(string name)
        {
            this.name = name;
            Content = "";
            changed = true;
            dependents = new Dictionary<string, Cell>();
        }

        /// <summary>
        /// Creates a new Cell object with <paramref name="content"/> as the content.
        /// </summary>
        /// <param name="name">String that identifies this cell</param>
        /// <param name="content">String representing the content of this cell</param>
        public Cell(string name, string content) : this(name)
        {
            Content = content;
        }

        /// <summary>
        /// String content of the cell.
        /// </summary>
        public string Content
        {
            get
            {
                return content;
            }

            set
            {
                changed = true;
                this.content = value;
            }
        }

        /// <summary>
        /// Name of the cell.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }
        
        /// <summary>
        /// Adds <paramref name="dependent"/> to the dependents dictionary
        /// </summary>
        /// <param name="dependent">Cell object to add</param>
        public void AddDependent(Cell dependent)
        {
            if (!dependents.ContainsKey(dependent.Name))
                dependents.Add(dependent.Name, dependent);
        }

        /// <summary>
        /// Removes <paramref name="dependent"/> from the dependents dictionary
        /// </summary>
        /// <param name="dependent">Cell object to remove</param>
        public void RemoveDependent(Cell dependent)
        {
            if (dependents.ContainsKey(dependent.Name))
                dependents.Remove(dependent.Name);
        }

        /// <summary>
        /// Returns an enumerable with the dependent cells of this cell directly.
        /// </summary>
        /// <returns>IEnumerable of </returns>
        public IEnumerable<Cell> GetDependents()
        {
            foreach (Cell dep in dependents.Values)
            {
                yield return dep;
            }
        }

        /// <summary>
        /// Value of the cell.
        /// </summary>
        public object Value
        {
            get
            {
                if (value is Formula)
                {
                    try
                    {
                        Formula _value = (Formula)value;
                        if (changed)
                            _value.Calculate();
                        return _value.Value;
                    }
                    catch (GenericBadFormulaException)
                    {
                        return new GenericFormulaError("");
                    }
                } else
                    return value;
            }
            set
            {
                if (this.value is Formula)
                {
                    Formula val = (Formula)this.value;
                    foreach (Cell c in val.GetCells())
                    {
                        c.RemoveDependent(this);
                    }
                }
                if (value is Formula)
                {
                    Formula val = (Formula)value;
                    foreach (Cell c in val.GetCells())
                    {
                        c.AddDependent(this);
                    }
                }
                this.value = value;
            }
        }
    }

    public class Formula
    {
        private LinkedList<object> formula;
        private double value;
        private bool valueCalculated;

        /// <summary>
        /// Creates a Formula. Parses the <paramref name="content"/> creating the 
        /// linked list with all the operands and operators. The linked list ends 
        /// with Cells and doubles as operands and strings as operators
        /// <paramref name="cells"/> is a the Spreadsheet.cells variable where to 
        /// look for the cells objects. Its not stored locally.
        /// </summary>
        /// <param name="content">The string representing the formula</param>
        /// <param name="cells">The Spreadsheet.cells variable with all the cells defined 
        /// so far.</param>
        public Formula(string content, Dictionary<string, Cell> cells)
        {
            formula = new LinkedList<object>();
            valueCalculated = false;
            Array pieces = content.Split(' ');
            int operandsCount = 0;
            int operatorsCount = 0;
            foreach (string piece in pieces)
            {
                if (piece == "=")
                    continue;
                object convertedValue = null;
                try
                {
                    convertedValue = Convert.ToDouble(piece);
                    operandsCount++;
                }
                catch (FormatException)
                {
                    if (convertedValue == null && (piece == "+" || piece == "-" || piece == "/" || piece == "*"))
                    {
                        convertedValue = piece;
                        operatorsCount++;
                    }
                    else
                    {
                        CodeDomProvider provider = CodeDomProvider.CreateProvider("C#");
                        if (!provider.IsValidIdentifier(piece))
                            throw new GenericBadFormulaException();

                        if (convertedValue == null && cells.ContainsKey(piece))
                        {
                            convertedValue = cells[piece];
                            operandsCount++;
                        }
                        else
                        {
                            convertedValue = piece;
                            operandsCount++;
                        }
                    }
                }
                formula.AddLast(convertedValue);
            }
            if (operandsCount - 1 != operatorsCount)
            {
                throw new GenericBadFormulaException();
            }
        }

        /// <summary>
        /// Calculated value of this formula.
        /// </summary>
        public double Value
        {
            get
            {
                if (!valueCalculated)
                {
                    Calculate();
                }
                return value;
            }
        }

        public void Calculate()
        {
            LinkedList<object> formulaCopy = new LinkedList<object>(formula);
            int index = 0;
            while (formulaCopy.Count > 1)
            {
                object current = formulaCopy.ElementAt(index);
                if (current is double || current is Cell)
                {
                    index++;
                    continue;
                }
                string currentStr = (string)current;
                if (current is string && currentStr != "+" && currentStr != "-" && currentStr != "/" && currentStr != "*")
                    throw new GenericBadFormulaException();
                string op = (string) current;
                formulaCopy.Remove(op);
                object item = formulaCopy.First();
                formulaCopy.RemoveFirst();
                double operand1 = Convert.ToDouble((item is Cell) ? ((Cell)item).Value : (double)item);
                item = formulaCopy.First();
                formulaCopy.RemoveFirst();
                double operand2 = Convert.ToDouble((item is Cell) ? ((Cell)item).Value : (double)item);
                switch(op)
                {
                    case "+":
                        formulaCopy.AddFirst((double)operand1 + (double)operand2);
                        break;
                    case "-":
                        formulaCopy.AddFirst((double)operand1 - (double)operand2);
                        break;
                    case "/":
                        if (operand2 == 0)
                            throw new GenericBadFormulaException();
                        formulaCopy.AddFirst((double)operand1 / (double)operand2);
                        break;
                    case "*":
                        formulaCopy.AddFirst((double)operand1 * (double)operand2);
                        break;
                    default:
                        // TODO: Throw an exception.
                        break;
                }
                index = 0;
            }

            object lastOne = formulaCopy.First();
            try
            {
                value = Convert.ToDouble((lastOne is Cell) ? ((Cell)lastOne).Value : (double)lastOne);
                valueCalculated = true;
            }
            catch
            {
                throw new GenericBadFormulaException();
            }
        }

        /// <summary>
        /// Returns an enumerable of all the Cell objects in this Formula
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Cell> GetCells()
        {
            foreach (object current in formula)
            {
                if (current is Cell)
                    yield return (Cell)current;
            }
        }
    }

    public class Spreadsheet : AbstractSpreadsheet
    {
        private Dictionary<string, Cell> cells;
        private bool changed;

        /// <summary>
        /// Creates a new Spreadsheet.
        /// </summary>
        public Spreadsheet()
        {
            cells = new Dictionary<string, Cell>();
            changed = false;
        }

        public override void Save(string filename)
        {
            // Create a sorted list of cells based on their dependencies and 
            // writes the output file using that order.
            // Sets changed to false
            changed = false;
        }

        public override AbstractSpreadsheet Read(string filename)
        {
            bool isName = true;
            string currentName = "";
            Spreadsheet ss = new Spreadsheet();
            foreach(string line in System.IO.File.ReadLines(filename))
            {
                if(isName)
                    currentName = line;
                else 
                {
                    ss.SetCellContents(currentName, line);
                }
            }

            return ss;
        }

        public override IEnumerable<string> GetAllCellNames()
        {
            foreach (string name in cells.Keys)
                if(cells[name].Value != "")
                    yield return name;
        }

        public override bool Changed
        {
            get
            {
                return changed;
            }
            protected set
            {
                changed = value;
            }
        }

        public override object GetCellValue(string name)
        {
            if (cells.ContainsKey(name))
                try
                {
                    return cells[name].Value;
                }
                catch (FormatException) 
                {
                    return new GenericFormulaError("");
                }
            return "";
        }

        public override string GetCellContents(string name)
        {
            if (cells.ContainsKey(name))
                return cells[name].Content;
            return "";
        }

        public override IEnumerable<string> SetCellContents(string name, string newContents)
        {
            // Checks if a cell with `name` already exists on the dictionary. If yes then 
            // try to change its content and update all its dependants as changed. Create 
            // a new cell otherwise.
            // Check if `newContents` starts with = and create a Formula object with it and
            // set the value of the cell with it. If not, try to create a double and set the 
            // value with it. Set the value to `newContents`.
            Cell current;
            if (cells.ContainsKey(name))
                current = cells[name];
            else
            {
                current = new Cell(name, newContents);
                cells[name] = current;
            }
            try
            {
                current.Value = Convert.ToDouble(newContents);
            }
            catch (FormatException)
            {
                if (newContents.StartsWith("="))
                {
                    current.Value = new Formula(newContents, cells);
                }
                else
                {
                    current.Value = newContents;
                }
            }
            changed = true;
            List<string> names = new List<string>();
            foreach(Cell c in current.GetDependents())
                names.Add(c.Name);
            return names;
        }

        protected override IEnumerable<string> GetDependents(string name)
        {
            List<string> names = new List<string>();
            if (cells.ContainsKey(name))
            {
                foreach (Cell current in cells[name].GetDependents())
                {
                    names.Add(current.Name);
                }
            }
            return names;
        }
    }
}
