﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SS
{
    public class GenericSpreadsheetWriteException : SpreadsheetWriteException
    {

    }

    public class GenericSpreadsheetReadException : SpreadsheetReadException
    {

    }

    public class GenericBadFormulaException : BadFormulaException
    {

    }

    public class GenericFormulaError : FormulaError
    {
        private string reason;

        public GenericFormulaError(string reason)
        {
            this.reason = reason;
        }

        public override string Reason
        {
            get { return reason; }
        }
    }


    public class Cell
    {
        private string name;
        private string content;
        private object value;
        private bool changed;
        private Dictionary<string, Cell> dependents;

        /// <summary>
        /// Creates a new Cell object with no content. Initializes the dependents dictionary with no elements.
        /// </summary>
        /// <param name="name">String that identifies this cell</param>
        public Cell(string name)
        {
            this.name = name;
            Content = "";
            changed = true;
            dependents = new Dictionary<string, Cell>();
        }

        /// <summary>
        /// Creates a new Cell object with <paramref name="content"/> as the content.
        /// </summary>
        /// <param name="name">String that identifies this cell</param>
        /// <param name="content">String representing the content of this cell</param>
        public Cell(string name, string content) : this(name)
        {
            Content = content;
        }

        /// <summary>
        /// String content of the cell.
        /// </summary>
        public string Content
        {
            get
            {
                return content;
            }

            set
            {
                changed = true;
                this.content = value;
            }
        }

        /// <summary>
        /// Name of the cell.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
        }
        
        /// <summary>
        /// Adds <paramref name="dependent"/> to the dependents dictionary
        /// </summary>
        /// <param name="dependent">Cell object to add</param>
        public void AddDependent(Cell dependent)
        {
            // If `dependent` is not a dependent already of this cell then add it to the dependents dictionary.
        }

        /// <summary>
        /// Removes <paramref name="dependent"/> from the dependents dictionary
        /// </summary>
        /// <param name="dependent">Cell object to remove</param>
        public void RemoveDependent(Cell dependent)
        {
            // Removes `dependent` from the dictionary of dependents
        }

        /// <summary>
        /// Returns an enumerable with the dependent cells of this cell directly.
        /// </summary>
        /// <returns>IEnumerable of </returns>
        public IEnumerable<Cell> GetDependents()
        {
            yield return null;
        }

        /// <summary>
        /// Value of the cell.
        /// </summary>
        public object Value
        {
            get
            {
                if (value is Formula)
                {
                    Formula _value = (Formula)value;
                    if (changed) 
                        _value.Calculate();
                    return _value.Value;
                } else
                    return value;
            }
            set
            {
                this.value = value;
            }
        }
    }

    public class Formula
    {
        private LinkedList<object> formula;
        private double value;

        /// <summary>
        /// Creates a Formula. Parses the <paramref name="content"/> creating the 
        /// linked list with all the operands and operators. The linked list ends 
        /// with Cells and doubles as operands and strings as operators
        /// <paramref name="cells"/> is a the Spreadsheet.cells variable where to 
        /// look for the cells objects. Its not stored locally.
        /// </summary>
        /// <param name="content">The string representing the formula</param>
        /// <param name="cells">The Spreadsheet.cells variable with all the cells defined 
        /// so far.</param>
        public Formula(string content, Dictionary<string, Cell> cells)
        {
            formula = new LinkedList<object>();
            // Parse content and fill formula with Cells, doubles and strings as operators
            Calculate();
        }

        /// <summary>
        /// Calculated value of this formula.
        /// </summary>
        public double Value
        {
            get
            {
                return value;
            }
        }

        public void Calculate()
        {
            // Calculate the result value of this formula and store it in value.
        }
    }

    public class Spreadsheet : AbstractSpreadsheet
    {
        private Dictionary<string, Cell> cells;
        private bool changed;

        /// <summary>
        /// Creates a new Spreadsheet.
        /// </summary>
        public Spreadsheet()
        {
            cells = new Dictionary<string, Cell>();
            changed = false;
        }

        public override void Save(string filename)
        {
            // Create a sorted list of cells based on their dependencies and 
            // writes the output file using that order.
            // Sets changed to false
        }

        public override AbstractSpreadsheet Read(string filename)
        {
            // Creates a new instance of Spreadsheet and reads the file setting 
            // the cells as specified in it.
            return new Spreadsheet();
        }

        public override IEnumerable<string> GetAllCellNames()
        {
            yield return null;
        }

        public override bool Changed
        {
            get
            {
                return changed;
            }
            protected set
            {
                changed = value;
            }
        }

        public override object GetCellValue(string name)
        {
            // Find the cell with name `name` in the dictionary and return its value.
            return null;
        }

        public override string GetCellContents(string name)
        {
            // Find the cell with name `name` in the dictionary and return its content.
            return "";
        }

        public override IEnumerable<string> SetCellContents(string name, string newContents)
        {
            // Checks if a cell with `name` already exists on the dictionary. If yes then 
            // try to change its content and update all its dependants as changed. Create 
            // a new cell otherwise.
            // Check if `newContents` starts with = and create a Formula object with it and
            // set the value of the cell with it. If not, try to create a double and set the 
            // value with it. Set the value to `newContents`.
            yield return null;
        }

        protected override IEnumerable<string> GetDependents(string name)
        {
            // Returns an IEnumerable of the names of the dependent cells of the cell `name`. 
            // If such cell doesn't exists return empty IEnumerable.
            yield return null;
        }
    }
}
