﻿using SS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SpreadsheetTests
{
    
    
    /// <summary>
    ///This is a test class for FormulaTest and is intended
    ///to contain all FormulaTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FormulaTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Formula Constructor
        ///</summary>
        [TestMethod()]
        public void FormulaConstructorTest()
        {
            string content = "B A +"; 
            Dictionary<string, Cell> cells = new Dictionary<string,Cell>();
            cells["A"] = new Cell("A", "15");
            cells["B"] = new Cell("B", "1.5");
            Formula target = new Formula(content, cells);
            Assert.IsInstanceOfType(target, typeof(Formula));
        }

        /// <summary>
        ///A test for Calculate method and Value property
        ///</summary>
        [TestMethod()]
        public void CalculateTest()
        {
            string content = "B A +";
            Dictionary<string, Cell> cells = new Dictionary<string, Cell>();
            cells["A"] = new Cell("A", "15");
            cells["B"] = new Cell("B", "1.5");
            Formula target = new Formula(content, cells);
            target.Calculate();
            Assert.AreEqual(target.Value, 15.5);
        }
    }
}
