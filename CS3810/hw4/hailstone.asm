# Julio C. Menendez - u0665929

	.data
prompt:
	.asciiz "Enter a positive integer: "
negativeInputMsg:
	.asciiz "Error: negative number entered."
resultPart1:
	.asciiz "The hailstone series starting from "
resultPart2:
	.asciiz " converges in "
resultPart3:
	.asciiz " iteration(s)."
overflowMsg:
	.asciiz "Overflow during iteration #"

	.text
main:
# Show message to the user asking for an integer
	la $a0, prompt
	li $v0, 4
	syscall
	
# Wait for the user input
	li $v0, 5
	syscall

	move $s1, $v0	   # Save the integer entered by the user to $s1

	blez $s1, negativeInputError # If the integer is less or equal to zero show message and exit

	addi $s2, $zero, 0 # In $s2 is the loop counter
	addi $s3, $zero, 1 # In $s3 is the number 1 to compare on each iteration
	move $s4, $s1	   # In $s4 is the current iteration result
	
mainLoop:
	bltz $s4, overflowError # If $s4 < 0 show message and exit
	
	# Print current iteration ...
	move $a0, $s4
	li $v0, 1
	syscall
	
	# ... with a space after it
	li $a0, 32
	li $v0, 11
	syscall
	
	beq $s4, $s3, printResult # If $s4 == 1 exits the loop, prints the result and exit
		
	andi $t1, $s4, 1   # AND with 1 to keep the last bit
	beq $t1, $zero, calcForEven # If $t1 == 0 $s4 is even so branch to calcForEven function
	move $a0, $s4 	   # Set the value of $s4 as the argument for calcForOdd
	jal calcForOdd 	   # Call calcForOdd
	move $s4, $v0      # Put the return value of calcForOdd in $s4
	j nextIteration
	
calcForEven:
	srl $s4, $s4, 1	   # Shift right logical to divide by 2
nextIteration:
	addi $s2, $s2, 1   # Increase loop counter
	j mainLoop

# Calculates N*3+1
# Arguments: In $a0 must have the integer N
# Returns: $v0 will have the result. If an overflow happens then $v0 is negative
calcForOdd:
	addi $sp, $sp, -12 # Save space on the stack for 3 items
	sw $s1, 8($sp)	   # Save register $s1
	sw $s2, 4($sp)	   # Save register $s2
	sw $ra, 0($sp)	   # Save register $ra
	
	move $s1, $a0	   # Store argument in $s1
	move $s2, $zero	   # Set $s2 to 0. In $s2 will go the results
	addi $t0, $zero, 3 # Set loop counter to 3
calcForOddLoop:
	bltz $s2, calcForOddFinished # If $s1 < 0 there was an overflow so return the negative number
	addu $s2, $s2, $s1  # To multiply by 3 the function adds N 3 times to itself
	addi $t0, $t0, -1  # Substract 1 of the loop counter
	bne $t0, $zero, calcForOddLoop # Loop again if counter is not 0
	addiu $s2, $s2, 1   # After calculating N*3 add 1
calcForOddFinished:
	move $v0, $s2	   # Store final result to $v0 to be the return value
	lw $s1, 8($sp)	   # Load register $s1
	lw $s2, 4($sp)	   # Load register $s2
	lw $ra, 0($sp)	   # Load register $ra
	addi $sp, $sp, 12  # Release space on the stack of 3 items
	jr $ra
# End of calcForOdd function

# Prints the result and calls the exit function
printResult:
	# Print the new line character
	li $a0, 10
	li $v0, 11
	syscall
	
	# Print the first part of the result message
	la $a0, resultPart1
	li $v0, 4
	syscall
	
	# Print the number entered by the user
	move $a0, $s1
	li $v0, 1
	syscall 

	# Print the second part of the result message
	la $a0, resultPart2
	li $v0, 4
	syscall
	
	# Print the number of iterations
	move $a0, $s2
	li $v0, 1
	syscall 

	# Print the third part of the result message
	la $a0, resultPart3
	li $v0, 4
	syscall
	
	j exit

# Prints the negative input error message and calls the exit function
negativeInputError:
	la $a0, negativeInputMsg
	li $v0, 4
	syscall
	j exit

# Prints the overflow error message and calls the exit function
overflowError:
	# Print the new line character
	li $a0, 10
	li $v0, 11
	syscall
	
	# Print the overflow error message
	la $a0, overflowMsg
	li $v0, 4
	syscall
	
	# Print the iteration number where the overflow ocurred
	move $a0, $s2
	li $v0, 1
	syscall 

	j exit

# Exit the program
exit:
        li $v0, 10
        syscall