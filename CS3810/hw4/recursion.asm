# Julio C. Menendez - u0665929

	.data
prompt:
	.asciiz "Enter a number: "
invalidNumber:
	.asciiz "Invalid number entered."
reachedEnd:
	.asciiz "Reached the end"
depth:
	.asciiz "Recursion depth "
colonChar:
	.asciiz ":"
xChar:
	.asciiz "x"

	.text
main:
	# Show message to the user asking for an integer
	la $a0, prompt
	li $v0, 4
	syscall
	
	# Wait for the user input
	li $v0, 5
	syscall

	move $s1, $v0	   # Save the integer entered by the user to $a0
	ble $s1, $zero, invalidNumberError # Branch to invalidNumberError if N <= 0
	add $t0, $zero, 10 # Put 10 in $t0 to compare it later with $s1
	bgt $s1, $t0, invalidNumberError # Branch to invalidNumberError if N > 10
	
	move $a0, $s1	   # Put N in $a0
	jal recursion
exit:
        li $v0, 10
        syscall

	# Print the error message and exit the application
invalidNumberError:
	la $a0, invalidNumber
	li $v0, 4
	syscall
	j exit		  # Exit the application

# Recursion function translated from C to MIPS
# Arguments:
#   $a0 = N
# Return:
#   $v0 = integer
recursion:
	addi $sp, $sp, -20 # Move stack
	# Save registers' values used in this procedure
	sw $ra, 0($sp)
	sw $fp, 4($sp)
	sw $s0, 8($sp)
	sw $s1, 12($sp)
	sw $s2, 16($sp)
	addi $fp, $sp, 16
	
	move $s1, $a0	   # Save N in $s1
	addi $t2, $zero, 9 # Put 9 in $t2 to compare it with N
	bgt $s1, $t2, recursionEnd # Branch to recursionEnd if N > 9
	
	# Print "Recursion depth "
	la $a0, depth
	li $v0, 4
	syscall
	
	# Print N
	move $a0, $s1
	li $v0, 1
	syscall
	
	# Print ":"
	la $a0, colonChar
	li $v0, 4
	syscall
	
	add $s0, $zero, $s1  # Put N in i (i is $s0) ($s0 is the loop counter)
printXLoop1:
	# Print "x"
	la $a0, xChar
	li $v0, 4
	syscall
	
	addi $s0, $s0, -1     # Decrement $s0
	bne $s0, $zero, printXLoop1 # Loop again if $s0 != 0
	
	# Print the new line character
	li $a0, 10
	li $v0, 11
	syscall
	
	addi $s0, $s1, 6      # i = N + 6
	addi $t1, $s1, -5     # k = N - 5
	addi $s2, $s1, 1      # j = N + 1
	
	move $a0, $s2	      # Set the argument of the procedure to $s2 (j)
	addi $sp, $sp, -4     # Make room for another value on the stack
	sw $t1, 20($sp)	      # Preserve the value of $t1
	jal recursion
	lw $t1, 20($sp)	      # Restore the value of $t1
	addi $sp, $sp, 4      # Destroy the space used to preserve $t1
	move $s2, $v0	      # Put the returning value of the procedure in $s2 (j)
	
	add $s2, $s2, $s0     # j = j + i
	add $s2, $s2, $t1     # j = j + k
	
	# Print "Recursion depth "
	la $a0, depth
	li $v0, 4
	syscall
	
	# Print N
	move $a0, $s1
	li $v0, 1
	syscall
	
	# Print ":"
	la $a0, colonChar
	li $v0, 4
	syscall
	
	add $s0, $zero, $s1  # Put N in i (i is $s0) ($s0 is the loop counter)
printXLoop2:
	# Print "x"
	la $a0, xChar
	li $v0, 4
	syscall
	
	addi $s0, $s0, -1     # Decrement $s0
	bne $s0, $zero, printXLoop2 # Loop again if $s0 != 0
	
	# Print ":"
	la $a0, colonChar
	li $v0, 4
	syscall
	
	# Print j
	move $a0, $s2
	li $v0, 1
	syscall
	
	# Print the new line character
	li $a0, 10
	li $v0, 11
	syscall
	
	move $v0, $s2      # Put $s2 (j) in $v0 as the return value of the procedure
		
recursionReturn:
	# Restore registers used in this procedure
	lw $ra, 0($sp)
	lw $fp, 4($sp)
	lw $s0, 8($sp)
	lw $s1, 12($sp)
	lw $s2, 16($sp)
	addi $sp, $sp, 20
	
	jr $ra

recursionEnd:
	# Print "Reached the end"
	la $a0, reachedEnd
	li $v0, 4
	syscall
	
	# Print the new line character
	li $a0, 10
	li $v0, 11
	syscall
	
	move $v0, $s1      # Put $s1 in $v0 as the return value of the procedure
	j recursionReturn